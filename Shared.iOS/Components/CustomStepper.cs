﻿
using CoreGraphics;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Shared.iOS.Components
{
    //public class MyConroller: UIViewController
    //{
    //    CustomStepper stepper;
    //    UILabel l;

    //    public override void ViewDidLoad()
    //    {
    //        base.ViewDidLoad();
    //        stepper = new CustomStepper();
    //        stepper.Bounds = new CGRect(20, 40, 100, 40);
           
    //        stepper.Format = "{0:F1} mph";
    //        stepper.StepInterval = 0.1f;
    //        stepper.Value = 5;
    //        stepper.Changed += (s, e) =>
    //        {
    //            l.Text = stepper.Value.ToString();
    //        };
    //        Add(stepper);
    //        l = new UILabel();// (new CGRect(0, 100, 60, 30));
    //        Add(l);
    //        l.Text = "Value";
    //       // View.BringSubviewToFront(stepper);

    //        //l.SetTitle("Alex", UIControlState.Normal);
    //        View.BackgroundColor = UIColor.White;
    //        View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
    //        View.AddConstraints(

    //            stepper.AtTopOf(View, 40),
    //            stepper.AtLeftOf(View, 10),
    //            stepper.Height().EqualTo(40),
    //            stepper.Width().EqualTo(120),
    //            l.WithSameTop(stepper),
    //            l.WithSameRight(View),
    //            l.Height().EqualTo(40),
    //            l.Width().EqualTo(100)
    //            );
    //    }
    //}
    public class CustomStepper : UIControl
    {
        const float kButtonWidth = 30.0f;
        const float hPadding = 2f;
        private float mValue;
        public float Value
        {
            get { return mValue; }
            set
            {
                if (mValue != value)
                {
                    mValue = value;
                    OnValueChanged();
                }
            }
        }
        public float StepInterval;
        public float Minimum;
        public float Maximum;
        public string Format;

        float buttonWidth;
        UIButton incrementButton, decrementButton;
        UILabel valueLabel;

        public CustomStepper()
        {

            Format = "F1";
            Initialize();OnValueChanged();
         
        }
        
        public string   Text
        {
            get { return valueLabel.Text; }

            set { valueLabel.Text = value; }
        }

        public event EventHandler Changed;
        protected void OnValueChanged()
        {
            LayoutSubviews();
            valueLabel.Text = string.Format(Format, Value);
            Changed?.Invoke(this, EventArgs.Empty);
        }
        void Initialize()
        {
            Value = 0.0f;
            StepInterval = 1.0f;
            Minimum = 0.0f;
            Maximum = 100.0f;
            buttonWidth = kButtonWidth;
            //_hidesDecrementWhenMinimum = NO;
            //_hidesIncrementWhenMaximum = NO;
            //_buttonWidth = kButtonWidth;

            this.ClipsToBounds = true;

            //[self setBorderWidth:1.0f];
            //[self setCornerRadius:3.0];

            valueLabel = new UILabel();
            valueLabel.TextAlignment = UITextAlignment.Center;
            valueLabel.Text = "Value";
            //self.countLabel.layer.borderWidth = 1.0f;
            this.Add(valueLabel);

            incrementButton = new UIButton();// [UIButton buttonWithType:UIButtonTypeCustom];
            incrementButton.SetTitle(@"+", UIControlState.Normal);
            Add(incrementButton);
            //var rec = new UILongPressGestureRecognizer((s) =>
            //{
            //    if (Value < Maximum)
            //    {
            //        Value += StepInterval;
            //        if (Value > Maximum)
            //            Value = Maximum;
            //        OnValueChanged();
            //    }
            //});
            //incrementButton.AddGestureRecognizer(rec);
            incrementButton.TouchUpInside += (s, e) =>
             {
                 if (Value < Maximum)
                 {
                     Value += StepInterval;
                     if (Value > Maximum)
                         Value = Maximum;
                     OnValueChanged();
                 }
             };
            //////[self.incrementButton addTarget:self action:@selector(incrementButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            //////[self addSubview:self.incrementButton];

            decrementButton = new UIButton();// [UIButton buttonWithType:UIButtonTypeCustom];
            decrementButton.SetTitle(@"-", UIControlState.Normal);
            Add(decrementButton);
            
            decrementButton.TouchUpInside += (s, e) =>
            {
                if (Value > Minimum)
                {
                    Value -= StepInterval;
                    if (Value < Minimum)
                        Value = Minimum;
                    OnValueChanged();
                }
            };
            
            ////[self.decrementButton addTarget:self action:@selector(decrementButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            ////[self addSubview:self.decrementButton];
            UIColor defaultColor = UIColor.Black;
            //UIColor defaultColor = UIColor.Red;// [UIColor colorWithRed: (79 / 255.0) green: (161 / 255.0) blue: (210 / 255.0) alpha: 1.0];
            SetLabelTextColor(defaultColor);
            SetButtonTextColor(defaultColor, UIControlState.Normal);
            SetCornerRadius(3f);
            SetBorderColor(defaultColor);
            SetBorderWidth(1f);

            //                                   // this.BosetBorderColor:defaultColor];
            //                                   //[self setLabelTextColor:defaultColor];
            //                                   //[self setButtonTextColor:defaultColor forState:UIControlStateNormal];

            //[self setLabelFont:[UIFont fontWithName:@"Avernir-Roman" size:14.0f]];
            SetButtonFont(UIFont.FromName(@"Avenir-Black", 20.0f));
        }
        public override CGSize SizeThatFits(CGSize size)
        {
            if (size == CGSize.Empty)
            {
                CGSize labelSize = valueLabel.SizeThatFits(size);
                return new CGSize(labelSize.Width + buttonWidth * 2, labelSize.Height);
            }
            else
                return base.SizeThatFits(size);
        }


        public override void LayoutSubviews()
        {

            nfloat width = this.Bounds.Width;
            nfloat height = Bounds.Height;
            nfloat x = Bounds.Left;
            nfloat y = Bounds.Top;
            valueLabel.SizeToFit();
            incrementButton.SizeToFit();
            decrementButton.SizeToFit();
            valueLabel.Frame = new CGRect(x + (width- valueLabel.Bounds.Width) /2, y, valueLabel.Bounds.Width, height);
            incrementButton.Frame = new CGRect(valueLabel.Frame.Right + hPadding, y, buttonWidth, height);
            decrementButton.Frame = new CGRect(valueLabel.Frame.Left - buttonWidth - hPadding, y, buttonWidth, height);
            //valueLabel.Frame = new CGRect(x + (width - valueLabel.Bounds.Width) / 2, y, valueLabel.Bounds.Width, height);

            // self.incrementButton.hidden = (self.hidesIncrementWhenMaximum && [self isMaximum]);
            // self.decrementButton.hidden = (self.hidesDecrementWhenMinimum && [self isMinimum]);
        }

        public void SetCornerRadius(float radius)
        {
            this.Layer.CornerRadius = radius;
        }

        public void SetLabelTextColor(UIColor color)
        {
            this.valueLabel.TextColor = color;
        }

        public void SetBorderColor(UIColor color)
        {
            Layer.BorderColor = color.CGColor;
            valueLabel.Layer.BorderColor = color.CGColor;
        }

        public void SetBorderWidth(float width)
        {
            Layer.BorderWidth = width;
        }

        public void SetLabelFont(UIFont font)
        {
            valueLabel.Font = font;
        }


        public void SetButtonFont(UIFont font)
        {
            incrementButton.TitleLabel.Font = font;
            decrementButton.TitleLabel.Font = font;
        }
//- (vsetLabelFont:(UIFont*)font
//{
//    self.countLabel.font = font;
//}

public void SetButtonTextColor(UIColor color, UIControlState state)
        {
            incrementButton.SetTitleColor(color, state);
            decrementButton.SetTitleColor(color, state);
        }

    }
    /*
     * //
//  PKYStepper.m
//  PKYStepper
//
//  Created by Okada Yohei on 1/11/15.
//  Copyright (c) 2015 yohei okada. All rights reserved.
//

// action control: UIControlEventApplicationReserved for increment/decrement?
// delegate: if there are multiple PKYSteppers in one viewcontroller, it will be a hassle to identify each PKYSteppers
// block: watch out for retain cycle

// check visibility of buttons when
// 1. right before displaying for the first time
// 2. value changed

#import "PKYStepper.h"

static const float kButtonWidth = 44.0f;

@implementation PKYStepper

#pragma mark initialization
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    _value = 0.0f;
    _stepInterval = 1.0f;
    _minimum = 0.0f;
    _maximum = 100.0f;
    _hidesDecrementWhenMinimum = NO;
    _hidesIncrementWhenMaximum = NO;
    _buttonWidth = kButtonWidth;
    
    self.clipsToBounds = YES;
    [self setBorderWidth:1.0f];
    [self setCornerRadius:3.0];
    
    self.countLabel = [[UILabel alloc] init];
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    self.countLabel.layer.borderWidth = 1.0f;
    [self addSubview:self.countLabel];
    
    self.incrementButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.incrementButton setTitle:@"+" forState:UIControlStateNormal];
    [self.incrementButton addTarget:self action:@selector(incrementButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.incrementButton];
    
    self.decrementButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.decrementButton setTitle:@"-" forState:UIControlStateNormal];
    [self.decrementButton addTarget:self action:@selector(decrementButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.decrementButton];
    
    UIColor *defaultColor = [UIColor colorWithRed:(79/255.0) green:(161/255.0) blue:(210/255.0) alpha:1.0];
    [self setBorderColor:defaultColor];
    [self setLabelTextColor:defaultColor];
    [self setButtonTextColor:defaultColor forState:UIControlStateNormal];
    
    [self setLabelFont:[UIFont fontWithName:@"Avernir-Roman" size:14.0f]];
    [self setButtonFont:[UIFont fontWithName:@"Avenir-Black" size:24.0f]];
}


#pragma mark render
- (void)layoutSubviews
{
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    self.countLabel.frame = CGRectMake(self.buttonWidth, 0, width - (self.buttonWidth * 2), height);
    self.incrementButton.frame = CGRectMake(width - self.buttonWidth, 0, self.buttonWidth, height);
    self.decrementButton.frame = CGRectMake(0, 0, self.buttonWidth, height);
    
    self.incrementButton.hidden = (self.hidesIncrementWhenMaximum && [self isMaximum]);
    self.decrementButton.hidden = (self.hidesDecrementWhenMinimum && [self isMinimum]);
}

- (void)setup
{
    if (self.valueChangedCallback)
    {
        self.valueChangedCallback(self, self.value);
    }
}

- (CGSize)sizeThatFits:(CGSize)size
{
    if (CGSizeEqualToSize(size, CGSizeZero))
    {
        // if CGSizeZero, return ideal size
        CGSize labelSize = [self.countLabel sizeThatFits:size];
        return CGSizeMake(labelSize.width + (self.buttonWidth * 2), labelSize.height);
    }
    return size;
}


#pragma mark view customization
- (void)setBorderColor:(UIColor *)color
{
    self.layer.borderColor = color.CGColor;
    self.countLabel.layer.borderColor = color.CGColor;
}

- (void)setBorderWidth:(CGFloat)width
{
    self.layer.borderWidth = width;
}

- (void)setCornerRadius:(CGFloat)radius
{
    self.layer.cornerRadius = radius;
}

- (void)setLabelTextColor:(UIColor *)color
{
    self.countLabel.textColor = color;
}

- (void)setLabelFont:(UIFont *)font
{
    self.countLabel.font = font;
}

- (void)setButtonTextColor:(UIColor *)color forState:(UIControlState)state
{
    [self.incrementButton setTitleColor:color forState:state];
    [self.decrementButton setTitleColor:color forState:state];
}

- (void)setButtonFont:(UIFont *)font
{
    self.incrementButton.titleLabel.font = font;
    self.decrementButton.titleLabel.font = font;
}


#pragma mark setter
- (void)setValue:(float)value
{
    _value = value;
    if (self.hidesDecrementWhenMinimum)
    {
        self.decrementButton.hidden = [self isMinimum];
    }
    
    if (self.hidesIncrementWhenMaximum)
    {
        self.incrementButton.hidden = [self isMaximum];
    }
    
    if (self.valueChangedCallback)
    {
        self.valueChangedCallback(self, _value);
    }
}



#pragma mark event handler
- (void)incrementButtonTapped:(id)sender
{
    if (self.value < self.maximum)
    {
        self.value += self.stepInterval;
        if (self.incrementCallback)
        {
            self.incrementCallback(self, self.value);
        }
    }
}

- (void)decrementButtonTapped:(id)sender
{
    if (self.value > self.minimum)
    {
        self.value -= self.stepInterval;
        if (self.decrementCallback)
        {
            self.decrementCallback(self, self.value);
        }
    }
}


#pragma mark private helpers
- (BOOL)isMinimum
{
    return self.value == self.minimum;
}

- (BOOL)isMaximum
{
    return self.value == self.maximum;
}

@end


     */
}
