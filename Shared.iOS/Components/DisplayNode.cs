using SpriteKit;
using UIKit;
using CoreGraphics;

namespace Shared.iOS.Components
{

    public class DisplayLabel
    {

        public SKLabelNode Label;
        public SKShapeNode Node;
        public object Tag { get; set; }
        public DisplayLabel(CGSize size) : this(CGPoint.Empty, size)
        {


        }
        public DisplayLabel(CGPoint position, CGSize size)
        {

            Node = SKShapeNode.FromRect(size, 5);
            Node.FillColor = UIColor.FromRGBA(0, 0, 0, 0.3f);         
            Label = new SKLabelNode("Arial");
            Label.FontColor =  UIColor.White;
            Label.UserInteractionEnabled = false;
            //Label.Position = new CGPoint(-size.Width/2, 0);
            Label.FontSize = 20;
            HorizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center;
            Label.VerticalAlignmentMode = SKLabelVerticalAlignmentMode.Center;
            Node.Add(Label);
            Node.SetPosition(position); 
        }
        public SKLabelHorizontalAlignmentMode HorizontalAlignmentMode
        {
            get { return Label.HorizontalAlignmentMode; }
            set
            {
                Label.HorizontalAlignmentMode = value;
                switch (value)
                {
                    case SKLabelHorizontalAlignmentMode.Center:
                        Label.Position = new CGPoint (0, 0);
                        break;
                    case SKLabelHorizontalAlignmentMode.Left:
                        Label.Position  = new CGPoint(-Node.Frame.Width / 2, 0);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}