﻿using CoreGraphics;
using SpriteKit;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.iOS.Components
{
    static class SpriteKitExtensions
    {
        public static void SetPosition(this SKNode node, CGPoint point)
        {
            var p = point + new CGSize(node.Frame.Size.Width / 2, node.Frame.Size.Height / 2);
            node.Position = p;
        }
    }
}
