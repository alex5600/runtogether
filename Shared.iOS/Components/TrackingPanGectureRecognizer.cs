using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Shared.iOS.Components
{

    class TrackingPanGectureRecognizer : UIPanGestureRecognizer
    {
        public CGPoint Start;
        public TrackingPanGectureRecognizer(Action<UIPanGestureRecognizer> recognizer):base(recognizer)
        {

        }
        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);
            Start = this.TranslationInView(this.View);
        }
        
    }
}