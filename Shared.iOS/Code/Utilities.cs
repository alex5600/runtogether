﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Shared.iOS.Code
{
    public static class Utilities
    {
       
        public static string GetFileNameInDocuments(string fileName)
        {
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return Path.Combine(documents, fileName);
        }
    }
}
