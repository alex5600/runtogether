﻿using System;
using System.Runtime.InteropServices;

namespace FFT
{
    public static class Dsp
    {
        class FftSetupD : IDisposable
        {
            public IntPtr Handle;
            public FftSetupD(int nlog2)
            {
                Handle = vDSP_create_fftsetupD(nlog2, FftRadix.Radix2);
            }
            ~FftSetupD()
            {
                DisposeIt();
            }
            public void Dispose()
            {
                GC.SuppressFinalize(this);
                DisposeIt();
            }
            void DisposeIt()
            {
                vDSP_destroy_fftsetupD(Handle);
                Handle = IntPtr.Zero;
            }

            [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_create_fftsetupD")]
            unsafe static extern IntPtr vDSP_create_fftsetupD(int log2n, FftRadix radix);

            [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_destroy_fftsetupD")]
            unsafe static extern void vDSP_destroy_fftsetupD(IntPtr setup);
        }

        class FftSetup : IDisposable
        {
            public IntPtr Handle;
            public FftSetup(int nlog2)
            {
                Handle = vDSP_create_fftsetup(nlog2, FftRadix.Radix2);
            }
            ~FftSetup()
            {
                DisposeIt();
            }
            public void Dispose()
            {
                GC.SuppressFinalize(this);
                DisposeIt();
            }
            void DisposeIt()
            {
                vDSP_destroy_fftsetup(Handle);
                Handle = IntPtr.Zero;
            }

            [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_create_fftsetup")]
            unsafe static extern IntPtr vDSP_create_fftsetup(int log2n, FftRadix radix);

            [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_destroy_fftsetup")]
            unsafe static extern void vDSP_destroy_fftsetup(IntPtr setup);
        }

        enum FftDirection : int
        {
            Forward = 1,
            Inverse = -1,
        }
        enum FftRadix : int
        {
            Radix2 = 0,
        }
        [StructLayout(LayoutKind.Sequential)]
        unsafe struct SplitComplexD
        {
            public double* Realp;
            public double* Imagp;
        }
        [StructLayout(LayoutKind.Sequential)]
        unsafe struct SplitComplex
        {
            public float* Realp;
            public float* Imagp;
        }

        static int GetNLog2(int n)
        {
            return Convert.ToInt32(Math.Floor(Math.Log(n, 2)));
        }
        public static IDisposable SetupFftD(int nlog2)
        {
            return new FftSetupD(nlog2);
        }

        public static IDisposable SetupFft(int nlog2)
        {
            return new FftSetup(nlog2);
        }

        public static void Fft(ComplexDataD data, object setup, bool forward)
        {
            unsafe
            {
                fixed (double* realp = data.Reals)
                {
                    fixed (double* imagp = data.Imags)
                    {
                        SplitComplexD d;
                        d.Realp = realp;
                        d.Imagp = imagp;
                        vDSP_fft_zipD(((FftSetupD)setup).Handle, ref d, 1, GetNLog2(data.Length), forward ? FftDirection.Forward : FftDirection.Inverse);
                    }
                }
            }
        }

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_fft_zipD")]
        unsafe static extern void vDSP_fft_zipD(IntPtr setup, ref SplitComplexD ioData, int stride, int log2n, FftDirection direction);

        public static void Fft(ComplexData data, object setup, bool forward)
        {
            unsafe
            {
                fixed (float* realp = data.Reals)
                {
                    fixed (float* imagp = data.Imags)
                    {
                        SplitComplex d;
                        d.Realp = realp;
                        d.Imagp = imagp;
                        vDSP_fft_zip(((FftSetup)setup).Handle, ref d, 1, GetNLog2(data.Length), forward ? FftDirection.Forward : FftDirection.Inverse);
                    }
                }
            }
        }

        public static void Fft_R(ComplexDataR data, object setup, bool forward)
        {
            unsafe
            {
                fixed (float* realp = data.Reals)
                {
                    fixed (float* imagp = data.Imags)
                    {
                        SplitComplex d;
                        d.Realp = realp;
                        d.Imagp = imagp;
                        vDSP_fft_zrip(((FftSetup)setup).Handle, ref d, 1, GetNLog2(data.Length), forward ? FftDirection.Forward : FftDirection.Inverse);
                    }
                }
            }
        }


        //void vDSP_ctoz(const DSPComplex *__C, vDSP_Stride __IC, const DSPSplitComplex *__Z, vDSP_Stride __IZ, vDSP_Length __N);

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_ctoz")]
        unsafe static extern void vDSP_ctoz(float* data, int stride1, ref SplitComplex ioData, int stride, int log2n);

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_fft_zip")]
        unsafe static extern void vDSP_fft_zip(IntPtr setup, ref SplitComplex ioData, int stride, int log2n, FftDirection direction);

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_fft_zrip")]
        unsafe static extern void vDSP_fft_zrip(IntPtr setup, ref SplitComplex ioData, int stride, int log2n, FftDirection direction);

        public static void GetMagnitudesSquared(ComplexDataD data, double[] output)
        {
            unsafe
            {
                fixed (double* realp = data.Reals)
                {
                    fixed (double* imagp = data.Imags)
                    {
                        SplitComplexD d;
                        d.Realp = realp;
                        d.Imagp = imagp;

                        vDSP_zvmagsD(ref d, 1, output, 1, output.Length);
                    }
                }
            }
        }

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_zvmagsD")]
        unsafe static extern void vDSP_zvmagsD(ref SplitComplexD a, int stride, double[] c, int k, int n);

        public static void GetMagnitudesSquared(ComplexData data, float[] output)
        {
            unsafe
            {
                fixed (float* realp = data.Reals)
                {
                    fixed (float* imagp = data.Imags)
                    {
                        SplitComplex d;
                        d.Realp = realp;
                        d.Imagp = imagp;

                        vDSP_zvmags(ref d, 1, output, 1, output.Length);
                    }
                }
            }
        }
       
        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_zvmags")]
        unsafe static extern void vDSP_zvmags(ref SplitComplex a, int stride, float[] c, int k, int n);

        public static void GetDecibelsFromPower(double[] power, double[] reference, double[] output)
        {
            vDSP_vdbconD(power, 1, reference, output, 1, output.Length, 0);
        }

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_vdbconD")]
        unsafe static extern void vDSP_vdbconD(double[] a, int i, double[] b, double[] c, int k, int n, uint f);

        public static void GetDecibelsFromPower(float[] power, float[] reference, float[] output)
        {
            vDSP_vdbcon(power, 1, reference, output, 1, output.Length, 0);
        }

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_vdbcon")]
        unsafe static extern void vDSP_vdbcon(float[] a, int i, float[] b, float[] c, int k, int n, uint f);

        public static void Lint(float[] a, float[] b, float[] output)
        {
            vDSP_vlint(a, b, 1, output, 1, output.Length, a.Length);
        }

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_vlint")]
        unsafe static extern void vDSP_vlint(float[] a, float[] b, int j, float[] c, int k, int n, int m);

        public static void MultiplyAndAdd(float[] a, float b, float c, float[] output)
        {
            vDSP_vsmsa(a, 1, ref b, ref c, output, 1, output.Length);
        }

        [System.Runtime.InteropServices.DllImport("__Internal", EntryPoint = "vDSP_vsmsa")]
        unsafe static extern void vDSP_vsmsa(float[] a, int i, ref float b, ref float c, float[] d, int l, int n);

        public static void Clip(float[] a, float b, float c, float[] output)
        {
            vDSP_vclip(a, 1, ref b, ref c, output, 1, output.Length);
        }

        [System.Runtime.InteropServices.DllImport("__Internal")]
        unsafe static extern void vDSP_vclip(float[] a, int i, ref float b, ref float c, float[] d, int l, int n);

        public static float[]  ComputeFFT(float[] data, int n)
        {
            int log2n = GetNLog2(n);
            var setup = SetupFft(log2n);
            ComplexData d = new ComplexData(data, null);
            Fft(d, setup, true);
            float[] mags = new float[n];
            GetMagnitudesSquared(d, mags);
            setup.Dispose();
            return mags;
        }


        //unsafe public static float[] ComputeFFTR(float[] data, int n)
        //{
        //    int log2n = GetNLog2(n);
        //    var setup = SetupFft(log2n);
        //    int n2 = n / 2;
            
        //        SplitComplex d = new SplitComplex();
        //        float[] real = new float[n2];
        //        float[] imag = new float[n2];
        //    fixed (float* datap = data)
        //    {
        //        fixed (float* realp = real)
        //        {
        //            fixed (float* imagp = imag)
        //            {

        //                d.Realp = realp;
        //                d.Imagp = imagp;
        //                vDSP_ctoz(datap, 2, ref d, 1, log2n);
        //            }
        //        }
        //    }
        //    vDSP_fft_zrip(((FftSetup)setup).Handle, ref d, 1, GetNLog2(data.Length), FftDirection.Forward);
        //    float[] mags = new float[n2];
        //    vDSP_zvmags(ref d, 1, mags, 1, mags.Length);
           
        //    setup.Dispose();
        //    return mags;
            
        //}

        public static double[] ComputeFFT(double[] data, int n)
        {
            int log2n = GetNLog2(n);
            var setup = SetupFftD(log2n);
            ComplexDataD d = new ComplexDataD(data, null);
            Fft(d, setup, true);
            double[] mags = new double[n];
            GetMagnitudesSquared(d, mags);
            setup.Dispose();
            return mags;
        }
    }

    public class ComplexDataD
    {
        public readonly double[] Reals;
        public readonly double[] Imags;

        public int Length { get { return Reals.Length; } }

        public ComplexDataD(int n)
        {
            Reals = new double[n];
            Imags = new double[n];
        }
        public ComplexDataD(double[] reals, double[] imags)
        {
            int n = reals.Length;
            Reals = new double[n];
            Imags = new double[n];
            unsafe
            {
                fixed (double* pSource = reals, pTarget = Reals)
                {

                    double* ps = pSource;
                    double* pt = pTarget;
                    for (int i = 0; i < n; i++)
                    {
                        *pt = *ps;
                        pt++;
                        ps++;
                    }
                }

            }

        }
    }

    public class ComplexDataR:ComplexData
    {
       
        public ComplexDataR(float[] reals)
        {
            int n = reals.Length;
            int nover2 = n / 2;
            Reals = new float[nover2];
            Imags = new float[nover2];
            for(int i = 0; i < nover2; i++)
            {
                Reals[i] = reals[2 * i];
                Imags[i] = reals[2 * i + 1];
            }

        }
    }
    public class ComplexData
    {
        public  float[] Reals;
        public  float[] Imags;

        public int Length { get { return Reals.Length; } }

        public ComplexData()
        {

        }
        public ComplexData(float[] reals, float[] imags)
        {
            int n = reals.Length;
            Reals = new float[n];
            Imags = new float[n];
            unsafe
            {
                fixed (float* pSource = reals, pTarget = Reals)
                {
                    
                    float* ps = pSource;
                    float* pt = pTarget;
                    for (int i = 0; i < n; i++)
                    {
                        *pt = *ps;
                        pt++;
                        ps++;
                    }
                }

            }

        }
    }
}

