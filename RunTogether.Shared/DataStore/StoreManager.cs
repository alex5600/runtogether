﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using RunTogether.Shared.AzureClient;
using RunTogether.Shared.DataStore.Interfaces;
using RunTogether.Shared.DataStore.ServerStore;
using RunTogether.Shared.Utils;

namespace RunTogether.Shared.DataStore
{
    public class StoreManager : IStoreManager
    {


        // MobileServiceSQLiteStore store;

        //public async Task InitializeAsync()
        //{
        //    if (IsInitialized)
        //        return;

        //    //Get our current client, only ever need one
        //    var client = ServiceLocator.Instance.Resolve<IAzureClient>()?.Client;

        //    if (!string.IsNullOrWhiteSpace(Settings.Current.AuthToken) &&
        //        !string.IsNullOrWhiteSpace(Settings.Current.AzureMobileUserId))
        //    {
        //        client.CurrentUser = new MobileServiceUser(Settings.Current.AzureMobileUserId)
        //        {
        //            MobileServiceAuthenticationToken = Settings.Current.AuthToken
        //        };
        //    }

        //    var path = $"syncstore{Settings.Current.DatabaseId}.db";
        //    //setup our local sqlite store and intialize our table
        //    store = new MobileServiceSQLiteStore(path);

        //    store.DefineTable<UserProfile>();
        //    store.DefineTable<TripPoint>();
        //    store.DefineTable<Photo>();
        //    store.DefineTable<Trip>();
        //    store.DefineTable<POI>();
        //    store.DefineTable<IOTHubData>();

        //    await client.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler());

        //    IsInitialized = true;
        //}

        //public async Task<bool> SyncAllAsync(bool syncUserSpecific)
        //{
        //    if (!IsInitialized)
        //        await InitializeAsync();

        //    var taskList = new List<Task<bool>> {TripStore.SyncAsync()};

        //    var successes = await Task.WhenAll(taskList);
        //    return successes.Any(x => !x); //if any were a failure.
        //}

        //public async Task DropEverythingAsync()
        //{
        //    Settings.Current.UpdateDatabaseId();
        //    await TripStore.DropTable();
        //    await PhotoStore.DropTable();
        //    await UserStore.DropTable();
        //    await IOTHubStore.DropTable();
        //    IsInitialized = false;
        //    await InitializeAsync();
        //}

        public bool IsInitialized { get; private set; }


      

        IUserStore userStore;
        public IUserStore UserStore => userStore ?? (userStore = ServiceLocator.Instance.Resolve<IUserStore>());

        ITripStore tripStore;
        public ITripStore TripStore => tripStore ?? (tripStore = ServiceLocator.Instance.Resolve<ITripStore>());

        IRouteStore routeStore;
        public IRouteStore RouteStore => routeStore ?? (routeStore = ServiceLocator.Instance.Resolve<IRouteStore>());

        IRunSpaceStore runStore;
        public IRunSpaceStore RunSpaceStore => runStore ?? (runStore = ServiceLocator.Instance.Resolve<IRunSpaceStore>());
        static IStoreManager storeManager;




        public static IStoreManager Instance
        {
            get
            {
                if (storeManager == null)
                {
                    Init();
                    storeManager = ServiceLocator.Instance.Resolve<IStoreManager>();

                }
                return storeManager;
            }
        }
        public static bool UseMock = true;
        static void Init()
        {
            ServiceLocator.Instance.Add<IMobileClient, MobileClient>();
            if (UseMock)
            {
                ServiceLocator.Instance.Add<IUserStore, DataStore.Mock.UserStore>();
                ServiceLocator.Instance.Add<ITripStore, DataStore.Mock.TripStore>();
                ServiceLocator.Instance.Add<IRouteStore, DataStore.Mock.RouteStore>();
                ServiceLocator.Instance.Add<IStoreManager, DataStore.Mock.StoreManager>();
                ServiceLocator.Instance.Add<IRunSpaceStore, DataStore.Mock.RunSpaceStore>();
            }
            else
            {
                //ServiceLocator.Instance.Add<ITripStore, DataStore.Azure.Stores.TripStore>();
                //ServiceLocator.Instance.Add<ITripPointStore, DataStore.Azure.Stores.TripPointStore>();
                //ServiceLocator.Instance.Add<IPhotoStore, DataStore.Azure.Stores.PhotoStore>();
                ServiceLocator.Instance.Add<IUserStore, UserStore>();
                ServiceLocator.Instance.Add<ITripStore, TripStore>();
                ServiceLocator.Instance.Add<IRouteStore, RouteStore>();
               // ServiceLocator.Instance.Add<IRunSpaceStore, DataStore.Mock.RunSpaceStore>();
                //ServiceLocator.Instance.Add<IHubIOTStore, DataStore.Azure.Stores.IOTHubStore>();
                //ServiceLocator.Instance.Add<IPOIStore, DataStore.Azure.Stores.POIStore>();
                ServiceLocator.Instance.Add<IStoreManager, StoreManager>();
            }
        }

    }
}