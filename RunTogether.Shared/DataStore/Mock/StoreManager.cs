﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.


using System.Threading.Tasks;
using RunTogether.Shared.DataStore.Interfaces;
using RunTogether.Shared.Utils;

namespace RunTogether.Shared.DataStore.Mock
{
    public class StoreManager : IStoreManager
    {
        #region IStoreManager implementation

        //public async Task InitializeAsync()
        //{
        //    await TripStore.InitializeStoreAsync();
        //}

        public Task<bool> SyncAllAsync(bool syncUserSpecific)
        {
            return Task.FromResult(true);
        }

        public Task DropEverythingAsync()
        {
            return Task.FromResult(true);
        }

        public bool IsInitialized => true;


        IUserStore userStore;
        public IUserStore UserStore => userStore ?? (userStore = ServiceLocator.Instance.Resolve<IUserStore>());

        ITripStore tripStore;
        public ITripStore TripStore => tripStore ?? (tripStore = ServiceLocator.Instance.Resolve<ITripStore>());

        IRouteStore routeStore;
        public IRouteStore RouteStore => routeStore ?? (routeStore = ServiceLocator.Instance.Resolve<IRouteStore>());

        IRunSpaceStore runStore;
        public IRunSpaceStore RunSpaceStore => runStore ?? (runStore = ServiceLocator.Instance.Resolve<IRunSpaceStore>());

        #endregion
    }
}