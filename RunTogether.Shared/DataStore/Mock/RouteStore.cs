﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunTogether.DataObjects;
using RunTogether.Shared.DataStore.Interfaces;
using RunTogether.Shared.Utils;

namespace RunTogether.Shared.DataStore.Mock
{
    public class RouteStore : BaseStore<RouteInfo>, IRouteStore
    {
        
        public RouteStore()
        {
            ItemCount = 5;
        }
        protected override RouteInfo CreateItem(int index)
        {
            string id = index.ToString();
            var item = new RouteInfo()
            {
                Id = id,
                Name = "Route " + id,
                Description = "Description " + id,
                Distance = 100 * index,
                RouteType = RouteType.Walking,
            };
            item.Picture = LocalResourceManager.ImageDataFromResource($"RunTogether.Shared.MockData.Route{index}.png");
          
            return item;
        }
        //protected override void PopulateItems()
        //{
        //    base.PopulateItems();
        //    Items = new List<RouteInfo>();
        //    for (int i = 1; i < 5; i++)
        //    {

        //        var rur
        //    }
        //}
    }
}
