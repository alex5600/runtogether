﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System;

using System.Collections.Generic;
using System.Threading.Tasks;
using RunTogether.Shared.DataStore.Interfaces;
using System.Linq;
using RunTogether.DataObjects;

namespace RunTogether.Shared.DataStore.Mock
{
    public abstract class BaseStore<T> : IBaseStore<T> where T : class, IBaseDataObject, new()
    {

        protected Lazy<List<T>> mItems;
      
        protected int ItemCount;
        protected List<T> Items => mItems.Value;
        public BaseStore()
        {

            mItems = new Lazy<List<T>>(() => CreateItems());
        }

        protected abstract T CreateItem(int index);
        
        protected virtual List<T> CreateItems()
        {
        
            var items = new List<T>();
            for (int i =1; i <= ItemCount; i++)
            {
                var item = CreateItem(i);
                if (item != null)
                    items.Add(item);
            }
            return items;
        }
        public virtual Task<T> GetItemAsync(string id)
        {
            var item = Items.Where(i => i.Id == id).SingleOrDefault();
            return Task.FromResult(item);
        }

        public virtual Task<IEnumerable<T>> GetItemsAsync(int skip = 0, int take = 100, bool forceRefresh = false)
        {
            return Task.FromResult(Items as IEnumerable<T>);
        }

        public virtual Task<string> InsertAsync(T item)
        {
            Items.Add(item);
            return Task.FromResult(item.Id);
        }
    }
}