﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunTogether.DataObjects;
using RunTogether.Shared.DataStore.Interfaces;

namespace RunTogether.Shared.DataStore.Mock
{
    class RunSpaceStore : BaseStore<Runner>, IRunSpaceStore
    {
        public RunSpaceStore()
        {
            ItemCount = 100;
        }
        public Task<IEnumerable<Runner>> GetRouteRunnersAsync(string routeId)
        {
            var res = Items.Where(r => r.RouteId == routeId).ToList();
            return Task.FromResult(res as IEnumerable<Runner>);
        }

        protected override List<Runner> CreateItems()
        {
            var storeManager = RunTogether.Shared.DataStore.StoreManager.Instance;
            List<Runner> items = new List<Runner>();
            var users = storeManager.UserStore.GetItemsAsync().Result;
            var routes = storeManager.RouteStore.GetItemsAsync().Result;
            int index = 0;
            Random rnd= new Random((int)DateTime.Now.Ticks);
            foreach(var r in routes)
            {
                foreach (var user in users)
                {
                    var item = new Runner()
                    {
                        User = user,
                        RouteId = r.Id,
                        Id = (++index).ToString(),
                        StepIndex = 0,
                        State = TripState.Running,
                        Velocity = 1 + 2 * rnd.NextDouble(),
                        Distance  = 200 * rnd.NextDouble()
                    };
                    items.Add(item);
                }
            }
            return items;
        }
        protected override Runner CreateItem(int index)
        {
            throw new NotImplementedException();
        }
    }
}
