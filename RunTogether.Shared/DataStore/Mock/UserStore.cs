﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System.Threading.Tasks;
using System.Collections.Generic;
using RunTogether.DataObjects;
using RunTogether.Shared.DataStore.Interfaces;
using System;
using System.Linq;

namespace RunTogether.Shared.DataStore.Mock
{
    public class UserStore : BaseStore<User>, IUserStore
    {

        public UserStore()
        {
            ItemCount = 10;
        }

        public override Task<string> InsertAsync(User user)
        {
            return  Task.FromResult(string.Empty);
            // return await Client.InvokeApiAsync<MyUser, string>("MyUser/AddUser", user, HttpMethod.Post, null);

        }

        protected override User CreateItem(int index)
        {
            var user = new User
            {
                Name = "User " + index,
                LastName = "Fiksel",
                Id = index.ToString()
            };
            return user;
        }
        public Task<IEnumerable<User>> GetFriends(User user)
        {
            var items = Items.Where(i => Int32.Parse(i.Id) % 2 == 0);
            return Task.FromResult(items);
        }

    }
}