﻿

using System.Threading.Tasks;

namespace RunTogether.Shared.DataStore.Interfaces
{
    public interface IStoreManager
    {
        bool IsInitialized { get; }
      
        IUserStore UserStore { get; }

        ITripStore TripStore { get; }

        IRouteStore RouteStore { get; }

        IRunSpaceStore RunSpaceStore { get; }
        //Task<bool> SyncAllAsync(bool syncUserSpecific);
        //Task DropEverythingAsync();
        //Task InitializeAsync();
    }
}