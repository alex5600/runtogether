﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RunTogether.DataObjects;

namespace RunTogether.Shared.DataStore.Interfaces
{
    public interface IUserStore : IBaseStore<User>
    {
        

         Task<IEnumerable<User>> GetFriends(User user);

        //Task AddFriend(MyUser user, MyUser friend);

        //Task RemoveFriend(MyUser user, MyUser friend);
    }
}