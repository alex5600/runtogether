﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using RunTogether.DataObjects;
using RunTogether.Shared.AzureClient;
using RunTogether.Shared.DataStore.Interfaces;
using RunTogether.Shared.Utils;

namespace RunTogether.Shared.DataStore.ServerStore
{
    public class BaseStore<T> : IBaseStore<T> where T : class, IBaseDataObject, new()
    {
        // IStoreManager storeManager;

        protected string Route = "MyUser";
        protected IMobileServiceClient Client => ServiceLocator.Instance.Resolve<IMobileClient>()?.Client;

        public virtual Task<T> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<IEnumerable<T>> GetItemsAsync(int skip = 0, int take = 100, bool forceRefresh = false)
        {
            // throw new NotImplementedException();
            return await Client.InvokeApiAsync<IEnumerable<T>>(Route +"/GetItems", HttpMethod.Get, null);
        }

        public virtual async Task<string> InsertAsync(T item)
        {
            return await Client.InvokeApiAsync<T, string>(Route +"/Insert", item, HttpMethod.Post, null);
        }
    }
}