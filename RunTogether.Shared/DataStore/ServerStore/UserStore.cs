﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using RunTogether.DataObjects;
using RunTogether.Shared.DataStore.Interfaces;

namespace RunTogether.Shared.DataStore.ServerStore
{
    public class UserStore : BaseStore<User>, IUserStore
    {
        public Task<IEnumerable<User>> GetFriends(User user)
        {
            throw new NotImplementedException();
        }
        //public async Task<IEnumerable<MyUser>> GetAllUsers()
        //{
        //    return await Client.InvokeApiAsync<IEnumerable<MyUser>>("MyUser/GetAllUsers", HttpMethod.Get, null);
        //}


        //public async Task<string> InsertUser(MyUser user)
        //{
        //    return await Client.InvokeApiAsync<MyUser, string>("MyUser/AddUser", user, HttpMethod.Post, null);
        //}


        //public virtual Task<T> GetItemAsync(string id)
        //{
        //    throw new NotImplementedException();
        //}

        //public virtual Task<IEnumerable<T>> GetItemsAsync(int skip = 0, int take = 100, bool forceRefresh = false)
        //{
        //    throw new NotImplementedException();
        //}

        //public virtual Task<string> InsertAsync(T item)
        //{
        //    throw new NotImplementedException();
        //}

    }
}