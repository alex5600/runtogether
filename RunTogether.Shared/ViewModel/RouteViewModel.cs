﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunTogether.DataObjects;

namespace RunTogether.Shared.ViewModel
{
 
    public class RouteViewModel : ViewModelBase
    {
        public RouteInfo Route;
        public IEnumerable<Runner> ActiveRunners;
        public RouteViewModel(RouteInfo route)
        {
            this.Route = route;
        }

        public async Task LoadModelAsync()
        {
            ActiveRunners = await StoreManager.RunSpaceStore.GetRouteRunnersAsync(Route.Id);
            return;
        }
    }
}
