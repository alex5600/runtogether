﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunTogether.DataObjects;

namespace RunTogether.Shared.ViewModel
{
    public class RoutesViewModel:ViewModelBase
    {
        public ICollection<RouteInfo> Routes { get; private set; }

        public RoutesViewModel()
        {
            Title = "Routes";
        }
        public async Task<bool> LoadRoutesAsync()
        {
            var res = await StoreManager.RouteStore.GetItemsAsync();
            Routes = res.ToList();
            return true;

        } 

        public RouteInfo GetElement(int index)
        {
            return Routes.ElementAt(index);
        }
    }
}
