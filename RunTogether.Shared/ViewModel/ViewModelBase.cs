﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.


using RunTogether.Shared.DataStore.Interfaces;
using RunTogether.Shared.Utils;
using Shared.Mvvm;

namespace RunTogether.Shared.ViewModel
{
    public class ViewModelBase : BaseViewModel
    {
        static IStoreManager _storeManager;

      
        //public Settings Settings => Settings.Current;

        public static IStoreManager StoreManager
        {
            get { return _storeManager; }
        }
        //    => _storeManager ?? (_storeManager = ServiceLocator.Instance.Resolve<IStoreManager>());

        public static void Init(bool useMock = false)
        {
            RunTogether.Shared.DataStore.StoreManager.UseMock = useMock;
           //  ServiceLocator.Instance.Add<, AzureClient.AzureClient>();
            _storeManager = RunTogether.Shared.DataStore.StoreManager.Instance;
            //if (useMock)
            //{
            //    ServiceLocator.Instance.Add<ITripStore, DataStore.Mock.Stores.TripStore>();
            //    ServiceLocator.Instance.Add<ITripPointStore, DataStore.Mock.Stores.TripPointStore>();
            //    ServiceLocator.Instance.Add<IPhotoStore, DataStore.Mock.Stores.PhotoStore>();
            //    ServiceLocator.Instance.Add<IUserStore, DataStore.Mock.Stores.UserStore>();
            //    ServiceLocator.Instance.Add<IHubIOTStore, DataStore.Mock.Stores.IOTHubStore>();
            //    ServiceLocator.Instance.Add<IPOIStore, DataStore.Mock.Stores.POIStore>();
            //    ServiceLocator.Instance.Add<IStoreManager, DataStore.Mock.StoreManager>();
            //}
            //else
            //{
            //    ServiceLocator.Instance.Add<ITripStore, DataStore.Azure.Stores.TripStore>();
            //    ServiceLocator.Instance.Add<ITripPointStore, DataStore.Azure.Stores.TripPointStore>();
            //    ServiceLocator.Instance.Add<IPhotoStore, DataStore.Azure.Stores.PhotoStore>();
            //    ServiceLocator.Instance.Add<IUserStore, DataStore.Azure.Stores.UserStore>();
            //    ServiceLocator.Instance.Add<IHubIOTStore, DataStore.Azure.Stores.IOTHubStore>();
            //    ServiceLocator.Instance.Add<IPOIStore, DataStore.Azure.Stores.POIStore>();
            //    ServiceLocator.Instance.Add<IStoreManager, DataStore.Azure.StoreManager>();
            //}
        }
    }
}