﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunTogether.DataObjects;

namespace RunTogether.Shared.Routes
{

#if __IOS__
    class SessionManagerEx
    {
        public List<RouteInfo> Routes { get; set; }
        public Dictionary<string, MyRunner> Runners;
        public DateTimeOffset LastUpdate { get; set; }

        public SessionManagerEx()
        {
            Runners = new Dictionary<string, MyRunner>();
            Routes = new List<RouteInfo>();
            LastUpdate = DateTimeOffset.Now;
        }
        public void UpdateRunner(IEnumerable<MyRunner> items, bool fireEvent)
        {
            foreach (var item in items)
                Runners[item.SessionId] = item;
            if (fireEvent)
                OnStateChanged(items, UpdateStatus.Updated);
        }

        public void UpdateRunner(Runner item, bool fireEvent)
        {
            Runners[item.SessionId] = item;
            if (fireEvent)
                OnStateChanged(new Runner[] { item }, UpdateStatus.Updated);
        }
        protected virtual RouteDefinition GetRoute(Runner item)
        {
            return Routes.Where(r => r.Id == item.RouteId).SingleOrDefault();
        }
        public virtual void AddRunners(IEnumerable<Runner> items, bool fireEvent)
        {
            foreach (var item in items)
                Runners[item.SessionId] = item;
            if (fireEvent)
                OnStateChanged(items, UpdateStatus.Added);
        }
        public virtual void DeleteRunner(Runner item, bool fireEvent)
        {
            // Runner r;
            Runners.TryRemove(item.SessionId, out item);
            if (fireEvent)
                OnStateChanged(new List<Runner>() { item }, UpdateStatus.Deleted);
        }
        public virtual void DeleteRunners(IEnumerable<Runner> items, bool fireEvent)
        {
            Runner outItem;
            foreach(var i in items)
                Runners.TryRemove(i.SessionId, out outItem);
            if (fireEvent)
                OnStateChanged(items, UpdateStatus.Deleted);
        }
        protected virtual void UpdatePositions(DateTimeOffset time)
        {
            foreach(var runner in Runners.Values)
            {
                if (runner.State == SessionState.Running)
                {
                    float delta = (float)(runner.Velocity * (time - LastUpdate).TotalSeconds);
                    runner.Distance += delta;
                }
            }
            LastUpdate = time;
        }

        public event Action PositionUpdate;
        public event Action<StateChangeEvent> StateChanged;
        protected virtual void OnPositionChanged()
        {
            PositionUpdate?.Invoke();
        }

        protected virtual void OnStateChanged(IEnumerable<Runner> list, UpdateStatus status)
        {
            StateChangeEvent e = new StateChangeEvent() { Items = list, Status = status };
            StateChanged?.Invoke(e);
        }
    }
    public enum UpdateStatus
    {
        Updated,
        Added,
        Deleted
    }
    public class StateChangeEvent
    {
        public UpdateStatus Status { get; set; }
        public IEnumerable<Runner> Items { get; set; }
    }
    class LocalSessionManager: SessionManagerEx
    {
        TimeSpan updateInterval;
        Timer timer;
        public Runner Actor;
        public LocalSessionManager(double interval)
        {
            updateInterval = TimeSpan.FromSeconds(interval);
        }

        public void Start()
        {
            timer = new Timer((_) =>
            {
                var time = LastUpdate + updateInterval;
                UpdatePositions(time);
            }, null, TimeSpan.Zero, updateInterval);
        }
        public void Stop()
        {
            if (timer != null)
                timer.Dispose();
            timer = null;
        }
        public RouteDefinition RouteDefinition
        {
            get { return Routes.SingleOrDefault(); }
        }
        protected override RouteDefinition GetRoute(Runner item)
        {
            return RouteDefinition;
        }

        public IEnumerable<Runner> GetRunners()
        {
            return Runners.Values.Where(i => i != Actor);
        }

        public IEnumerable<Runner> GetRunnersInFov()
        {
            return Runners.Values.Where(i => i != Actor && Math.Abs(i.Distance - Actor.Distance) <= 75);// Settings.Fov);
        }
        public virtual void Init()
        {
            SetRunnersPositions(true);
        }
      
        protected override void UpdatePositions(DateTimeOffset time)
        {
            base.UpdatePositions(time);
            SetRunnersPositions(false);
           
        }

        private void SetRunnersPositions( bool init)
        {
            foreach (Runner r in Runners.Values)
            {

                if (r.State != SessionState.Running && !init)
                    continue;
                var route = GetRoute(r);
                var index = route.Steps.FindIndex(r.StepIndex, (s) =>
                {
                    return s.Distance > r.Distance;
                });
                if (index == -1)
                {
                    r.State = SessionState.Completed;
                    r.StepIndex = route.Steps.Count - 1;
                    r.CurrentStep = route.Steps[r.StepIndex];
                    r.Position = r.CurrentStep.Position;
                }
                else
                {
                    r.StepIndex = index > 0 ? index - 1 : 0;
                    r.CurrentStep = route.Steps[r.StepIndex];
                    r.NextStep = route.Steps[r.StepIndex + 1];
                    float scale = (float)((r.Distance - r.CurrentStep.Distance) / (r.NextStep.Distance - r.CurrentStep.Distance));
                     r.Position = new SCNVector3(
                        r.CurrentStep.Position.X + (r.NextStep.Position.X - r.CurrentStep.Position.X) * scale,
                        r.CurrentStep.Position.Y + (r.NextStep.Position.Y - r.CurrentStep.Position.Y) * scale,
                        r.CurrentStep.Position.Z + (r.NextStep.Position.Z - r.CurrentStep.Position.Z) * scale
                        );
                    // LoggingService.LogDebug("Position {0}", r.Position.ToString());
                }

            }
            OnPositionChanged();
        }
    }
#endif
}
