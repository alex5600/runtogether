﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunTogether.DataObjects;

namespace RunTogether.Shared.RunSpace
{

    public enum UpdateStatus
    {
        Updated,
        Added,
        Deleted
    }
    public class StateChangeEvent
    {
        public UpdateStatus Status { get; set; }
        public Runner Item { get; set; }
    }

    public interface IRunSpaceManager
    {

        //void UpdateRunner(MyRunner item);

        //void AddRunner(MyRunner item);

        //void DeleteRunner(MyRunner item);

        void ChangeState(Runner item, UpdateStatus status);

        event Action<StateChangeEvent> StateChanged;
    }

    public interface ILocalRunSpaceManager: IRunSpaceManager
    {
         Runner Actor { get; set; }
       
         event Action PositionUpdated;

    }
    
}
