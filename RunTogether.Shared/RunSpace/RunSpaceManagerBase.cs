﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunTogether.DataObjects;
using RunTogether.Shared.DataStore.Interfaces;

namespace RunTogether.Shared.RunSpace
{
    public class RunSpaceManagerBase : IRunSpaceManager
    {
        
        public Dictionary<string, Runner> Runners;
        public DateTimeOffset LastUpdate { get; set; }

        public RunSpaceManagerBase()
        {
            Runners = new Dictionary<string, Runner>();
            LastUpdate = DateTimeOffset.Now;
          
        }
       
        
        protected virtual void UpdateState(Runner item, UpdateStatus status)
        {
            LastUpdate = DateTimeOffset.Now;
            SaveRunnerInfo(item);
            switch (status)
            {
                case UpdateStatus.Added:
                    Runners[item.Id] = item;
                    break;
                case UpdateStatus.Deleted:
                    Runners.Remove(item.Id);
                    break;
                case UpdateStatus.Updated:
                    Runners[item.Id] = item;
                    break;
                default:
                    break;
            }
        }
        public virtual void ChangeState(Runner item, UpdateStatus status)
        {
            UpdateState(item, status);
            FireStateChanged(item, status);
        }

        protected virtual void FireStateChanged(Runner item, UpdateStatus status)
        {
            StateChanged?.Invoke(new StateChangeEvent() { Item = item, Status = status });
        }
        public event Action<StateChangeEvent> StateChanged;
        
        protected virtual void SaveRunnerInfo(Runner item)
        {
           
        }

    }
}
