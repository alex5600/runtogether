﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RunTogether.DataObjects;
using Shared.Geometry;

namespace RunTogether.Shared.RunSpace
{

    public interface IPositionUpdater
    {
        event Action<DateTimeOffset> Update;
    }
    public class LocalRunSpaceManager : RunSpaceManagerBase, ILocalRunSpaceManager
    {


        public Runner Actor { get; set; }
        IPositionUpdater updater;
        public RouteInfo Route { get; set; }

        IRunSpaceManager proxy;

        public event Action PositionUpdated;

        public LocalRunSpaceManager(IRunSpaceManager proxy)
        {
            this.proxy = proxy;
            LastUpdate = DateTimeOffset.Now;
        }

        public void Start(IPositionUpdater updater)
        {
            //timer = new Timer((_) =>
            //{
            //    var time = LastUpdate + updateInterval;
            //    UpdatePositions(time);
            //}, null, TimeSpan.Zero, updateInterval);
            this.updater = updater;
            if (updater != null)
                updater.Update += UpdatePositions;
            if (proxy != null)
                proxy.StateChanged += OnStateChanged;
        }
        public void Stop()
        {
            if (proxy != null)
                proxy.StateChanged -= OnStateChanged;
            if (updater != null)
                updater.Update -= UpdatePositions;
        }

        void OnStateChanged(StateChangeEvent evt)
        {
            UpdateState(evt.Item, evt.Status);
            FireStateChanged(evt.Item, evt.Status);
        }

        public override void ChangeState(Runner item, UpdateStatus status)
        {
            UpdateState(item, status);
        }
        public IEnumerable<Runner> GetRunners()
        {
            return Runners.Values.Where(i => i != Actor);
        }

        public IEnumerable<Runner> GetRunnersInFov()
        {
            return Runners.Values.Where(i => i != Actor && Math.Abs(i.Distance - Actor.Distance) <= 75);// Settings.Fov);
        }
        public virtual void Init()
        {
            SetRunnersPositions(true);
        }

        protected void UpdatePositions(DateTimeOffset time)
        {
            foreach (var runner in Runners.Values)
            {
                if (runner.State == TripState.Running)
                {
                    float delta = (float)(runner.Velocity * (time - LastUpdate).TotalSeconds);
                    runner.Distance += delta;
                }
            }
            LastUpdate = time;
            SetRunnersPositions(false);

        }

      
        protected virtual void OnPositionUpdated()
        {
            PositionUpdated?.Invoke();
        }
        private void SetRunnersPositions(bool init)
        {
            foreach (var r in Runners.Values)
            {

                if (r.State != TripState.Running && !init)
                    continue;
                var route = Route;
                var index = route.Steps.FindIndex(r.StepIndex, (s) =>
                {
                    return s.Distance > r.Distance;
                });
                if (index == -1)
                {
                    r.State = TripState.Completed;
                    r.StepIndex = route.Steps.Count - 1;
                    r.CurrentStep = route.Steps[r.StepIndex];
                    r.Location = r.CurrentStep.Location;
                }
                else
                {
                    r.StepIndex = index > 0 ? index - 1 : 0;
                    r.CurrentStep = route.Steps[r.StepIndex];
                    r.NextStep = route.Steps[r.StepIndex + 1];
                    float scale = (float)((r.Distance - r.CurrentStep.Distance) / (r.NextStep.Distance - r.CurrentStep.Distance));
                    r.Location = new Location3(
                       r.CurrentStep.Location.X + (r.NextStep.Location.X - r.CurrentStep.Location.X) * scale,
                       r.CurrentStep.Location.Y + (r.NextStep.Location.Y - r.CurrentStep.Location.Y) * scale,
                       r.CurrentStep.Location.Z + (r.NextStep.Location.Z - r.CurrentStep.Location.Z) * scale
                       );
                    // LoggingService.LogDebug("Position {0}", r.Position.ToString());
                }

            }
            OnPositionUpdated();
        }
    }
}
