﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Microsoft.WindowsAzure.MobileServices;

namespace RunTogether.Shared.AzureClient
{
    public interface IMobileClient
    {
        IMobileServiceClient Client { get; }
    }
}