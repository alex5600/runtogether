﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Geometry
{
    public struct Location
    {
        public double X;
        public double Y;
        public Location(double x, double y)
        {
            X = x;
            Y = y;
        }

        public Location(float x, float y)
        {
            X = x;
            Y = y;
        }
    }

    public struct Location3
    {
        public double X;
        public double Y;
        public double Z;
        public Location3(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Location3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
    public class Position
    {
        public Location3 Location { get; set; }
        public double Distance { get; set; }
        //public SCNVector3 Heading { get; set; }
    }
}
