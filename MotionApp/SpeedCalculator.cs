﻿using CoreMotion;
using Foundation;
using Shared.iOS.Code;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Shared.Collections;
using Shared;
//sing FFT;

namespace MotionApp
{
    public class SpeedCalculator
    {
        const string CalibrationFile = "Calibration.txt";
        CMMotionManager motionManager;
        const double updateInterval = 0.01;
        const int calculateInterval = 1000;
        const int bufferPoints = 2048;// about 20 sec;
        const int ringSize = 4096;
        NSOperationQueue queue;
        RingBuffer<double> ringBuffer;
        IDisposable setup;
        List<Tuple<double, double>> calibrationPoints;
        const double DefaultStep = 0.73;// meters
        Timer cTimer;
       
        object mGate = new object();

        public void Start(bool clearCal = false)
        {
            LoadCalibration(clearCal);
            queue = NSOperationQueue.CurrentQueue;
            ringBuffer = new RingBuffer<double>(ringSize);
            motionManager = new CMMotionManager();
            if (!motionManager.AccelerometerAvailable)
                throw new Exception("No AccelerometerAvailable");
            motionManager.AccelerometerUpdateInterval = updateInterval;
           
            int nLog2 = Dsp.GetNLog2(bufferPoints);
            setup = Dsp.SetupFftD(nLog2);
            cTimer = new Timer(CalculateSpeed, null, calculateInterval, calculateInterval);
            motionManager.StartAccelerometerUpdates(queue, (data, error) =>
            {
                var value = data.Acceleration.X * data.Acceleration.X + data.Acceleration.Y * data.Acceleration.Y + data.Acceleration.Z * data.Acceleration.Z;
                lock (mGate)
                {
                    ringBuffer.Add(value);
                }
            });
            //motionManager.StartAccelerometerUpdates(queue, (data) =>
            //{
            //    lock (mGate)
            //    {
            //        ringBuffer.Add(data);
            //    }

            //});
        }
        void CalculateSpeed(object state)
        {
            if (ringBuffer.Count >= bufferPoints)
            {
                lock (mGate)
                {
                    var b = new double[bufferPoints];
                    ringBuffer.CopyTo(b, 0, bufferPoints);
                   // ringPos += ringDelta;
                    Task.Run(() =>
                    {
                        ProcessBuffer(b);

                    });
                }
            }
            else
                VelocityData?.Invoke(null);

        }
        public void ResetCalibration()
        {
            string filePath = Utilities.GetFileNameInDocuments(CalibrationFile);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            calibrationPoints = new List<Tuple<double, double>>();
            calibrationPoints.Add(new Tuple<double, double>(0, DefaultStep));
        }
        void LoadCalibration(bool clearCal)
        {
            calibrationPoints = new List<Tuple<double, double>>();
            string filePath = Utilities.GetFileNameInDocuments(CalibrationFile);
            try
            {
                if (File.Exists(filePath))
                {
                    if (clearCal)
                        File.Delete(filePath);
                    else
                    {
                        foreach (var l in File.ReadAllLines(filePath))
                        {
                            var s = l.Split(',').Select(i => Convert.ToDouble(i)).ToArray();
                            var data = Tuple.Create<double, double>(s[0], s[1]);
                            calibrationPoints.Add(data);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                calibrationPoints = new List<Tuple<double, double>>();
            }
            if (calibrationPoints.Count == 0)
                calibrationPoints.Add(new Tuple<double, double>(0, DefaultStep));
        }
        void SaveCalibration()
        {
            string filePath = Utilities.GetFileNameInDocuments(CalibrationFile);
            List<string> lines = new List<string>();

            foreach (var p in calibrationPoints)
            {
                lines.Add($"{p.Item1},{p.Item2}");
            }
            File.WriteAllLines(filePath, lines);
        }
        public Action<IEnumerable<FftPoint>> VelocityData;
        public void Stop()
        {
            cTimer.Dispose();
            motionManager.StopAccelerometerUpdates();
            setup.Dispose();
            

        }
        public void DoubleSpeed()
        {
           // motionManager.Speed *= 2;
        }
        public void SetCalibPoint(double stride, double velocity)
        {
            double calPoint = Math.Round(stride * 10) / 10;
            Tuple<double, double> newPoint = new Tuple<double, double>(calPoint, velocity / calPoint);
            var cp = calibrationPoints.Where(p => p.Item1 == calPoint).SingleOrDefault();
            if (cp != null)
                calibrationPoints.Remove(cp);
            calibrationPoints.Add(newPoint);
            calibrationPoints = calibrationPoints.OrderBy(p => p.Item1).ToList();
            SaveCalibration();
        }
        void ProcessBuffer(double[] buffer)
        {
            ComplexDataD cd = new ComplexDataD(bufferPoints);
            cd.Reals = buffer;
            Dsp.Fft(cd, this.setup, true);
            var mags = new double[bufferPoints];
            Dsp.GetMagnitudesSquared(cd, mags);
         
            List<FftPoint> fr = new List<FftPoint>();

            double sample = 1 / updateInterval;
            for (int i = 0; i < bufferPoints / 3; i++)
            {
                double f = i * sample / bufferPoints;
              
                fr.Add(new FftPoint() { Magnitude = mags[i], Frequency = f });
            }

            var data = fr
                .Where(r => r.Frequency < 5 &&  r.Frequency > 0.1)
                .OrderByDescending(r => r.Magnitude)
                .Take(1);// Take(3).ToList();
            foreach (var d in data)
            {
                Tuple<double, double> lastPoint = calibrationPoints.First();
                int index = 1;
                Tuple<double, double> next = lastPoint;
                while (index < calibrationPoints.Count)
                {
                    next = calibrationPoints[index];
                    if (next.Item1 > d.Frequency || index == calibrationPoints.Count - 1)
                    {
                        break;
                    }
                    else
                    {
                        lastPoint = next;
                        index++;
                    }
                }
                var point = Math.Abs(lastPoint.Item1 - d.Frequency) < Math.Abs(next.Item1 - d.Frequency) ? lastPoint : next;
                d.Velocity = UnitsConverter.Speed.MpsToMph(d.Frequency * point.Item2);
            }
            VelocityData?.Invoke(data);
           
        }
    }
    public class FftPoint
    {
        public double Magnitude { get; set; }
        public double Frequency { get; set; }
        public double Velocity { get; set; }
    }

   
    class MockCMMotionManager
    {
       Timer updateTimer;
        public MockCMMotionManager()
        {
            Speed = 5;
        }
       
        public bool AccelerometerAvailable
        {
            get { return true; }
        }

        public double Speed { get; set; }
        public  double AccelerometerUpdateInterval { get; set; }
        public void StartAccelerometerUpdates(NSOperationQueue queue, Action<double> handler)
        {
           
           // double x2 = Math.PI * 2 / 20;
            int k = 0;
            updateTimer = new Timer((state) =>
            {

                for (int i = 0; i < 256; i++)
                {
                    double v = Math.Sin(k * Math.PI * 2 / Speed);// + Math.Sin( i * x2);
                    handler(v);
                    k++;
                }

            }, null, 0, 100);
        }
        
        public void StopAccelerometerUpdates()
        {
            updateTimer.Dispose();
        }
    }
}
