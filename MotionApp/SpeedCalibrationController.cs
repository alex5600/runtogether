using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using CoreMotion;
using System.IO;
using System.Collections.Generic;
using MessageUI;
using System.Threading;
using System.Linq;

using System.Threading.Tasks;
using Shared.iOS.Code;
using Shared.iOS.Components;
using CoreGraphics;
using Shared;

namespace MotionApp
{
   
    public class SpeedCalibrationController : UIViewController
    {
        UILabel[] lbls;
        UIButton start;
        UIButton stop;
        UIButton speed;
        UIButton clear;
        SpeedCalculator speedCalculator;
        CustomStepper mStepper;
        UIButton set;
        public SpeedCalibrationController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        const string FileName = "Data.txt";

       
        string GetFileName()
        {
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return Path.Combine(documents, FileName);

        }

        //public override void ViewDidLoad()
        //{
        //    base.ViewDidLoad();

        //    process = new UIButton(new CoreGraphics.CGRect(0, 20,200, 40));
        //    process.SetTitleColor(UIColor.Red, UIControlState.Normal);
        //    process.SetTitle("Press", UIControlState.Normal);
        //    Add(process);
        //    process.TouchUpInside += Process_TouchUpInside;
        //    //string[] lines = File.ReadAllLines("Treadmill.txt");

        //    lbl = new UILabel(new CoreGraphics.CGRect(0, 60, UIScreen.MainScreen.ApplicationFrame.Width, 40));
        //    lbl.TextColor = UIColor.Red;
        //    View.BackgroundColor = UIColor.White;
        //    Add(lbl);
        //    motionManager = new CMMotionManager();
        //    motionManager.AccelerometerUpdateInterval = 0.01;
        //    int index = 0;

        //    aData = new double[bufferpoints];
        //    motionManager.StartAccelerometerUpdates(NSOperationQueue.CurrentQueue, (data, error) =>
        //    {

        //        var d =  data.Acceleration.X * data.Acceleration.X + data.Acceleration.Y * data.Acceleration.Y + data.Acceleration.Z * data.Acceleration.Z;
        //        //InvokeOnMainThread(() => lbl.Text = string.Format("Power {0:F3}, index {1}", d, index));
        //        //// if (error != null)
        //        ////   lbl.Text = error.Description;
        //        //dataList.Add(new Data() { Time = DateTime.Now.TimeOfDay, Value = d });
        //        aData[index++] = (float)d;

        //        if (index == bufferpoints - 1)
        //        {
        //            lock (mgate)
        //            {
        //                Task.Run(() =>
        //                {
        //                    ProcessBuffer(aData);
        //                });
        //                aData = new double[bufferpoints];
        //                index = 0;

        //            }
        //        }
        //    });
        //}

        int count;
        IEnumerable<FftPoint> lastData;
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            start = new UIButton(new CoreGraphics.CGRect(0, 20, 100, 40));
            start.SetTitleColor(UIColor.Red, UIControlState.Normal);
            start.SetTitle("Start", UIControlState.Normal);
            Add(start);
            start.TouchUpInside += (s, e) =>
            {
                lbls[0].Text = "Started";
                speedCalculator.Start();
            };

            stop = new UIButton(new CoreGraphics.CGRect(150, 20, 100, 40));
            stop.SetTitleColor(UIColor.Red, UIControlState.Normal);
            stop.SetTitle("Stop", UIControlState.Normal);
            Add(stop);
            stop.TouchUpInside += (s, e) =>
            {
                lbls[0].Text = "Stopped";
                speedCalculator.Stop();
            };

            clear = new UIButton(new CoreGraphics.CGRect(250, 20, 100, 40));
            clear.SetTitleColor(UIColor.Red, UIControlState.Normal);
            clear.SetTitle("Clear", UIControlState.Normal);
            Add(clear);
            clear.TouchUpInside += (s, e) =>
            {

                speedCalculator.ResetCalibration();
            };
            lbls = new UILabel[5];
            //string[] lines = File.ReadAllLines("Treadmill.txt");
            for (int i = 0; i < 5; i++)
            {
                var lbl = new UILabel(new CoreGraphics.CGRect(0, 70 + 24 * i, UIScreen.MainScreen.ApplicationFrame.Width, 30));
                lbl.TextColor = UIColor.Red;
                //  lbl.Font = UIFont.FromName(@"Arial Rounded MT Bold", size: 24);
                lbl.Font = lbl.Font.WithSize(26);
                View.BackgroundColor = UIColor.White;
                Add(lbl);
                lbls[i] = lbl;

            }
            mStepper = new CustomStepper();
            mStepper.Format = "{0:F1} mph";

            mStepper.StepInterval = 0.1f;
            mStepper.Frame = new CGRect(0, 200, 150, 40);
            mStepper.Value = 1;
            Add(mStepper);

            set = new UIButton(new CoreGraphics.CGRect(160, 200, 100, 40));
            set.SetTitleColor(UIColor.Red, UIControlState.Normal);
            set.SetTitle("Set", UIControlState.Normal);
            Add(set);
            set.TouchUpInside += (s, e) =>
            {
                if(lastData != null)
                speedCalculator.SetCalibPoint(lastData.First().Frequency, UnitsConverter.Speed.MphToMps(mStepper.Value));
               
            };

            speed = new UIButton(new CoreGraphics.CGRect(160, 250, 100, 40));
            speed.SetTitleColor(UIColor.Red, UIControlState.Normal);
            speed.SetTitle("Speed", UIControlState.Normal);
            Add(speed);
            speed.TouchUpInside += (s, e) =>
            {
                speedCalculator.DoubleSpeed();

            };

            speedCalculator = new SpeedCalculator();
            speedCalculator.VelocityData = (d) =>
            {
                InvokeOnMainThread(() =>
                {
                    lastData = d;
                    int index = 0;
                    if (d == null || d.Count() == 0)
                        lbls[0].Text = $"Collecting data, please wait ";
                    else
                    {
                        foreach (var l in d)
                        {
                            var lbl = lbls[index++];
                            lbl.Text = string.Format("Vel {0:F3}, stride {1:F3} ", l.Velocity, l.Frequency);
                        }
                    }

                });

            };


        }
     
       
    }
    class Data
    {
        public TimeSpan Time { get; set; }
        public double Value { get; set; }
    }

    class Freq
    {
        public double Magnituted { get; set; }
        public double Frequency { get; set; }
    }
}