// As One Technologies Platform
// 
// Confidential and Proprietary
// 
// This unpublished work contains valuable confidential and proprietary 
// information. Disclosure, use or reproduction outside of As One Technologies is prohibited 
// except as authorized in writing. This unpublished work by As One Technologies is protected 
// by the laws of the United States and other countries. If publication occurs, 
// the following notice shall apply:
// 
// Copyright � 2006 by As One Technologies
// All rights reserved.

//@author: Alex Fiksel

using System;
using System.Collections.Generic;
using System.IO;
using log4net;
using log4net.Core;
using log4net.Config;
using System.Xml;
using System.Text;
using System.Configuration;

namespace RunTogether.Logging
{
    public enum LogLevel
    {
        Debug,
        Info,
        Warning,
        Error,
        Off
    }
    /// <summary>
    /// Provides logging functionality. It is an adapter for log4net logging interface
    /// </summary>
    public class LoggingService
    {
        static Dictionary<string, ILog> m_Loggers = new Dictionary<string, ILog>();
        static ILog m_DefaultLogger;
        const string DefaultLogger = "Default";
        //static string configFile;
        static LoggingService()
        {
            m_DefaultLogger = LogManager.GetLogger(DefaultLogger);
            m_Loggers.Add(DefaultLogger, m_DefaultLogger);
           // ConfigurationFile = ConfigurationManager. SavigentConfigurationManager.ConfigurationFile;
            /* using config from bin instead of data folder 
            Configuration cfg = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationFile = cfg.FilePath; */
        }
		
       // public static string ConfigurationFile { get; set; }
        //public static void LoadConfiguration(string configFile)
        //{
        //    if (!string.IsNullOrEmpty(configFile))
        //    {
        //        ConfigurationFile = configFile;
        //    }
        //    XmlConfigurator.Configure(new FileInfo(ConfigurationFile));
        //}
        //public static void SetLogFileProperties(string path, string fileName)
        //{
        //    bool reconfig = false;
        //    string oldValue = (string)GlobalContext.Properties[LogConfig.TraceDirProperty];
        //    if (oldValue != path)
        //    {
        //        GlobalContext.Properties[LogConfig.TraceDirProperty] = path;
        //        reconfig = true;
        //    }
        //    oldValue = (string)GlobalContext.Properties[LogConfig.FileNameProperty];
        //    if (oldValue != fileName)
        //    {
        //        GlobalContext.Properties[LogConfig.FileNameProperty] = fileName;
        //        reconfig = true;
        //    }
        //    if (reconfig)
        //    {
        //        XmlConfigurator.Configure(new FileInfo(ConfigurationFile));
        //    }
        //}

        static bool IsFormat(string s)
        {
            return s.Contains("{");
        }
        static ILog GetLogger(string name)
        {
            ILog logger = null;
            if (name == null)
                return m_DefaultLogger;
            if (!m_Loggers.TryGetValue(name, out logger))
            {
                logger = LogManager.GetLogger(name);
                m_Loggers[name] = logger;
            }
            return logger;
        }

		public static LogLevel LogLevel 
		{ 
			get
			{
				log4net.Repository.Hierarchy.Hierarchy h = (log4net.Repository.Hierarchy.Hierarchy)log4net.LogManager.GetRepository();
				log4net.Repository.Hierarchy.Logger rootLogger = h.Root;
				LogLevel result = Logging.LogLevel.Off;
				if (rootLogger.Level == Level.Debug)
					result = Logging.LogLevel.Debug;
				else if (rootLogger.Level == Level.Info)
					result = Logging.LogLevel.Info;
				else if (rootLogger.Level == Level.Warn)
					result = Logging.LogLevel.Warning;
				else if (rootLogger.Level == Level.Error)
					result = Logging.LogLevel.Error;
				return result;
			}

			set
			{
				Level level = Level.Off;
				switch (value)
				{
					case LogLevel.Debug:
						level = Level.Debug;
						break;
					case LogLevel.Info:
						level = Level.Info;
						break;
					case LogLevel.Warning:
						level = Level.Warn;
						break;
					case LogLevel.Error:
						level = Level.Error;
						break;
					case LogLevel.Off:
						level = Level.Off;
						break;
					default:
						level = Level.Error;
						break;
				}

				ApplyLogLevel(level);
			}
		}

		private static void ApplyLogLevel(Level level)
		{
			foreach (var repository in LogManager.GetAllRepositories())
			{
				repository.Threshold = level;
				log4net.Repository.Hierarchy.Hierarchy hier = (log4net.Repository.Hierarchy.Hierarchy)repository;
				log4net.Core.ILogger[] loggers = hier.GetCurrentLoggers();
				foreach (log4net.Core.ILogger logger in loggers)
				{
					((log4net.Repository.Hierarchy.Logger)logger).Level = level;
				}
			}

			// Configure the root logger.
			log4net.Repository.Hierarchy.Hierarchy h = (log4net.Repository.Hierarchy.Hierarchy)log4net.LogManager.GetRepository();
			log4net.Repository.Hierarchy.Logger rootLogger = h.Root;
			rootLogger.Level = level;
		}

		public static bool IsDebugEnabled(string logger)
        {
            return GetLogger(logger).IsDebugEnabled;
        }
        public static bool IsInfoEnabled(string logger)
        {
            return GetLogger(logger).IsInfoEnabled;
        }
        public static bool IsWarnEnabled(string logger)
        {
            return GetLogger(logger).IsWarnEnabled;
        }
		public static bool IsErrorEnabled(string logger)
        {
            return GetLogger(logger).IsErrorEnabled;
        }
        public static void Debug(object message)
        {
            m_DefaultLogger.Debug(message);          
        }
        public static void Debug(string format, params object[] args)
        {
            m_DefaultLogger.DebugFormat(format, args);
        }
        public static void Debug(string sender, object message)
        {
       //     if (IsFormat(sender))
           //     m_DefaultLogger.DebugFormat(sender, message);
          //  else 
                GetLogger(sender).Debug(message);
        }
        public static void Debug(string sender,string format, params object[] args)
        {
            if (IsDebugEnabled(sender))
            {
                if (IsFormat(format))
                    GetLogger(sender).DebugFormat(format, args);
                else if (IsFormat(sender))
                {
                    if (args != null)
                    {
                        object[] a = new object[args.Length + 1];
                        a[0] = format;
                        args.CopyTo(a, 1);
                        m_DefaultLogger.DebugFormat(sender, a);
                    }
                    else
                        m_DefaultLogger.DebugFormat(sender, format);
                }
                else
                    GetLogger(sender).Debug(format);
            }
        }

        public static void Info(object message)
        {
            m_DefaultLogger.Info(message);
        }
        public static void Info(string sender, object message)
        {
            GetLogger(sender).Info(message);
        }
        public static void Info(string format, params object[] args)
        {
            m_DefaultLogger.InfoFormat(format, args);
        }
        public static void Info(string sender, string format, params object[] args)
        {
            if (IsInfoEnabled(sender))
            {
                if (IsFormat(format))
                    GetLogger(sender).InfoFormat(format, args);
                else if (IsFormat(sender))
                {
                    if (args != null)
                    {
                        object[] a = new object[args.Length + 1];
                        a[0] = format;
                        args.CopyTo(a, 1);
                        m_DefaultLogger.InfoFormat(sender, a);
                    }
                    else
                        m_DefaultLogger.InfoFormat(sender, format);
                }
                else
                    GetLogger(sender).Info(format);
            }
        }

        public static void Warn(object message)
        {
            m_DefaultLogger.Warn(message);
        }
        public static void Warn(string sender, object message)
        {
            GetLogger(sender).Warn(message);
        }
        public static void Warn(string format, params object[] args)
        {
            m_DefaultLogger.WarnFormat(format, args);
        }
        public static void Warn(string sender, string format, params object[] args)
        {
            if (IsWarnEnabled(sender))
            {
                if (IsFormat(format))
                    GetLogger(sender).WarnFormat(format, args);
                else if (IsFormat(sender))
                {
                    if (args != null)
                    {
                        object[] a = new object[args.Length + 1];
                        a[0] = format;
                        args.CopyTo(a, 1);
                        m_DefaultLogger.WarnFormat(sender, a);
                    }
                    else
                        m_DefaultLogger.WarnFormat(sender, format);
                }
                else
                    GetLogger(sender).Warn(format);
            }
        }

        public static void Error(string message)
        {
            m_DefaultLogger.Error(message);
        }
        public static void Error(Exception ex)
        {
            m_DefaultLogger.Error(ex);
        }
        public static void Error(Exception ex, string message)
        {
            m_DefaultLogger.Error(message,ex);
        }

        public static void Error(string sender, Exception ex)
        {
            m_DefaultLogger.Error(sender, ex);
        }
        public static void Error(string sender, Exception ex, object message)
        {
            GetLogger(sender).Error(message, ex);
        }

        public static void Error(string sender, object message)
        {
            Exception ex  = message as Exception;
            if (ex != null)
                m_DefaultLogger.Error(sender,ex );
            else
                GetLogger(sender).Error(message);
        }

        //public static void SetLoggingConnectionString(string connectionString)
        //{
        //    string configXml = File.ReadAllText(ConfigurationFile);
        //    if (configXml.Contains("%property{CONNECTION_STRING}"))
        //    {
        //        configXml = configXml.Replace("%property{CONNECTION_STRING}", connectionString);
        //        log4net.Config.XmlConfigurator.Configure(new MemoryStream((new ASCIIEncoding()).GetBytes(configXml)));
        //    }
        //}        
    }
    [Serializable]
    public class LogConfig
    {
        LogLevel m_Level;
        public const string FileNameProperty = "FILE_NAME";
        public const string TraceDirProperty = "TRACE_DIR";
       
        private Dictionary<string, object> m_Props = new Dictionary<string, object>();
        public Dictionary<string, object> Properties
        {
            get { return m_Props; }
        }
        public LogLevel Level {get; set;}
        
        //public bool RealTimeViewEnabled
        //{
        //    get { return m_RealtimeviewEnabled; }
        //    set { m_RealtimeviewEnabled = value; }
        //}
    }
    //public class LoggingServiceConfigurator : MarshalByRefObject
    //{

    //    public void Configure(LogConfig config)
    //    {
    //        // bool reconfig = false;
    //        foreach (KeyValuePair<string, object> pair in config.Properties)
    //        {
    //            object oldValue = GlobalContext.Properties[pair.Key];
    //            if (!(Object.Equals(oldValue, pair.Value)))
    //            {
    //                GlobalContext.Properties[pair.Key] = pair.Value;
    //            }
    //        }

    //        XmlDocument doc = new XmlDocument();
    //        string fileName = SavigentConfigurationManager.ConfigurationFile;
    //        doc.Load(fileName);
    //        XmlNode logNode = doc.SelectSingleNode("configuration/log4net");
    //        XmlNodeList levelNodes = logNode.SelectNodes("root/level");
    //        Level l = Level.Off;
    //        if (levelNodes != null)
    //        {
    //            foreach (XmlNode level in levelNodes)
    //            {
    //                switch (config.Level)
    //                {
    //                    case LogLevel.Debug:
    //                        l = Level.Debug;
    //                        break;
    //                    case LogLevel.Info:
    //                        l = Level.Info;
    //                        break;
    //                    case LogLevel.Warning:
    //                        l = Level.Warn;
    //                        break;
    //                    case LogLevel.Error:
    //                        l = Level.Error;
    //                        break;
    //                    case LogLevel.Off:
    //                        l = Level.Off;
    //                        break;
    //                    default:
    //                        l = Level.Error;
    //                        break;
    //                }
    //                level.Attributes["value"].Value = l.Name;

    //            }
    //        }
    //        XmlNode rootNode = doc.SelectSingleNode("configuration/log4net/root");
    //        if (config.RealTimeViewEnabled)
    //        {
    //            XmlNode debug = Xml.XmlHelper.AppendChild(rootNode, "level");
    //            Xml.XmlHelper.AppendAttribute(debug, "value", l.Name);
    //            XmlNode realtimeNode = Xml.XmlHelper.AppendChild(rootNode, "appender-ref");
    //            Xml.XmlHelper.AppendAttribute(realtimeNode, "ref", "RealtimeViewAppender");
        
    //        }
    //        //doc.Save(fileName);
    //        //XmlConfigurator.Configure(new FileInfo(fileName));
    //        XmlConfigurator.Configure(logNode as XmlElement);

    //    }
     
    //}

   


}
