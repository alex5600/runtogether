﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Web.Http;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Authentication;
using Microsoft.Azure.Mobile.Server.Config;
using Owin;
using RunTogether.DataObjects;
using System.Web.Http.Routing;
using System.Net.Http;

namespace RunTogetherService
{
    public partial class Startup
    {
        public static void ConfigureMobileApp(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            //For more information on Web API tracing, see http://go.microsoft.com/fwlink/?LinkId=620686 
            // config.EnableSystemDiagnosticsTracing();
            Register(config);
            new MobileAppConfiguration()
                .UseDefaultConfiguration()
                .ApplyTo(config);
            
            // Use Entity Framework Code First to create database tables based on your DbContext
           // Database.SetInitializer(new AppInitializer());

            // To prevent Entity Framework from modifying your database schema, use a null database initializer
            // Database.SetInitializer<RdTest1Context>(null);

            MobileAppSettingsDictionary settings = config.GetMobileAppSettingsProvider().GetMobileAppSettings();

            if (string.IsNullOrEmpty(settings.HostName))
            {
                // This middleware is intended to be used locally for debugging. By default, HostName will
                // only have a value when running in an App Service application.
                app.UseAppServiceAuthentication(new AppServiceAuthenticationOptions
                {
                    SigningKey = ConfigurationManager.AppSettings["SigningKey"],
                    ValidAudiences = new[] { ConfigurationManager.AppSettings["ValidAudience"] },
                    ValidIssuers = new[] { ConfigurationManager.AppSettings["ValidIssuer"] },
                    TokenHandler = config.GetAppServiceTokenHandler()
                });
            }
            app.UseWebApi(config);
        }

        public static void Register(HttpConfiguration config)
        {

            //config.Routes.Clear();
            config.Routes.MapHttpRoute(
               name: "MapByAction",
               routeTemplate: "api/{controller}/{action}/{id}", defaults: new { id = RouteParameter.Optional });
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional });

            //config.Routes.MapHttpRoute("DefaultApiWithId", "api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });
            //config.Routes.MapHttpRoute("DefaultApiWithAction", "api/{controller}/{action}");
            //config.Routes.MapHttpRoute("DefaultApiGet", "api/{controller}", new { action = "Get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            //config.Routes.MapHttpRoute("DefaultApiPost", "api/{controller}", new { action = "Post" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            //config.Routes.MapHttpRoute("DefaultApiPut", "api/{controller}", new { action = "Put" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            //config.Routes.MapHttpRoute("DefaultApiDelete", "api/{controller}", new { action = "Delete" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Delete) });

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
        }
    }

    //public class AppInitializer : DropCreateDatabaseIfModelChanges<Models.AppContext>
    //{

    //    public override void InitializeDatabase(AppContext context)
    //    {
    //        base.InitializeDatabase(context);
    //        //context.Database.ExecuteSqlCommand("ALTER DATABASE RunApp SET SINGLE_USER WITH ROLLBACK IMMEDIATE");

    //    }
    //    protected override void Seed(Models.AppContext context)
    //    {
    //        List<MyUser> todoItems = new List<MyUser>
    //        {
    //            new MyUser { Id = Guid.NewGuid().ToString(), Name = "Alex" , AccountInfo = new AccountInfo() { Name = 4} },
    //            new MyUser { Id = Guid.NewGuid().ToString(), Name = "Fiksel" , AccountInfo = new AccountInfo() { Name = 4} }

    //        };

    //        foreach (var  todoItem in todoItems)
    //        {
    //            context.MyUsers.Add(todoItem);
    //        }

    //        base.Seed(context);
    //    }
    //}
}

