﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Collections;

using System.Security.Cryptography;
using System.Data;
using System.Data.SqlClient;

namespace Storage.Storage
{
    public static class StorageUtils
    {
        const double GB = 1.0;
        const double MB = GB * 1024.0;
        const double KB = MB * 1024;
        const double B = KB * 1024;
        const double TB = GB / 1024;

        public static string[] TrimAndRemoveEmptyEntries(string[] sar)
        {
            List<string> list = new List<string>();
            foreach (string s in sar)
            {                
                string item = s.Trim();
                if (!IsEmpty(s))
                   list.Add(item);
            }
            return list.ToArray<string>();
        }

        public static bool IsEmpty(string s)
        {
            bool result = string.IsNullOrEmpty(s);
            if (!result)
                result = string.IsNullOrEmpty(s.Trim());
            return result;
        }

        public static string GenerateSqlInClause<T>(IEnumerable<T> list)
        {
            return GenerateSqlInClause(list, 0);
        }

        public static string GenerateSqlInClause<T>(IEnumerable<T> list, int startIndex)
        {
            return GenerateSqlInClause(list, startIndex, list.Count());
        }

        public static string GenerateSqlInClause<T>(IEnumerable<T> list, int startIndex, int noOfItems)
        {
            StringBuilder sb = new StringBuilder("IN (");
            bool quote = (typeof(T) == typeof(string) ||
                          typeof(T) == typeof(Guid));
            bool firstItem = true;
            int endIndex = Math.Min(startIndex + noOfItems, list.Count());

            for (int i = startIndex; i < endIndex; i++)
            {
                T item = list.ElementAt(i);
                if (!firstItem)
                    sb.Append(",");
                if (quote)
                    sb.AppendFormat("'{0}'", item.ToString());
                else
                    sb.Append(item.ToString());
                firstItem = false;
            }
            sb.Append(")");
            return sb.ToString();
        }

        public static string GenerateSqlInClause(IEnumerable<object> list)
        {
            if (list.Count() == 0)
                return null;
            Type type = list.First().GetType();
            StringBuilder sb = new StringBuilder("IN (");
            bool quote = (type == typeof(string) ||
                          type == typeof(Guid));
            bool firstItem = true;
            int startIndex = 0;
            int endIndex = list.Count();

            for (int i = startIndex; i < endIndex; i++)
            {
                object item = list.ElementAt(i);
                if (!firstItem)
                    sb.Append(",");
                if (quote)
                    sb.AppendFormat("'{0}'", item.ToString());
                else
                    sb.Append(item.ToString());
                firstItem = false;
            }
            sb.Append(")");
            return sb.ToString();
        }

        public static string MakeSqlStringLiteral(string s)
        {
            return s.Replace("'", "''");
        }

        public static string RemoveLineWithSubstring(string s, string ss, bool caseSensitive = false)
        {
            string[] arr = s.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            string ssLower = caseSensitive ? ss.ToLower() : null;
            StringBuilder sb = new StringBuilder();
            foreach (string line in arr)
            {
                if ((!caseSensitive && !line.Contains(ss)) || (caseSensitive && !line.ToLower().Contains(ssLower)))
                    sb.AppendLine(line);
            }
            return sb.ToString();
        }

        public static SpaceUsedInfo GetSpaceUsedInfo(DataManager dataManager)
        {
            string sql1 = @"
                SELECT 	SUM(CAST(dbf.size/128.0 AS FLOAT(53))) AS [DataSize], 
                        SUM(CAST(dbf.size/128.0 - CAST(FILEPROPERTY(dbf.name, 'SpaceUsed') AS int)/128.0 AS FLOAT(53))) AS SpaceAvailable
                FROM sys.database_files dbf
	                INNER JOIN sys.sysfiles s ON dbf.name = s.name
	                LEFT JOIN sys.filegroups f ON dbf.data_space_id = f.data_space_id
                WHERE dbf.[type] = 0";
            SpaceUsedInfo info = dataManager.ExecuteQuery<SpaceUsedInfo>(sql1).Single();
            //LoggingService.Debug(string.Format("Utils.GetSpaceUsedInfo. DataSize={0}; SpaceAvailable={1}",
            //    info.DataSize, info.SpaceAvailable));
            return info;
        }

        // Get total used space in GB
        public static double GetTotalUsedSpace(DataManager dataManager)
        {
            SpaceUsedInfo si = GetSpaceUsedInfo(dataManager);
            return (si.DataSize - si.SpaceAvailable) / 1024.0;
        }

        public static string[] ParsePath(string path, string separator)
        {
            string[] result = new string[2];
            int lastSeparator = separator != null && separator.Length == 1 && path != null ? path.LastIndexOf(separator) : 0;
            if (lastSeparator > 0)
            {
                result[0] = path.Substring(0, lastSeparator);
                result[1] = path.Substring(lastSeparator + 1);
            }
            else
            {
                result[0] = string.Empty;
                result[1] = path;
            }
            return result;
        }

        public static string ToSqlFormat(DateTimeOffset dt)
        {
            return dt.ToString("yyyy-MM-dd HH:mm:ss.fffffff zzz");
        }

        public static string ToHashValue(string input)
        {
            using (MD5 md5Hash = MD5.Create())
                return ToHashValue(input, md5Hash);
        }

        public static string ToHashValue(string input, MD5 md5Hash)
        {
            // Convert the path list into list of hashcodes, like the follwing: 0xC35B701E3815523070F5F9A93F5D47B8
            StringBuilder hashSb = new StringBuilder();
            byte[] data = ToHashValueBytes(input, md5Hash);
            for (int i = 0; i < data.Length; i++)
            {
                hashSb.Append(data[i].ToString("x2"));
            }
            return hashSb.ToString();
        }

        public static byte[] ToHashValueBytes(string input)
        {
            using (MD5 md5Hash = MD5.Create())
                return ToHashValueBytes(input, md5Hash);
        }

        public static byte[] ToHashValueBytes(string input, MD5 md5Hash)
        {
            return md5Hash.ComputeHash(Encoding.Unicode.GetBytes(input.ToLower()));
        }

        public static string ToGb(double number, DataSpaceUoM uom)
        {
            double koef = 1.0;
            switch (uom)
            {
                case DataSpaceUoM.GB:
                    koef = GB; break;
                case DataSpaceUoM.MB:
                    koef = MB; break;
                case DataSpaceUoM.KB:
                    koef = KB; break;
                case DataSpaceUoM.B:
                    koef = B; break;
                case DataSpaceUoM.TB:
                    koef = TB; break;
            }
            return (number / koef).ToString("N1");
        }

        public static SqlParameter PrepareInternalKeyParameter(IEnumerable<InternalKey> source)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id", typeof(Guid)));
            dt.Columns.Add(new DataColumn("Date", typeof(DateTimeOffset)));
            dt.Columns.Add(new DataColumn("Id2", typeof(long)));

            foreach (InternalKey key in source)
            {
                dt.Rows.Add(key.Id, key.Date, key.Id2);
            }

            SqlParameter par = new SqlParameter("@InternalKey", SqlDbType.Structured);
            par.Value = dt;
            par.TypeName = "InternalKey";

            return par;
        }

        #region Helper Types

        class SpaceUsed
        {
            public int Size { get; set; }   // in MB
            public int Type { get; set; }
            public int FreeSpace { get; set; }
        }

        #endregion Helper Types

    }

    [Serializable]
    public class SpaceUsedInfo
    {
        public double DataSize { get; set; } //database size in MB
        public double SpaceAvailable { get; set; } //allocated space - db size in MB
    }

    public enum DataSpaceUoM { B, KB, MB, GB, TB };

    public class InternalKey
    {
        public Guid? Id { get; set; }
        public DateTimeOffset? Date { get; set; }
        public long? Id2 { get; set; }

        public InternalKey()
        {
        }

        public InternalKey(Guid id, DateTimeOffset? date = null)
        {
            Id = id;
            Date = date;
        }

        public InternalKey(long id2)
        {
            Id2 = id2;
        }
    }

}
