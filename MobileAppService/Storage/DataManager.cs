﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Security.Principal;
using System.ComponentModel;
using System.Data;

namespace Storage.Storage
{


    //public interface ITimeRange
    //{
    //    bool HasStart { get; }
    //    DateTimeOffset Start { get; }
    //    bool HasEnd { get; }
    //    DateTimeOffset End { get; }
    //    TimeSpan Duration { get; }
    //    bool HasInside(DateTimeOffset timestamp);
    //    object Value { get; }
    //}

    //public interface ITimeline : ITimeRange
    //{
    //    bool HasRanges { get; }
    //    List<ITimeRange> TimeRanges { get; }
    //    ITimeRange GetMaxRange();
    //}
    public interface ITimeline
    {

    }
    public class DataManager : IDisposable
    {
        #region Fields

        //string m_User;
        static object mSync = new object();
#if SQL_LOG
        public const string IsLogSqlEnabled = "Debug.IsLogSqlEnabled";
#endif
        //[ThreadStatic]
        //static short m_SessionId;
        public const string UpdateLockResource = "_Platform_DbUpdater";
        public const string DataProcessingLockResource = "_Platform_DataProcessing";
        //const string LOGGER = "DataManager";

        #endregion Fields

        #region Constructors

        public DataManager()
            : this(new DatabaseSettings())
        {
        }

        public DataManager(DatabaseSettings settings)
        {
            Settings = settings;
            // Default user
            //WindowsIdentity wi = WindowsIdentity.GetCurrent();
            //User = wi.Name;
            Options = new SmartDataContextOptions();
        }

        #endregion Constructors

        #region Public Properties

        //public string User
        //{
        //    get { return m_User; }
        //    set
        //    {
        //        m_User = value;
        //    }
        //}

        public DatabaseSettings Settings { get; set; }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public List<SqlInfo> LastCommandInfo { get; private set; }
        public SmartDataContextOptions Options { get; private set; }

        #endregion Public Properties

        #region Public Methods

        public virtual IEnumerable<TResult> ExecuteQuery<TResult>(string query, params object[] parameters)
        {
            return ExecuteQuery<TResult>(null, query, false, parameters);
        }

        public virtual IEnumerable<TResult> ExecuteQuery<TResult>(ITimeline timeline, string query, params object[] parameters)
        {
            return ExecuteQuery<TResult>(timeline, query, false, parameters);
        }

        public virtual IEnumerable<TResult> ExecuteQuery<TResult>(string query, bool useTransaction, params object[] parameters)
        {
            return ExecuteQuery<TResult>(null, query, useTransaction, parameters);
        }

        public virtual IEnumerable<TResult> ExecuteQuery<TResult>(ITimeline timeline, string query, bool useTransaction, params object[] parameters)
        {
            var options = Options.GetClone();
            options.TransactionUse = useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                IEnumerable<TResult> result = ctx.ExecuteQuery<TResult>(query, parameters);
                LastCommandInfo = ctx.GetSqlInfo();
                if (useTransaction) ctx.Complete();
                return result;
            }
        }

        public virtual DataTable ExecuteQuery(string query, params SqlParameter[] parameters)
        {
            return ExecuteQuery(null, query, false, parameters);
        }
       
        public virtual DataTable ExecuteQuery(ITimeline timeline, string query, params SqlParameter[] parameters)
        {
            return ExecuteQuery(query, false, parameters);
        }

        public virtual DataTable ExecuteQueryEx(string query, Dictionary<string, Type> replaceTypes = null, params SqlParameter[] parameters)
        {
            return ExecuteQueryEx(null, query, replaceTypes, parameters);
        }
        public virtual DataTable ExecuteQueryEx(ITimeline timeline, string query, Dictionary<string, Type> replaceTypes = null, params SqlParameter[] parameters)
        {
            var options = Options.GetClone();
            options.TransactionUse = SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                DataTable result = ctx.ExecuteQueryEx(query, replaceTypes, parameters);
                LastCommandInfo = ctx.GetSqlInfo();
                return result;
            }
        }
        public virtual DataTable ExecuteQuery(string query, bool useTransaction, params SqlParameter[] parameters)
        {
            return ExecuteQuery(null, query, useTransaction, parameters);
        }

        public virtual DataTable ExecuteQuery(ITimeline timeline, string query, bool useTransaction, params SqlParameter[] parameters)
        {
            var options = Options.GetClone();
            options.TransactionUse = useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                DataTable result = ctx.ExecuteQuery(query, parameters);
                LastCommandInfo = ctx.GetSqlInfo();
                if (useTransaction) ctx.Complete();
                return result;
            }
        }

        public int ExecuteCommand(string query, params object[] parameters)
        {
            return ExecuteCommand(query, false, parameters);
        }

        public int ExecuteCommand(string query, bool useTransaction, params object[] parameters)
        {
            var options = Options.GetClone();
            options.TransactionUse = useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                int result = ctx.ExecuteCommand(query, parameters);
                LastCommandInfo = ctx.GetSqlInfo();
                if (useTransaction) ctx.Complete();
                return result;
            }
        }

        /// <summary>
        /// Execute INSERT command.
        /// </summary>
        /// <param name="table">database table where to insert</param>
        /// <param name="fields">string containing all fields names, nullable and not nullable; white space separated</param>
        /// <param name="values">values for all fields; one field takes one argument</param>
        /// <example>
        /// table: "myTable"
        /// fields: "a, b, c, x, y, z" - comma separated list of fields; x, y, z may be null
        /// values: A, B, C, X, Y, Z - some properties that return values.
        /// Alltogether:
        /// ExecuteInsertCommand("myTable", "a b c x y z", A, B, C, X, Y, Z);
        /// </example>
        /// <remarks>Do not include identity fields in the list</remarks>
        /// <returns>Number of inserted rows</returns>        
        public int ExecuteInsertCommand(string table, string fields, params object[] values)
        {
            //return ExecuteInsertCommand(table, fields, true, values);
            return ExecuteInsertCommand(table, fields, false, values);
        }

        public int ExecuteInsertCommand(string table, string fields, bool useTransaction, params object[] values)
        {
            var options = Options.GetClone();
            options.TransactionUse = useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                int result = ctx.ExecuteInsertCommand(table, fields, values);
                LastCommandInfo = ctx.GetSqlInfo();
                if (useTransaction) ctx.Complete();
                return result;
            }
        }

        public int ExecuteInsertCommand(string table, string fields, out int identity, params object[] values)
        {
            //return ExecuteInsertCommand(table, fields, out identity, true, values);
            return ExecuteInsertCommand(table, fields, out identity, false, values);
        }

        public int ExecuteInsertCommand(string table, string fields, out int identity, bool useTransaction, params object[] values)
        {
            var options = Options.GetClone();
            options.TransactionUse = useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                int result = ctx.ExecuteInsertCommand(table, fields, out identity, values);
                LastCommandInfo = ctx.GetSqlInfo();
                if (useTransaction) ctx.Complete();
                return result;
            }
        }

        /// <summary>
        /// Execute UPDATE command.
        /// </summary>
        /// <param name="table">database table.</param>
        /// <param name="felds">string containing all fields names for WHERE and SET clause, nullable and not nullable, comma separated</param>
        /// <param name="whereFieldsNo">Number of fields in WHERE clause </param>
        /// <param name="values">values for all fields; one field takes one argument; whereFields go first</param>
        /// <example>
        /// table: "myTable"
        /// fields: "name, version, a, b, c, x, y, z" - comma separated list of fields; x, y, z may be null
        /// values: A, B, C, X, Y, Z - some properties that return values.
        /// whereFieldsNo: 2
        /// Alltogether:
        /// ExecuteUpdateCommand("myTable", "name, version, a b c x y z", 2, name, version, A, B, C, X, Y, Z);
        /// There will be updated all records of myTable with [name]='name' and [version]='version'.
        /// </example>
        /// <returns>Number of updated rows</returns>
        public int ExecuteUpdateCommand(string table, string fields, int whereFieldsNo, params object[] values)
        {
            //return ExecuteUpdateCommand(table, fields, whereFieldsNo, true, values);
            return ExecuteUpdateCommand(table, fields, whereFieldsNo, false, values);
        }

        public int ExecuteUpdateCommand(string table, string fields, int whereFieldsNo, bool useTransaction, params object[] values)
        {
            var options = Options.GetClone();
            options.TransactionUse = useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                int result = ctx.ExecuteUpdateCommand(table, fields, whereFieldsNo, values);
                LastCommandInfo = ctx.GetSqlInfo();
                if (useTransaction) ctx.Complete();
                return result;
            }
        }

        public void ExecuteUpdateCommandEx(string table, string fields, int whereFieldsNo, params object[] values)
        {
            int updated = ExecuteUpdateCommand(table, fields, whereFieldsNo, values);
            if (updated != 1)
                throw new Exception("Update command failed.");
        }

        /// <summary>
        /// Execute UPDATE command; if it fails, execute INSERT command.
        /// </summary>
        public int ExecuteUpdateInsertCommand(string table, string fields, int whereFieldsNo, params object[] values)
        {
            return ExecuteUpdateInsertCommand(table, fields, whereFieldsNo, false, values);
        }

        public int ExecuteUpdateInsertCommand(string table, string fields, int whereFieldsNo, bool useTransaction, params object[] values)
        {
            var options = Options.GetClone();
            options.TransactionUse = useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                int result = ctx.ExecuteUpdateInsertCommand(table, fields, whereFieldsNo, values);
                LastCommandInfo = ctx.GetSqlInfo();
                if (useTransaction) ctx.Complete();
                return result;
            }
        }

        /// <summary>
        /// Executes a stored procedure that retuns results.
        /// </summary>
        /// <param name="proc">stored procedure name</param>
        /// <param name="procParameters">parameters; comma separated. Parameters may be prefixed by '@' character or not prefixed.</param>
        /// <param name="values">values for all parameters; one field takes one argument</param>
        /// <example>
        /// proc: "myStoredProcedure"
        /// fields: "@par1, @par2" - parameters; 
        /// values: X, Y - actual parameter values.
        /// Alltogether:
        /// ExecuteProc("myStoredProcedure", "@par1, @par2", X, Y);
        /// </example>
        /// <returns>Number of updated rows</returns>
        public IEnumerable<TResult> ExecuteProc<TResult>(string proc, string procParameters, params object[] values)
        {
            return ExecuteProc<TResult>(proc, procParameters, false, values);
        }

        public IEnumerable<TResult> ExecuteProc<TResult>(string proc, string procParameters, bool useTransaction, params object[] values)
        {
            var options = Options.GetClone();
            options.TransactionUse = useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None;
            using (SmartDataContext ctx = CreateContext(options))
            {
                IEnumerable<TResult> result = ctx.ExecuteProc<TResult>(proc, procParameters, values).ToList<TResult>();
                LastCommandInfo = ctx.GetSqlInfo();
                if (useTransaction) ctx.Complete();
                return result;
            }
        }

        public void ExecuteTransaction(Func<bool> f, string description)
        {
            ExecuteTransaction(f, null, description);
        }

        public void ExecuteTransaction(Func<bool> f, Action cleanup, string description)
        {
            var options = Options.GetClone();
            options.TransactionUse = SmartTransactionUse.Shared;
            using (SmartDataContext ctx = CreateContext(options))
            {
                ctx.ExecuteTransaction(f, cleanup, description);
            }
        }

        //public void ExecuteTransaction(Action a, string description)
        //{
        //    ExecuteTransaction(() => { a(); return true; }, description);
        //    //using (SmartDataContext ctx = CreateContext(SmartTransactionUse.Shared))
        //    //{
        //    //    ctx.ExecuteTransaction(a, description);
        //    //}
        //}

        public SmartDataContext CreateContext(bool useTransaction = false)
        {
            return CreateContext(useTransaction ? SmartTransactionUse.Exclusive : SmartTransactionUse.None);
        }

        public SmartDataContext CreateContext(SmartTransactionUse useTransaction)
        {
            return new SmartDataContext(this, new SmartDataContextOptions { TransactionUse = useTransaction });
        }

        public SmartDataContext CreateContext(SmartDataContextOptions options)
        {
            return new SmartDataContext(this, options);
        }

        public bool CheckConnection()
        {
            using (SmartDataContext ctx = CreateContext())
            {
                try
                {
                    Options.NoErrorRecovery = true;
                    GetServerDate();
                    return true;
                }
                catch (Exception ex)
                {
                    string errorText = @"CheckConnection failed. Possible errors:
1). Config.xml file refers to not-existing database server, or database.
2). You did not run the appropriate scripts for databases.
3). SQL Server Authentication is not set to the mixed mode (Windows and SQL Server Authentication).
4). You restored the database from a backup and did not run the scripts 'create_user.sql', and 'repository_permissions.sql'.";
                  //  LoggingService.Error(DbLogger.DbLayer, errorText);
                    return false;
                }
            }
        }

        public DateTimeOffset GetServerDate()
        {
            string sql = "SELECT SYSDATETIMEOFFSET()";
            return ExecuteQuery<DateTimeOffset>(sql).Single();
        }

        

        public string GetDatabaseSettingsValue(string key)
        {
            string sql = "SELECT [Value] FROM [dbo].[DatabaseSettings] WHERE [Key] = @key";
            return ExecuteQuery<string>(sql, new SqlParameter("@key", key)).SingleOrDefault();
        }

        public string GetDatabaseVersion()
        {
            //string sql = "SELECT [Value] FROM [dbo].[DatabaseSettings] WHERE [Key] = 'CurrentDatabaseVersion'";
            //return ExecuteQuery<string>(sql).SingleOrDefault();
            return GetDatabaseSettingsValue("CurrentDatabaseVersion");
        }

       

        public void ExecuteBatchScript(string script)
        {
            using (SmartDataContext ctx = CreateContext())
            {
                ctx.ExecuteBatchScript(script);
            }
        }

        public void BulkInsert(string tableName, DataTable data, SmartDataContextOptions options = null)
        {
            if (options == null)
                options = new SmartDataContextOptions();
            using (SmartDataContext ctx = CreateContext(options))
            {
                ctx.BulkInsert(tableName, data);
            }
        }

        public bool LockAndExecute(string resource, SmartLockOptions lockOptions, Action a)
        {
            using (SmartDataContext ctx = CreateContext())
            {
                return ctx.LockAndExecute(resource, lockOptions, a);
            }
        }

        public bool LockAndExecute(IEnumerable<string> resources, SmartLockOptions lockOptions, Action a, string tag = null)
        {
            using (SmartDataContext ctx = CreateContext())
            {
                return ctx.LockAndExecute(resources, lockOptions, a, tag);
            }
        }

        #endregion Public Methods

        #region IDisposable Members

        public void Dispose()
        {
            // So far nothing to dispose.
        }
        #endregion IDisposable Members

        #region Private Methods

        // Obsolete?
        //private static void LogDebugDynamicStatementImpl(string logger, SqlStmtType stmtType, string sql, object[] parameters)
        //{
        //    if (logger.Equals("auto", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        logger = sql.IndexOf("useraction", StringComparison.InvariantCultureIgnoreCase) >= 0
        //            ? "PORTALDAL" : "DAL";
        //    }
        //    if (LoggingService.IsDebugEnabled(logger))
        //    {
        //        if (stmtType == SqlStmtType.QueryReader)
        //        {
        //            foreach (SqlParameter p in parameters)
        //            {
        //                string pv = "<NULL>";
        //                if (p != null)
        //                {
        //                    pv = p.Value.ToString();
        //                    if (p.Value is string)
        //                        pv = string.Format("'{0}'", pv);
        //                }
        //                sql = sql.Replace(p.ParameterName, pv);
        //            }
        //        }
        //        else if (stmtType == SqlStmtType.ProcReader)
        //        {
        //            StringBuilder sb = new StringBuilder();
        //            foreach (SqlParameter p in parameters)
        //            {
        //                string pv = "<NULL>";
        //                if (p != null)
        //                {
        //                    pv = p.Value.ToString();
        //                    if (p.Value is string)
        //                        pv = string.Format("'{0}'", pv);
        //                }
        //                if (sb.Length > 0)
        //                    sb.Append(",");
        //                sb.AppendFormat(" {0}={1}", p.ParameterName, pv);
        //            }
        //            sql += sb.ToString();
        //        }
        //        else
        //        {
        //            object[] prms = parameters.ToArray();
        //            for (int i = 0; i < parameters.Length; i++)
        //            {
        //                if (parameters[i] == null)
        //                {
        //                    string errorMsg = string.Format("Error while constructing dynamic SQL statement. Parameter #{0} is null. Base sql is: '{1}'.",
        //                        i, sql);
        //                    LoggingService.Error(LOGGER, errorMsg);
        //                }
        //                string prmString = parameters[i].ToString();
        //                if (prmString.Length > 50)
        //                    prmString = prmString.Substring(0, 50) + "...";
        //                if (!parameters[i].GetType().IsPrimitive)
        //                    prms[i] = string.Format("'{0}'", prmString);
        //            }
        //            sql = string.Format(sql, prms);
        //        }
        //        string result = string.Format("Dynamic {0}: {1}",
        //            Enum.GetName(typeof(SqlStmtType), stmtType),
        //            sql);

        //        LoggingService.Debug(logger, result);
        //    }
        //}

        #endregion Private Methods

    }
}
