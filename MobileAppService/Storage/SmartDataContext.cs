﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Transactions;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Shared.Core;

namespace Storage.Storage
{
   

    public enum SqlStmtType
    {
        Insert,
        Update,
        Procedure,
        Query,
        Command,
        QueryReader,
        ProcReader
    }

    public class SmartDataContext : IDisposable
    {
        #region Fields & Properties

        public DataManager Manager { get; set; }
        public SmartDataContextOptions Options { get; set; }
        private DatabaseSettings m_Settings = null;
        internal StatisticsHelper StatisticsHelper { get; set; }
        internal string ConnectionString { get; private set; }
        private DataContext m_Context = null;
        static TypesCache m_TypesCache = new TypesCache();
        Queue<SqlInfo> m_InfoQueue = new Queue<SqlInfo>();
        const int MAX_INFO_QUEUE_COUNT = 100;
        TransactionScope m_Transaction = null;
        TransactionState m_TransactionState = TransactionState.NoTransaction;
        [ThreadStatic]  // the top SmartDataContext in chain in the given thread (not neccesarilly holding transaction)
        static
        SmartDataContext m_OuterContext = null;
        [ThreadStatic]
        static SmartDataContext m_ExclusiveLockHolder = null;
        bool m_IsAmbientTx = false;
        const bool UsingXLock = false;
        const int PauseBeforeNewAttemptInExecTxMs = 50; // 50 ms
        public QueryInfo QueryInfo { get; private set; }
        readonly TimeSpan LONG_QUERY_MEASUREMENT_TIMEOUT = TimeSpan.Parse("00:01:00");
        //public List<SmartLock> ActiveLocks { get; private set; }
#if MYDEBUG
        static ConcurrentDictionary<string, SmartLockResourceInfo> m_LocksMap = new ConcurrentDictionary<string, SmartLockResourceInfo>();
#endif

        public DataContext Context
        {
            get
            {
                if (m_Context == null)
                {
                    m_Context = new DataContext(ConnectionString);
                }
                return m_Context;
            }
        }

        /// <summary>
        ///  If set then overwrite the settings defined in Manager.
        /// </summary>
        public DatabaseSettings Settings
        {
            get { return m_Settings; }
            set
            {
                m_Settings = value;
                if (m_Context != null)
                    m_Context.Dispose();
                ConnectionString = m_Settings.ConnectionString;
            }
        }

        public List<QueryStatistics> AllStatistics { get; private set; }

        public System.Transactions.IsolationLevel IsolationLevel
        {
            get
            {
                return m_OuterContext.Options.IsolationLevel.HasValue
                    ? m_OuterContext.Options.IsolationLevel.Value
                    : Settings.IsolationLevel.HasValue
                        ? Settings.IsolationLevel.Value
                        : SmartDataContextOptions.DefaultIsolationLevel;
            }
        }

        #endregion Fields & Properties

        #region Constructors

        public SmartDataContext(DataManager manager, SmartDataContextOptions options)
        {
            if (m_OuterContext == null)
                m_OuterContext = this;
            Manager = manager;
            Settings = Manager.Settings;
            Options = options ?? new SmartDataContextOptions();
            AllStatistics = new List<QueryStatistics>();
            m_IsAmbientTx = (Transaction.Current != null);
            //ActiveLocks = new List<SmartLock>();
            PrepareTransaction();
        }

        #endregion Constructors

        #region Public Methods

        public int ExecuteCommand(string query, params object[] parameters)
        {
            return ExecuteCommand(new QueryDescriptor(query), parameters);
        }

        public int ExecuteCommand(QueryDescriptor descriptor, params object[] parameters)
        {
            using (new StatisticsHelper(descriptor, this))
            {
                int result = 0;
#if SQL_LOG
                LogDebugDynamicStatement("AUTO", SqlStmtType.Command, descriptor.Query, parameters);
#endif
                if (descriptor.Implementation == QueryImplementation.LinqByPass)
                    result = ExecuteCommandImplement(descriptor.Query, parameters);
                else if (descriptor.Implementation == QueryImplementation.AdoNet)
                    result = ExecuteCommandImplement2(descriptor.Query, parameters);
                return result;
            }
        }

        public IEnumerable<TResult> ExecuteQuery<TResult>(string query, params object[] parameters)
        {
            return ExecuteQuery<TResult>(new QueryDescriptor(query), parameters);
        }

        public IEnumerable<TResult> ExecuteQuery<TResult>(SqlConnection conn, string query, params object[] parameters)
        {
            return ExecuteQueryImplement<TResult>(conn, query, parameters);
        }

        public IEnumerable<TResult> ExecuteQuery<TResult>(QueryDescriptor descriptor, params object[] parameters)
        {
            using (new StatisticsHelper(descriptor, this))
            {
                IEnumerable<TResult> result = null;
#if SQL_LOG
                LogDebugDynamicStatement("AUTO", SqlStmtType.Query, descriptor.Query, parameters);
#endif
                if (descriptor.Implementation == QueryImplementation.LinqByPass)
                    result = ExecuteQueryImplement<TResult>(descriptor.Query, parameters);
                else if (descriptor.Implementation == QueryImplementation.AdoNet)
                    result = ExecuteQueryImplement2<TResult>(descriptor, parameters);
                return result;
            }
        }

        public DataTable ExecuteQuery(string query, params SqlParameter[] parameters)
        {
            return ExecuteQuery(new QueryDescriptor(query), parameters);
        }

        public DataTable ExecuteQueryEx(string query, Dictionary<string,Type> replaceTypes, params SqlParameter[] parameters)
        {

            return ExecuteQueryImplementEx(query, replaceTypes,parameters);
        }
        public DataTable ExecuteQuery(QueryDescriptor descriptor, params SqlParameter[] parameters)
        {
            using (new StatisticsHelper(descriptor, this))
            {
                return ExecuteQueryImplement(descriptor.Query, parameters);
            }
        }

        public DataTable ExecuteProc(QueryDescriptor descriptor, params SqlParameter[] parameters)
        {
            using (new StatisticsHelper(descriptor, this))
            {
                return ExecuteProcImplement(descriptor.Query, parameters);
            }
        }

        public int ExecuteInsertCommand(string table, string fields, params object[] values)
        {
            QueryDescriptor desc = new QueryDescriptor("", string.Format("Insert into table '{0}'", table), QueryImplementation.AdoNet);
            return ExecuteInsertCommand(desc, table, fields, values);
        }

        /// <summary>
        /// Builds and execute INSERT command with nullable values.
        /// </summary>
        /// <param name="command">SQL command containing INSERT and INTO clauses.</param>
        /// <param name="fields">string containing all fields names, nullable and not nullable; white space separated</param>
        /// <param name="values">values for all fields; one field takes one argument</param>
        /// <example>
        /// command: "INSERT INTO t""
        /// fields: "a b c x y z" - fields; x, y, z may be null
        /// values: A, B, C, X, Y, Z - some properties that return values.
        /// Alltogether:
        /// ExecuteInsertCommand("INSERT INTO t", "a b c x y z", A, B, C, X, Y, Z);
        /// </example>
        /// <returns>Number of inserted rows</returns>
        public int ExecuteInsertCommand(QueryDescriptor descriptor, string table, string fields, params object[] values)
        {
            using (new StatisticsHelper(descriptor, this))
            {
                return ExecuteInsertCommandImplement(table, fields, values);
            }
        }

        public int ExecuteInsertCommand(string table, string fields, out int identity, params object[] values)
        {
            QueryDescriptor desc = new QueryDescriptor("", string.Format("Insert into table '{0}'", table), QueryImplementation.AdoNet);
            return ExecuteInsertCommand(desc, table, fields, out identity, values);
        }

        public int ExecuteInsertCommand(QueryDescriptor descriptor, string table, string fields, out int identity, params object[] values)
        {
            using (new StatisticsHelper(descriptor, this))
            {
                return ExecuteInsertCommandImplement(table, fields, out identity, values);
            }
        }

        public void ExecuteInsertCommandEx(string table, string fields, params object[] values)
        {
            int inserted = ExecuteInsertCommandImplement(table, fields, values);
            if (inserted != 1)
                throw new Exception("Insert command failed.");
        }

        public int ExecuteUpdateCommand(string table, string fields, int whereFieldsNo, params object[] values)
        {
            QueryDescriptor desc = new QueryDescriptor("", string.Format("Update table '{0}'", table), QueryImplementation.AdoNet);
            return ExecuteUpdateCommand(desc, table, fields, whereFieldsNo, values);
        }

        /// <summary>
        /// Builds and execute UPDATE command with nullable values.
        /// </summary>
        /// <param name="command">SQL command containing UPDATE and WHERE clauses.</param>
        /// <param name="felds">string containing all fields names for WHERE and SET clause, nullable and not nullable comma separated</param>
        /// <param name="whereFieldsNo">Number of fields in WHERE clause </param>
        /// <param name="values">values for all fields; one field takes one argument; whereFields go first</param>
        /// <example>
        /// command: "UPDATE t"
        /// fields: "name, version, a b c x y z" - fields; x, y, z may be null
        /// values: A, B, C, X, Y, Z - some properties that return values.
        /// whereFieldsNo: 2
        /// Alltogether:
        /// ExecuteUpdateCommand("UPDATE t", "name, version, a b c x y z", 2, name, version, A, B, C, X, Y, Z);
        /// </example>
        /// <returns>Number of updated rows</returns>
        public int ExecuteUpdateCommand(QueryDescriptor descriptor, string table, string fields, int whereFieldsNo, params object[] values)
        {
            using (new StatisticsHelper(descriptor, this))
            {
                return ExecuteUpdateCommandImplement(table, fields, whereFieldsNo, values);
            }
        }

        public void ExecuteUpdateCommandEx(string table, string fields, int whereFieldsNo, params object[] values)
        {
            int updated = ExecuteUpdateCommandImplement(table, fields, whereFieldsNo, values);
            if (updated != 1)
                throw new Exception("Update command failed.");
        }

        public int ExecuteUpdateInsertCommand(string table, string fields, int whereFieldsNo, params object[] values)
        {
            return ExecuteUpdateInsertCommand(new QueryDescriptor(), table, fields, whereFieldsNo, values);
        }

        public int ExecuteUpdateInsertCommand(QueryDescriptor descriptor, string table, string fields, int whereFieldsNo, params object[] values)
        {
            int result = 0;
            ExecuteTransaction(() =>
            {
                using (new StatisticsHelper(descriptor, this))
                {
                    result = ExecuteUpdateCommandImplement(table, fields, whereFieldsNo, values);
                    if (result <= 0)
                    {
                        result = ExecuteInsertCommandImplement(table, fields, values);
                    }
                    return true;
                }
            }, "ExecuteUpdateInsertCommand");
            return result;
        }

        public IEnumerable<TResult> ExecuteProc<TResult>(string proc, string procParameters, params object[] values)
        {
            QueryDescriptor desc = new QueryDescriptor(proc,
                string.Format("Executing Sored Procedure '{0}'", proc),
                QueryImplementation.LinqByPass);
            return ExecuteProc<TResult>(desc, procParameters, values);
        }

        public IEnumerable<TResult> ExecuteProc<TResult>(QueryDescriptor descriptor, string procParameters, params object[] values)
        {
            using (new StatisticsHelper(descriptor, this))
            {
                return ExecuteProcImplement<TResult>(descriptor.Query, procParameters, values);
            }
        }

        // Obsolete?
        //public void LogDebugStatistics()
        //{
        //    LogDebugStatistics("DbStat");
        //}

        //public void LogDebugStatistics(string logger)
        //{            
        //    try
        //    {
        //        if (LoggingService.IsDebugEnabled(logger))
        //        {
        //            // Determine is it worth logging at all: it's worth if we have at least one sql with exec time > threshold
        //            TimeSpan threshold = TimeSpan.FromMilliseconds(Options.LogStatisticsThresholdMs);
        //            IEnumerable<QueryStatistics> allStatistics = GetSqlInfo().Select<SqlInfo, QueryStatistics>(p => p.Statistics)
        //                .Where(qs => qs != null);
        //            if (allStatistics.Any(qs => qs.SqlTime > threshold))
        //            {
        //                foreach (QueryStatistics qs in allStatistics)
        //                {
        //                    string lgItem = string.Format("{0}: sql: {1}; total: {2}", qs.Descriptor.Description, qs.SqlTime, qs.ExecutionTime);
        //                    LoggingService.Debug(logger, lgItem);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingService.Error(ex, "Error occurred while logging SmartDataContext statistics.");
        //    }
        //}

        public List<SqlInfo> GetSqlInfo()
        {
            return new List<SqlInfo>(m_InfoQueue.ToArray());
        }

        public void Complete()
        {
            if (Options.TransactionUse != SmartTransactionUse.None)
            {
                if (m_TransactionState == TransactionState.Closed)
                    throw new InvalidOperationException("Invalid use of Complete method.");
                if (m_Transaction != null)
                {

                    if (Transaction.Current != null && Transaction.Current.TransactionInformation.Status == TransactionStatus.Active)
                    {
                        m_Transaction.Complete();
                    }
                }
                m_TransactionState = TransactionState.Closed;
            }
            ClearTransaction();
        }

        /// <summary>
        /// Executes Batch. Stops executing on the first error. Doesn't rollback what was successfully completed.
        /// </summary>
        /// <remarks>Batch consists of individula statements divided by "GO" and newline</remarks>
        /// <param name="script"></param>
        public void ExecuteBatchScript(string script)
        {
            string[] statements = script.Split(new string[] { "GO\r\n", "go\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < statements.Length; i++)
            {
                ExecuteCommand(statements[i]);
            }
        }

        public void ExecuteTransaction(Func<bool> f, string description)
        {
            ExecuteTransactionImplement(f, null, description);
        }

        public void ExecuteTransaction(Func<bool> f, Action cleanup, string description)
        {
            ExecuteTransactionImplement(f, cleanup, description);
        }

        public bool BulkInsert(string tableName, DataTable data)
        {
            //if (Options.TransactionUse == SmartTransactionUse.None)
            //    throw new InvalidOperationException("BulkInsert method is supposed to be invoked under transaction.");

            int attempt = 1;
            while (true)
            {
                try
                {
                    //LoggingService.Debug(Logger, string.Format("BulkInsert attempt #{0}", attempt));
                    ReplayBulkInsert(tableName, data);
                    
                    return true;
                }
                catch (Exception ex)
                {
                    ErrorRecover(ex, "Bulk Insert at " + tableName, ref attempt);
                }
            }
        }

        public void ClearTransaction()
        {
            try
            {
                if (m_Transaction != null)
                {
                    m_Transaction.Dispose();
                    m_Transaction = null;
                }
            }
            catch
            {
            }
        }

        public SmartLock CreateLock(string resource, SmartLockOptions options = null, SqlConnection conn = null)
        {
            if (options == null)
                options = new SmartLockOptions();
            return new SmartLock(resource, options, this, conn);
        }

        public bool LockAndExecute(string resource, SmartLockOptions lockOptions, Action a)
        {
            lockOptions.InitialLock = false;
            string connectionstringOutOfPool = ConnectionString + "Pooling=false;";
            // Increase timeout for the entire query on 3 sec comparing to timeout of sp_getapplock procedure (if positive).
            Options.Timeout = lockOptions.Timeout.Ticks <= 0 ? 0 : (int)Math.Ceiling(lockOptions.Timeout.TotalMinutes * 60.0) + 3;

            bool result = false;
            SqlConnection conn = new SqlConnection(connectionstringOutOfPool);
            var now = DateTimeOffset.Now;
            OpenConnection(conn);
            try
            {
                using (SmartLock lk = CreateLock(resource, lockOptions, conn))
                {
                    result = lk.Lock(true);
                    if (result)
                    {
#if MYDEBUG
                        m_LocksMap[resource] = new SmartLockResourceInfo(resource);
#endif
                        a();
                    }
#if MYDEBUG
                    else
                    {
                        //LoggingService.Debug(Logger, string.Format("Ctx.LockAndExecute. Unable to acquire resource '{0}'", resource));
                        SmartLockResourceInfo ri;
                        if (!m_LocksMap.TryGetValue(resource, out ri))
                            ri = new SmartLockResourceInfo(resource) { LastTimeAcquired = null };
                        ri.UnsuccessfullyAcquiredSince = DateTimeOffset.Now;
                        m_LocksMap[resource] = ri;
                    }
#endif
                }
            }
            finally
            {
                CloseConnection(conn);
                if (result)
                {
#if MYDEBUG
                    m_LocksMap[resource].LastTimeReleased = DateTimeOffset.Now;
#endif                    
                    //LoggingService.Debug(Logger, string.Format("Ctx.LockAndExecute. Released lock: '{0}'", resource));
                }
            }
            return result;
        }

        public bool LockAndExecute(IEnumerable<string> resources, SmartLockOptions lockOptions, Action a, string tag = null)
        {
            string requiredResources = StorageUtils.GenerateSqlInClause(resources).Substring(3);
            //LoggingService.Debug(DbLogger.Locking, string.Format("Ctx.LockAndExecute. requiredResources: {0}; Timeout: {1}; Tag='{2}'",
            //    requiredResources, lockOptions.Timeout, tag));
            lockOptions.InitialLock = false;
            string connectionstringOutOfPool = ConnectionString + "Pooling=false;";
            // Increase timeout for the entire query on 3 sec comparing to timeout of sp_getapplock procedure (if positive).
            Options.Timeout = lockOptions.Timeout.Ticks <= 0 ? 0 : (int)Math.Ceiling(lockOptions.Timeout.TotalMinutes * 60.0) + 3;

            List<Task> tasks = new List<Task>();
            ConcurrentDictionary<string, ResourceInfo> resInfoMap = new ConcurrentDictionary<string, ResourceInfo>();
            foreach (var res in resources)
            {
                resInfoMap.TryAdd(res, new ResourceInfo { Resource = res });
                Task t = Task.Factory.StartNew((state) =>
                {
                    var resAndTag = (Tuple<string, string>)state;
                    var resInfo = resInfoMap[resAndTag.Item1];
                    //LoggingService.Debug(DbLogger.Locking, string.Format("Ctx.LockAndExecute[]. Acquiring resource '{0}'; tag: '{1}'", resInfo.Resource, resAndTag.Item2));
                    resInfo.Connection = new SqlConnection(connectionstringOutOfPool);
                    var now = DateTimeOffset.Now;
                    //LoggingService.Debug(DbLogger.Locking, "Ctx.LockAndExecute[]. Opening connection");
                    resInfo.Connection.Open();
                    //LoggingService.Debug(DbLogger.Locking, string.Format("Ctx.LockAndExecute[]. Time for opening connection={0}", DateTimeOffset.Now - now));
                    using (SmartLock lk = CreateLock(res, lockOptions, resInfo.Connection))
                    {
                        lk.Lock(true);
                        if (lk.Status >= 0)
                        {
                            //LoggingService.Debug(DbLogger.Locking, string.Format("Ctx.LockAndExecute[]. Opened resource '{0}'", resInfo.Resource));
#if MYDEBUG                            
                            m_LocksMap[res] = new SmartLockResourceInfo(res);
#endif
                        }
                        else
                        {
                            //LoggingService.Debug(DbLogger.Locking, string.Format("Ctx.LockAndExecute[]. Unable to acquire resource '{0}'", resInfo.Resource));
                        }
                        resInfo.LockStatus = lk.Status;
                    }
                }, new Tuple<string, string>(res, tag));
                tasks.Add(t);
            }

            bool result = false;
            try
            {
                Task.WaitAll(tasks.ToArray());
                // Execute action if all resources have been acquired
                var sb = new StringBuilder();
                foreach (string res in resources)
                {
                    if (sb.Length > 0)
                        sb.Append("; ");
                    sb.Append(res);
                }
               // LoggingService.Debug(DbLogger.Locking, string.Format("Acquired resources '{0}'", sb.ToString()));
                result = (resInfoMap.Values.All(p => p.LockStatus >= 0));
                if (result)
                    a();
            }
            finally
            {
                // Cleanup
                foreach (var resInfo in resInfoMap.Values)
                {
                    CloseConnection(resInfo.Connection);
                    if (resInfo.LockStatus >= 0)
                    {
#if MYDEBUG
                        m_LocksMap[resInfo.Resource].LastTimeReleased = DateTimeOffset.Now;
#endif
                        //LoggingService.Debug(DbLogger.Locking, string.Format("Ctx.LockAndExecute[]. Released lock: '{0}'", resInfo.Resource));
                    }
                }
                //LoggingService.Debug(DbLogger.Locking, "All locks have been released.");
            }
            return result;
        }

#if MYDEBUG
        public static KeyValuePair<string, SmartLockResourceInfo>[] GetLocksInfo()
        {
            return m_LocksMap.ToArray();
        }
#endif

        public string GetDatabaseSettingsValue(string key)
        {
            string sql = "SELECT [Value] FROM [dbo].[DatabaseSettings] WHERE [Key] = @key";
            return ExecuteQuery<string>(sql, new SqlParameter("@key", key)).SingleOrDefault();
        }

        #endregion Public Methods

        #region IDisposable Members

        public void Dispose()
        {
#if SQL_STAT
            if (Options.LogStatisticsThresholdMs > 0)
                LogDebugStatistics();
#endif
            ClearTransaction();

            if (m_ExclusiveLockHolder == this)
            {
                m_ExclusiveLockHolder = null;
                //StackTrace st = new StackTrace();
                //LoggingService.Debug(Logger, string.Format("Released xLock. {0}", st.ToString()));
                //LogStackTrace("Released xLock.");
            }

            if (m_OuterContext == this)
                m_OuterContext = null;

            if (m_Context != null)
            {
                m_Context.Dispose();
                m_Context = null;
            }

            // Debug print implicitly closed locks.
            //foreach (var lk in ActiveLocks)
            //{
            //    LoggingService.Debug(Logger, string.Format("Implicitly released lock: {0}", lk.Resource));
            //}
        }

        #endregion IDisposable Members

        #region Private Methods

        private int ExecuteCommandImplement(string command, params object[] parameters)
        {
            AddInfo(SqlStmtType.Command, command, parameters);
            Context.CommandTimeout = Options.Timeout;
            using (new StatisticsHelper2(this))
            {
                return ReplayCommandLinq(command, parameters);
            }
        }

        private IEnumerable<TResult> ExecuteQueryImplement<TResult>(string query, params object[] parameters)
        {
            AddInfo(SqlStmtType.Query, query, parameters);
            Context.CommandTimeout = Options.Timeout;
            using (new StatisticsHelper2(this))
            {
                return ReplayQueryLinq<TResult>(query, parameters);
            }
        }

        private IEnumerable<TResult> ExecuteQueryImplement2<TResult>(QueryDescriptor descriptor, params object[] parameters)
        {
            //TODO: use QueryAdoNetImplement to return enumerator.
            AddInfo(SqlStmtType.Query, descriptor.Query, parameters);
            using (new StatisticsHelper2(this))
            {
                //return ReplayQueryAdo<TResult>(descriptor.Query, parameters);
                return ReplayQueryAdo<TResult>(descriptor, parameters);
            }
        }

        private int ExecuteCommandImplement2(string query, params object[] parameters)
        {
            AddInfo(SqlStmtType.Command, query, parameters);
            using (new StatisticsHelper2(this))
            {
                return ReplayCommandAdo(query, parameters);
            }
        }

        private DataTable ExecuteQueryImplement(string query, params SqlParameter[] parameters)
        {
#if SQL_LOG
            LogDebugDynamicStatement("AUTO", SqlStmtType.QueryReader, query, parameters);
#endif
            AddInfo(SqlStmtType.QueryReader, query, parameters);
            Context.CommandTimeout = Options.Timeout;
            using (new StatisticsHelper2(this))
            {
                return ExecuteReader(query, CommandType.Text, parameters);
            }
        }

        private DataTable ExecuteQueryImplementEx(string query, Dictionary<string, Type> replaceTypes, params SqlParameter[] parameters)
        {

            AddInfo(SqlStmtType.QueryReader, query, parameters);
            Context.CommandTimeout = Options.Timeout;
            using (new StatisticsHelper2(this))
            {
                return this.ReplayReaderLinqEx(query, CommandType.Text, replaceTypes, parameters);
            }
        }
        private DataTable ExecuteProcImplement(string proc, params SqlParameter[] parameters)
        {
#if SQL_LOG
            LogDebugDynamicStatement("AUTO", SqlStmtType.ProcReader, "EXEC " + proc, parameters);
#endif
            AddInfo(SqlStmtType.ProcReader, proc, parameters);
            Context.CommandTimeout = Options.Timeout;
            using (new StatisticsHelper2(this))
            {
                return ExecuteReader(proc, CommandType.StoredProcedure, parameters);
            }
        }

        private int ExecuteInsertCommandImplement(string table, string fields, params object[] values)
        {
            int identity;
            return ExecuteInsertCommandImplement(table, fields, false, out identity, values);
        }

        private int ExecuteInsertCommandImplement(string table, string fields, out int identity, params object[] values)
        {
            return ExecuteInsertCommandImplement(table, fields, true, out identity, values);
        }

        private int ExecuteInsertCommandImplement(string table, string fields, bool returnIdentity, out int identity, params object[] values)
        {
            StringBuilder sbFields = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            identity = 0;

            string[] fieldArray = StorageUtils.TrimAndRemoveEmptyEntries(fields.Split(','));

            if (fieldArray.Length != values.Length)
                throw new Exception("Insert failed: the number of fields does not match the number of values");

            SqlParameter[] parameters = new SqlParameter[fieldArray.Length];
            // Predefine parameters
            for (int i = 0; i < parameters.Length; i++)
            {
                parameters[i] = new SqlParameter("@p" + i.ToString(), DBNull.Value);
            }

            for (int i = 0; i < fieldArray.Length; i++)
            {
                if (i > 0)
                {
                    sbFields.Append(", ");
                    sbValues.Append(", ");
                }
                sbFields.AppendFormat("{0}", fieldArray[i]);
                sbValues.AppendFormat("@p{0}", i);
                if (!IsNull(values[i]))
                    parameters[i].Value = values[i];
            }


            string sql = string.Format("INSERT INTO {0} ({1}) VALUES({2})",
                table, sbFields.ToString(), sbValues.ToString());
            if (returnIdentity)
            {
                sql = string.Format(@"
                    {0}
                    SELECT @@ROWCOUNT AS [Affected], SCOPE_IDENTITY() AS [Identity]
                    ", sql);
            }
            if (Options.ExplicitIdentityOn)
            {
                sql = string.Format(@"                    
                    SET IDENTITY_INSERT {0} ON
                    {1}
                    ", table, sql);
            }

#if SQL_LOG
            LogDebugDynamicStatement("AUTO", SqlStmtType.Insert, sql, parameters);
#endif
            Context.CommandTimeout = Options.Timeout;
            using (new StatisticsHelper2(this))
            {
                if (returnIdentity)
                {
                    AffectedIdentity ai = ExecuteQueryImplement2<AffectedIdentity>(new QueryDescriptor(sql), parameters).FirstOrDefault();
                    identity = (int)ai.Identity;
                    return ai.Affected;
                }
                else
                    return ExecuteCommandImplement2(sql, parameters);
            }
        }

        private int ExecuteUpdateCommandImplement(string table, string fields, int whereFieldsNo, params object[] values)
        {
            StringBuilder sbSetClause = new StringBuilder();
            StringBuilder sbWhereClause = new StringBuilder();

            string[] fieldArray = StorageUtils.TrimAndRemoveEmptyEntries(fields.Split(','));

            if (fieldArray.Length != values.Length)
                throw new Exception("Update failed: the number of fields does not match the number of values");

            SqlParameter[] parameters = new SqlParameter[fieldArray.Length];
            // Predefine parameters
            for (int i = 0; i < parameters.Length; i++)
            {
                parameters[i] = new SqlParameter("@p" + i.ToString(), DBNull.Value);
            }

            for (int i = 0; i < fieldArray.Length; i++)
            {
                if (i < whereFieldsNo)
                {
                    if (sbWhereClause.Length > 0)
                        sbWhereClause.AppendFormat(" AND ");
                    if (values[i] != null)
                    {
                        sbWhereClause.AppendFormat("{0} = @p{1}", fieldArray[i], i);
                        parameters[i].Value = values[i];
                    }
                    else
                    {
                        sbWhereClause.AppendFormat("{0} IS NULL", fieldArray[i]);
                    }
                }
                else
                {
                    if (sbSetClause.Length > 0)
                        sbSetClause.AppendFormat(", ");
                    sbSetClause.AppendFormat("{0} = @p{1}", fieldArray[i], i);
                    if (!IsNull(values[i]))
                        parameters[i].Value = values[i];
                }
            }

            if (sbWhereClause.Length > 0)
                sbWhereClause.Insert(0, "WHERE ");
            string sql = string.Format("UPDATE {0} SET {1} {2}",
                table, sbSetClause.ToString(), sbWhereClause.ToString());
            using (new StatisticsHelper2(this))
            {
                return ExecuteCommandImplement2(sql, parameters);
            }
        }

        private IEnumerable<TResult> ExecuteProcImplement<TResult>(string proc, string procParameters, params object[] values)
        {
            StringBuilder sbParameters = new StringBuilder();
            List<object> parameters = new List<object>();
            string[] prmArray = StorageUtils.TrimAndRemoveEmptyEntries(procParameters.Split(','));
            for (int i = 0; i < prmArray.Length; i++)
            {
                if (values[i] != null)
                {
                    if (sbParameters.Length > 0)
                        sbParameters.AppendFormat(", ");
                    if (!prmArray[i].StartsWith("@"))
                        prmArray[i] = "@" + prmArray[i];
                    sbParameters.AppendFormat("{0} = {{{1}}}", prmArray[i], parameters.Count);
                    parameters.Add(values[i]);
                }
            }
            string sql = string.Format("EXEC {0} {1}",
                proc, sbParameters.ToString());
            object[] prms = parameters.ToArray();
#if SQL_LOG
            LogDebugDynamicStatement("AUTO", SqlStmtType.Procedure, sql, prms);
#endif
            Context.CommandTimeout = Options.Timeout;
            AddInfo(SqlStmtType.Procedure, sql, parameters.ToArray());
            using (new StatisticsHelper2(this))
            {
                return ReplayQueryLinq<TResult>(sql, prms);
            }
        }

#if SQL_LOG
        private void LogDebugDynamicStatement(string logger, SqlStmtType stmtType, string sql, object[] parameters)
        {
            if (Options.LogSql)
                DataManager.LogDebugDynamicStatement(logger, stmtType, sql, parameters);
        }
#endif

        private DataTable ExecuteReader(string query, CommandType commandType, params SqlParameter[] parameters)
        {
            if (StatisticsHelper != null)
            {
                StatisticsHelper.QueryStatistics.Descriptor.Description += "(+ reader)";
            }
            return ReplayReaderLinq(query, commandType, parameters);
        }

       
        private bool IsNull(object obj)
        {
            bool result = (obj == null);
            if (!result && obj is Guid)
                result = ((Guid)obj == Guid.Empty);
            return result;
        }

        private void AddInfo(SqlStmtType stmtType, string statement, object[] parameters)
        {
            m_InfoQueue.Enqueue(new SqlInfo(stmtType, statement, parameters));
            if (m_InfoQueue.Count > MAX_INFO_QUEUE_COUNT)
                m_InfoQueue.Dequeue();
        }

        private string ConvertToSqlParameters(string sql, object[] values, out SqlParameter[] sqlParameters)
        {
            string result = sql;
            sqlParameters = new SqlParameter[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                string oldParam = "{" + i.ToString() + "}";
                string newParam = "@p" + i.ToString();
                result = result.Replace(oldParam, newParam);
                sqlParameters[i] = new SqlParameter(newParam, values[i]);
            }
            return result;
        }

        void PrepareTransaction()
        {
            // Isolation Level used is defined in the following order:
            // 1st is used IsolationLevel property of the most outer SmartDataContext in the current thread;
            // if not set then is used IsolationLevel specified in DatabaseSettings of this SmartDataContext;
            // otherwise, the value of DefaultIsolationLevel constant is used.

            if (Options.TransactionUse != SmartTransactionUse.None)
            {
                // Treat exclusive lock as the shared one.
                //if (Options.TransactionUse == SmartTransactionUse.Exclusive)
                //    Options.TransactionUse = SmartTransactionUse.Shared;

                string operation = string.Format("Open {0}", Options.TransactionUse == SmartTransactionUse.Exclusive ? "xTx" : "sTx");
               
                    m_Transaction = new TransactionScope(
                            TransactionScopeOption.Required,
                            //new TransactionOptions { IsolationLevel = m_OuterContext.Options.IsolationLevel, Timeout = TimeSpan.FromSeconds(Options.Timeout) });
                            new TransactionOptions { IsolationLevel = IsolationLevel, Timeout = TimeSpan.FromSeconds(Options.Timeout) });
                m_TransactionState = TransactionState.Opened;

                //ExecuteCommand("SET XACT_ABORT ON");
                if (Options.TransactionUse == SmartTransactionUse.Exclusive && UsingXLock)
                {
                    ExecuteCommand("EXEC [dbo].[LockModule] 'PrepareTransaction'");
                    if (m_ExclusiveLockHolder == null)
                        m_ExclusiveLockHolder = this;
                    //StackTrace st = new StackTrace();
                    //LoggingService.Debug(Logger, string.Format("Obtained xLock. {0}", st.ToString()));
                    LogStackTrace("Obtained xLock.");
                }

            }
        }

        void LogStackTrace(string header)
        {
            return;
            // StackTrace st = new StackTrace();
            // determine # of frames we need to skip
            //int skipCount = 0;
            //while (true)
            //{
            //    string sf = st.GetFrame(skipCount).ToString();
            //    if (sf.Contains("at AsOneTech.Storage.Data.SmartDataContext") || sf.Contains("at AsOneTech.Storage.Data.DataManager"))
            //        skipCount++;
            //    else
            //        break;
            //}
            ////st = new StackTrace(skipCount);
            ////LoggingService.Debug(Logger, string.Format("{0}.\r\n{1}", header, st.ToString()));
            //StringBuilder sb = new StringBuilder(header);
            //sb.AppendLine();
            //for (int i = skipCount; i < st.FrameCount; i++)
            //    sb.AppendLine(st.GetFrame(i).ToString());
            //LoggingService.Debug(Logger, string.Format("{0}\r\n{1}", header, sb.ToString()));
            // string fullStack = st.ToString();
            //string[] lines = fullStack.Split("\r\n", StringSplitOptions.RemoveEmptyEntries);
            //fo
        }

        void ExecuteTransactionImplement(Func<bool> f, Action cleanup, string description)
        {
            //LoggingService.Debug(Logger, string.Format("ExecuteTransactionImplement. Description={0}; ConnectionString={1}", description, ConnectionString));
            int attempt = 1;
            bool repeat = !m_IsAmbientTx;
            do
            {
                try
                {
                    if (f())
                    {
                        Complete();
                    }
                    if (attempt > 1)
                        LoggingService.Debug(DbLogger.DbLayer, string.Format("ExecuteTransaction '{0}' has completed successfully on attempt #{1}", description, attempt));
                    repeat = false;
                }
                catch (RecoverableDatabaseException ex)
                {
                    //LoggingService.Debug(Logger, string.Format("ExecuteTransaction Error. '{0}'; attempt #{1}; Error: {2}", description, attempt, ex.Message));
                    bool rethrow = true;
                    if (!m_IsAmbientTx)
                    {
                        bool isFinal = (attempt >= Options.ReplayCount);
                        StringBuilder sb = new StringBuilder();
                        if (!isFinal)
                        {
                            sb.AppendFormat("Transaction '{0}' will be automatically retried. Attempt #{1}. Reason: {2}",
                                description, attempt, (int)ex.Reason);
                            attempt++;
                        }
                        else
                        {
                            sb.AppendFormat("Transaction '{0}' has failed. This was the final attempt to execute the transaction. Attempt #{1}. Reason: {2}.",
                                description, attempt, (int)ex.Reason);
                        }
                       // LoggingService.Debug(DbLogger.DbLayer, sb.ToString());

                        ClearTransaction();

                        if (!isFinal)
                        {
                            Thread.Sleep(PauseBeforeNewAttemptInExecTxMs);
                            // Cleanup after the prev. attempt
                            if (cleanup != null)
                                cleanup();

                            PrepareTransaction();
                            rethrow = false;
                        }
                    }
                    if (rethrow)
                    {
                        throw;
                    }
                }
            } while (repeat);
        }

        void RegisterSqlError(string query, Exception exception, params object[] parameters)
        {
            return;
            // This works if the table DatabaseSettings contains the record with key "RegisterSqlErrors" with value "1".
            // This allows you to turn on/off the feature on the fly w/o code changing.
            //bool isAllowed = true;
            // 0 (or no rtecord) - no register
            // -1 - register all
            // > 0 - only specific objects (not yet specified)

            //int mask = Convert.ToInt32(GetDatabaseSettingsValue("RegisterSqlErrors"));
            ////if (isAllowed)
            //if (mask < 0)
            //{
            //    bool priorErrorRecovery = Options.NoErrorRecovery;
            //    Options.NoErrorRecovery = true;
            //    // Build parameters string
            //    var sb = new StringBuilder();
            //    sb.AppendFormat("There are {0} parameters in this statement.", parameters.Length);
            //    sb.AppendLine();

            //    for (int i = 0; i < parameters.Length; i++)
            //    {
            //        //if (i > 0)
            //        //    sb.AppendLine();
            //        sb.AppendFormat("Parameter #{0}; type: {1}", i, parameters[i].GetType());
            //        SqlParameter sqlpar = parameters[i] as SqlParameter;
            //        if (sqlpar != null)
            //            sb.AppendFormat("\tName: '{0}'; type: '{1}' Value: '{2}'", sqlpar.ParameterName, sqlpar.SqlDbType, sqlpar.Value);
            //        else
            //            sb.AppendFormat("\tValue: '{0}'", parameters[i]);
            //    }

            //    string parametersStr = sb.ToString();
            //    if (parametersStr.Length > 4000)
            //        parametersStr = parametersStr.Substring(0, 4000);
            //    //if (query.Length >= 4000 || sb.Length >= 4000 || exception.ToString().Length >= 4000)
            //    //    LoggingService.Debug(Logger, string.Format("RegisterSqlError. Length. sqlstmt: {0}; parameters: {1}; exception: {2}",
            //    //        query.Length, sb.Length, exception.ToString().Length));

            //    ExecuteInsertCommandImplement(
            //        "[dbo].[SqlErrorLog]",
            //        "[Timestamp], [Sqlstmt], [Parameters], [Exception]",
            //         DateTimeOffset.Now, query, parametersStr, exception.ToString());

            //    Options.NoErrorRecovery = priorErrorRecovery;
            //}
        }

        public static void LogSqlWithParameters(string sql, IEnumerable<SqlParameter> parameters, DatabaseSettings settings, string header = null)
        {
            if (string.IsNullOrWhiteSpace(header))
                header = "SQL:";
            //LoggingService.Debug(DbLogger.DbLayer, string.Format("Database: '{0}'; {1} {2}", settings.GetSourceAndDatabase(), header, sql));
            foreach (SqlParameter p in parameters)
            {
                string value = p.Value.ToString();
                if (p.SqlDbType == SqlDbType.DateTimeOffset)
                    value = StorageUtils.ToSqlFormat((DateTimeOffset)p.Value);
               // LoggingService.Debug(DbLogger.DbLayer, string.Format("Parameter \"{0}\" value: \"{1}\"", p.ParameterName, value));
            }
        }

        #endregion Private Methods

        #region Replay Methods

        int ReplayCommandLinq(string command, params object[] parameters)
        {
            int attempt = 0;
            while (true)
            {
                try
                {
                    int affected = ReplayCommandLinqOne(command, parameters);
                    if (attempt > 1)
                        LoggingService.Debug(DbLogger.DbLayer, string.Format("Successfully completed the command on attempt #{0}. Command: {1}",
                            attempt, command));
                    return affected;
                }
                catch (Exception ex)
                {
                    ErrorRecover(ex, command, ref attempt, parameters);
                }
            }
        }

        int ReplayCommandLinqOne(string command, params object[] parameters)
        {
            return Context.ExecuteCommand(command, parameters);
        }

        IEnumerable<TResult> ReplayQueryLinq<TResult>(string query, params object[] parameters)
        {
            int attempt = 1;
            while (true)
            {
                try
                {
                    var result = ReplayQueryLinqOne<TResult>(query, parameters);
                    if (attempt > 1)
                        LoggingService.Debug(DbLogger.DbLayer, string.Format("Successfully completed the query on attempt #{0}. Query: {1}",
                            attempt, query));
                    return result;
                }
                catch (Exception ex)
                {
                    ErrorRecover(ex, query, ref attempt, parameters);
                }
            }
        }

        IEnumerable<TResult> ReplayQueryLinqOne<TResult>(string query, params object[] parameters)
        {
            return Context.ExecuteQuery<TResult>(query, parameters);
        }

        DataTable ReplayReaderLinq(string query, CommandType commandType, params SqlParameter[] parameters)
        {
            int attempt = 1;
            while (true)
            {
                try
                {
                    var result = ReplayReaderLinqOne(query, commandType, parameters);
                    if (attempt > 1)
                        LoggingService.Debug(DbLogger.DbLayer, string.Format("Successfully completed the query on attempt #{0}. Query: {1}",
                            attempt, query));
                    return result;
                }
                catch (Exception ex)
                {
                    ErrorRecover(ex, query, ref attempt, parameters);
                }
            }
        }

       
        DataTable ReplayReaderLinqEx(string query, CommandType commandType, Dictionary<string,Type> enumTypes, params SqlParameter[] parameters)
        {
            int attempt = 1;
            while (true)
            {
                try
                {
                    var result = ReplayReaderLinqOneEx(query, commandType, enumTypes,parameters);
                    if (attempt > 1)
                        LoggingService.Debug(DbLogger.DbLayer, string.Format("Successfully completed the query on attempt #{0}. Query: {1}",
                            attempt, query));
                    return result;
                }
                catch (Exception ex)
                {
                    ErrorRecover(ex, query, ref attempt, parameters);
                }
            }
        }
        DataTable ReplayReaderLinqOne(string query, CommandType commandType, params SqlParameter[] parameters)
        {
            
            using (IDbCommand command = Context.Connection.CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = commandType;
                command.CommandTimeout = Options.Timeout;
                foreach (SqlParameter par in parameters)
                    command.Parameters.Add(par);

                try
                {
                    OpenConnection(command.Connection);
                    DataTable dt = new DataTable();
                    using (IDataReader rdr = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        dt.Load(rdr);
                    }
                    return dt;
                }
                finally
                {
                    command.Parameters.Clear();
                }
            }
        }

        private DataTable ReplayReaderLinqOneEx(string query, CommandType commandType, Dictionary<string, Type> enumTypes, params SqlParameter[] parameters)
        {

            using (IDbCommand command = Context.Connection.CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = commandType;
                command.CommandTimeout = Options.Timeout;
                foreach (SqlParameter par in parameters)
                    command.Parameters.Add(par);

                try
                {
                    OpenConnection(command.Connection);
                    DataTable dt = new DataTable();
                    using (SqlDataReader rdr = (SqlDataReader)command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        DataTable dtSchema = rdr.GetSchemaTable();
                        if (dtSchema != null)
                        {
                            foreach (DataRow drow in dtSchema.Rows)
                            {
                                string columnName = System.Convert.ToString(drow["ColumnName"]);
                                Type datatype;
                                if (enumTypes == null || !enumTypes.ContainsKey(columnName))
                                    datatype = (Type)(drow["DataType"]);
                                else
                                    datatype = typeof(string);// enumTypes[columnName];
                                DataColumn column = new DataColumn(columnName, datatype);
                                column.Unique = (bool)drow["IsUnique"];
                                column.AllowDBNull = (bool)drow["AllowDBNull"];
                                column.AutoIncrement = (bool)drow["IsAutoIncrement"];
                                dt.Columns.Add(column);
                            }
                        }
                        while (rdr.Read())
                        {
                            DataRow dataRow = dt.NewRow();
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {

                                var data = rdr[i];
                                Type t = null;
                                if (enumTypes != null)
                                    enumTypes.TryGetValue(dt.Columns[i].ColumnName, out t);
                                if (t != null && t.IsEnum)
                                    data = Enum.GetName(t, data);
                                dataRow[i] = data;
                            }
                            dt.Rows.Add(dataRow);
                        }
                    }
                    return dt;
                }
                finally
                {
                    command.Parameters.Clear();
                }
            }
        }

        int ReplayCommandAdo(string query, params object[] parameters)
        {
            int attempt = 1;
            while (true)
            {
                try
                {
                    //LoggingService.Debug(string.Format("ReplayCommandAdo 1. m_Transaction={0}", m_Transaction == null ? "null" : "not null"));
                    int affected = ReplayCommandAdoOne(query, parameters);
                   
                    return affected;
                }
                catch (Exception ex)
                {
                    //LoggingService.Debug(string.Format("ReplayCommandAdo 2. m_Transaction={0}", m_Transaction == null ? "null" : "not null"));
                    ErrorRecover(ex, query, ref attempt, parameters);
                }
            }
        }

        int ReplayCommandAdoOne(string query, params object[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                int result = 0;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Options.Timeout;
                foreach (SqlParameter par in parameters)
                    cmd.Parameters.Add(par);

                //if (Options.IsVerbose)
                    //LogSqlWithParameters(query, cmd.Parameters.Cast<SqlParameter>(), "ReplayCommandAdoOne");

                try
                {
                    OpenConnection(conn);
                    result = cmd.ExecuteNonQuery();
                    conn.Close();
                    return result;
                }
                finally
                {
                    cmd.Parameters.Clear();
                }
            }
        }

        IEnumerable<TResult> ReplayQueryAdo<TResult>(QueryDescriptor descriptor, params object[] parameters)
        {
            int attempt = 1;
            while (true)
            {
                try
                {
                    var result = ReplayQueryAdoOne<TResult>(descriptor, parameters);
                    if (attempt > 1)
                        LoggingService.Debug(DbLogger.DbLayer, string.Format("Successfully completed the query on attempt #{0}. Query: {1}",
                            attempt, descriptor.Query));
                    return result;
                }
                catch (Exception ex)
                {
                    try
                    {
                        ErrorRecover(ex, descriptor.Query, ref attempt, parameters);
                    }
                    catch (DatabaseQueryCanceled)
                    {
                        return null;
                    }
                }
            }
        }

        IEnumerable<TResult> ReplayQueryAdoOne<TResult>(QueryDescriptor descriptor, params object[] parameters)
        {
            
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                IEnumerable<TResult> result = null;
                OpenConnection(conn, descriptor);

                //Task t = RegisterLongQuery();
                result = ExecuteQueryImplement<TResult>(conn, descriptor.Query, parameters);
               // UnregisterLongQuery();

                //if (Options.IsCancelable)
               //     t.Wait();
                return result;
            }
        }

        IEnumerable<TResult> ExecuteQueryImplement<TResult>(SqlConnection conn, string query, params object[] parameters)
        {
            List<TResult> result = null;
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Options.Timeout;
                foreach (SqlParameter par in parameters)
                    cmd.Parameters.Add(par);

                //if (Options.IsVerbose)
                    //LogSqlWithParameters(query, cmd.Parameters.Cast<SqlParameter>(), "ExecuteQueryImplement");

                try
                {
                    bool isConnectionOpen = conn.State == ConnectionState.Open;
                    if (!isConnectionOpen)
                        OpenConnection(conn);
                    using (SqlDataReader reader = cmd.ExecuteReader(isConnectionOpen ? CommandBehavior.Default : CommandBehavior.CloseConnection))
                    {
                        result = Activator.CreateInstance<List<TResult>>();
                        TypeInfo ti = m_TypesCache.GetTypeInfo(typeof(TResult));
                        while (reader.Read())
                        {
                            TResult resultItem = default(TResult);
                            object[] values = new object[reader.FieldCount];
                            int fieldCount = reader.GetValues(values);


                            if (fieldCount == 1 && ti.IsPrimitive)
                            {
                                // primitive type
                                if (values[0] != null && !(values[0] is System.DBNull))
                                {
                                    resultItem = (TResult)values[0];
                                }
                            }
                            else if (fieldCount == 1 &&
                                (ti.PropertyMapByName.Count == 0 ||
                                    values[0] is DateTimeOffset))
                            {
                                // most probably value type like Guid
                                if (!(values[0] is System.DBNull))
                                {
                                    resultItem = (TResult)values[0];
                                }
                            }
                            else
                            {
                                resultItem = Activator.CreateInstance<TResult>();
                                for (int i = 0; i < fieldCount; i++)
                                {
                                    PropertyInfo pi;
                                    if (values[i] != null && !(values[i] is System.DBNull))
                                    {
                                        string propertyName = reader.GetName(i);
                                        if (ti.PropertyMapByName.TryGetValue(propertyName, out pi) && pi.CanWrite)
                                        {
                                            try
                                            {
                                                pi.SetValue(resultItem, values[i], null);
                                            }
                                            catch (Exception ex)
                                            {
                                                string text = string.Format("Error setting property '{0}', value '{1}' by the reader. ",
                                                    propertyName, values[i].ToString());
                                                throw new Exception(text, ex);
                                            }
                                        }
                                    }
                                }
                            }
                            result.Add(resultItem);
                        }
                        if (!isConnectionOpen)
                            conn.Close();
                    }
                }
                finally
                {
                    cmd.Parameters.Clear();
                }
                return result;
            }
        }

        void ReplayBulkInsert(string tableName, DataTable data)
        {
           
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                //if (Options.IsVerbose)
                    //LoggingService.Debug(Logger, string.Format("ReplayBulkInsert. Database='{0}', Table='{1}'; Rows: {2}",
                    //    Settings.GetSourceAndDatabase(), tableName, data.Rows.Count));

                OpenConnection(conn);
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.UseInternalTransaction, null))
                //using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.UseInternalTransaction | SqlBulkCopyOptions.CheckConstraints | SqlBulkCopyOptions.FireTriggers, null))
                {
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.BulkCopyTimeout = Options.BulCopyTimeout; //in seconds
                    bulkCopy.BatchSize = data.Rows.Count;

                    // Set up the column mappings source and destination.
                    foreach (DataColumn col in data.Columns)
                        bulkCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));

                    bulkCopy.WriteToServer(data);
                }
            }
            //LoggingService.Debug(Logger, string.Format("ReplayBulkInsert Exit: {0}", tableName));
        }

        void ErrorRecover(Exception ex, string query, ref int attempt, params object[] parameters)
        {
            RecoverableDatabaseException recoverEx = null;
            SqlException sqlEx = ex as SqlException;
            bool underTx = Transaction.Current != null;

            if (Options.NoErrorRecovery)
            {
                LoggingService.Debug(DbLogger.DbLayer, string.Format("ErrorRecover. Rethrowing exception due to NoErrorRecovery option: '{0}'", ex.Message));
                throw ex;
            }
            if (sqlEx != null && QueryInfo != null && QueryInfo.Canceling)
            {
                // The query has been canceled, or is being canceling. Just ignore.
                throw new DatabaseQueryCanceled();
            }

            try
            {
                RegisterSqlError(query, ex, parameters);
            }
            catch { }

            if (sqlEx != null && sqlEx.Number == 1205)
            {
                recoverEx = new RecoverableDatabaseException("SQL statement failed due to deadlock.", query, DatabaseErrorReason.Deadlock, ex);
                if (underTx)
                    throw recoverEx;
                Thread.Sleep(Options.PauseAfterDeadlock * attempt);
            }
            else if (sqlEx != null && sqlEx.Message.StartsWith("Timeout expired."))
            {
                recoverEx = new RecoverableDatabaseException("SQL statement failed due to timeout.", query, DatabaseErrorReason.Timeout, ex);
                if (underTx)
                    throw recoverEx;
            }
            else if (sqlEx != null && sqlEx.Number == 3960)
            {
                recoverEx = new RecoverableDatabaseException("SQL statement failed due to update conflict.", query, DatabaseErrorReason.UpdateConflict, ex);
                if (underTx)
                    throw recoverEx;
                Thread.Sleep(Options.PauseAfterDeadlock * attempt);
            }
            else if (sqlEx != null && sqlEx.Message.StartsWith("Connection Timeout Expired."))
            {
                recoverEx = new RecoverableDatabaseException("SQL statement failed due to connection timeout.", query, DatabaseErrorReason.ConnectionTimeout, ex);
                if (underTx)
                    throw recoverEx;
            }
            // Some standard unrecoverable exceptions
            else if (sqlEx != null && sqlEx.Number == 1105)
            {
                throw new SpaceAllocationDatabaseException(ex);
            }

            if (recoverEx != null && attempt < Options.ReplayCount)
            {
                LoggingService.Debug(DbLogger.DbLayer, string.Format("SQL statement will be automatically retried. Attempt #{0}. Reason: {1}",
                    attempt, (int)recoverEx.Reason));
                attempt++;
            }
            else if (recoverEx != null)
            {
                LoggingService.Error(DbLogger.DbLayer, string.Format("Unable to recover from the database error after {0} attempts. Query: {1}", attempt, query));
                throw recoverEx;
            }
            else
            {
                //LoggingService.Debug(Logger, string.Format("ErrorRecover. Rethrowing exception '{0}'; underTx={1}", ex.Message, underTx));
                throw ex;  // new UnrecoverableDatabaseException(query, ex);
            }
        }

        void LogTxStatus(string header)
        {
            LoggingService.Debug(DbLogger.DbLayer, string.Format("{0}. Current transaction status is: {1}",
                header,
                Transaction.Current != null ? Transaction.Current.TransactionInformation.Status.ToString() : "no transaction"));
        }

        void OpenConnection(IDbConnection conn, QueryDescriptor descriptor = null)
        {
            SqlConnection sqlConn = conn as SqlConnection;
            if (sqlConn != null)
            {
                // Open connection if closed
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                    // Make initial settings
                    StringBuilder sb = new StringBuilder();
                    if (Options.TransactionUse != SmartTransactionUse.None)
                        sb.AppendLine("SET XACT_ABORT ON; ");
                    else
                        sb.AppendFormat("SET TRANSACTION ISOLATION LEVEL {0}; SET XACT_ABORT ON; ", IsolationLevel.ToString());

                    if (!Options.IsCancelable)
                    {
                        using (SqlCommand cmd = new SqlCommand(sb.ToString(), sqlConn))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandTimeout = Options.Timeout;
                            //LoggingService.Debug(string.Format("OpenConnection. SQL={0}", cmd.CommandText));
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        sb.Append(@"
                        SELECT session_id AS SessionId, connection_id AS ConnectionId FROM sys.dm_exec_requests WHERE session_id = @@SPID");
                        //LoggingService.Debug(string.Format("OpenConnection. SQL={0}", sb.ToString()));
                        QueryInfo = ExecuteQueryImplement<QueryInfo>(sqlConn, sb.ToString()).Single();
                        QueryInfo.Context = this;
                    }
                }
            }
        }

        void CloseConnection(IDbConnection conn)
        {
            SqlConnection sqlConn = conn as SqlConnection;
            if (sqlConn != null)
                sqlConn.Close();
        }

        Task RegisterLongQuery()
        {
            Task result = null;
            if (Options.IsCancelable)
            {
                //LoggingService.Debug(LOGGER, "ReplayQueryAdoOne. IsLongQuery.");
                // We will start a query in a separate thread that will get the start time of our long query.
                // We will repeat the measurement until:
                //  - get not empty start time;
                //  - or long query ends;
                //  - or timeout expires (LONG_QUERY_MEASUREMENT_TIMEOUT)
                result = Task.Factory.StartNew(() =>
                {
                    DateTimeOffset startTask = DateTimeOffset.Now;
                    DateTimeOffset maxEndTask = startTask + LONG_QUERY_MEASUREMENT_TIMEOUT;
                    // Intial sleep interval = 1ms; it will be doubled every time.
                    TimeSpan sleepTime = TimeSpan.Zero;
                    using (SqlConnection conn2 = new SqlConnection(ConnectionString))
                    {
                        conn2.Open();   // to avoid calling OpenConnection() method.
                        string sqlInfo = @"SELECT start_time FROM sys.dm_exec_requests WHERE session_id = @sessionId";
                        int attempts = 0;
                        while (true)
                        {
                            DateTime startTime = ExecuteQueryImplement<DateTime>(conn2, sqlInfo,
                                new SqlParameter("@sessionId", QueryInfo.SessionId)).SingleOrDefault();
                            attempts++;
                            lock (QueryInfo)
                            {
                                if (QueryInfo.StartTime > DateTime.MinValue || startTime > DateTime.MinValue)
                                {
                                    if (QueryInfo.StartTime == DateTime.MinValue)
                                        QueryInfo.StartTime = startTime;
                                    break;
                                }
                                //if (attempts > MAX_ATTEMPTS_TO_MEASURE_REQUEST)
                                if (DateTimeOffset.Now >= maxEndTask)
                                {
                                    // Expired.
                                    break;
                                }
                            }
                            sleepTime = sleepTime == TimeSpan.Zero ? TimeSpan.FromMilliseconds(1) : sleepTime + sleepTime;
                            if (DateTimeOffset.Now + sleepTime > maxEndTask)
                                sleepTime = maxEndTask - DateTimeOffset.Now;
                            Thread.Sleep(sleepTime);
                        }
                        //LoggingService.Debug(Logger, string.Format("ReplayQueryAdoOne. attempts={0}", attempts));
                        //LoggingService.Debug(Logger, string.Format("ReplayQueryAdoOne. SessionId={0}; ConnectionId={1}; StartTime={2}; ",
                        //    //descriptor.Description,
                        //    QueryInfo.SessionId, QueryInfo.ConnectionId, StorageUtils.ToSqlFormat(QueryInfo.StartTime)));
                    }
                });

                // Register Query
                //CancellationContext.AddCancelableItem(QueryInfo);
            }
            return result;
        }

        void UnregisterLongQuery()
        {
            if (Options.IsCancelable)
            {
                lock (QueryInfo)
                {
                    QueryInfo.StartTime = DateTime.Now;
                   // CancellationContext.RemoveCancelableItem(QueryInfo);
                }
            }
        }

        #endregion Replay Methods

        #region Helper Types

        class AffectedIdentity
        {
            public int Affected { get; set; }
            public decimal Identity { get; set; }
        }

        enum TransactionState
        {
            NoTransaction,
            Opened,
            Closed,
        }

        class ResourceInfo
        {
            public string Resource { get; set; }
            public SqlConnection Connection { get; set; }
            //public bool UnableToLock { get; set; }
            public SmartLockStatus LockStatus { get; set; }
            public ResourceInfo()
            {
                LockStatus = SmartLockStatus.Undefined;
            }
        }

        #endregion Helper Types
    }

    public class SmartDataContextOptions
    {
        const int DefaultTimeout = 600; // 10 min
        const int DefaultBulkCopyTimeout = 120; // 2 min
        const int DefaultReplayCount = 10;
        const int DefaultPauseAfterDeadlock = 1000;
        public const System.Transactions.IsolationLevel DefaultIsolationLevel = System.Transactions.IsolationLevel.Snapshot;
#if SQL_LOG
        public bool LogSql { get; set; }
#endif
#if SQL_STAT
        public int LogStatisticsThresholdMs { get; set; }
#endif
        public int Timeout { get; set; }    // in seconds
        //public int ConnectionTimeout { get; } // in seconds
        public bool ExplicitIdentityOn { get; set; }
        public SmartTransactionUse TransactionUse { get; set; }
        public int BulCopyTimeout { get; set; }
        public int ReplayCount { get; set; }
        //public bool SessionIdRequested { get; set; }
        public int PauseAfterDeadlock { get; set; } // in ms
        public System.Transactions.IsolationLevel? IsolationLevel { get; set; }
        public bool IsCancelable { get; set; }
        public bool NoErrorRecovery { get; set; }
        //public bool IsVerbose { get; set; }

        public SmartDataContextOptions()
        {
            Timeout = DefaultTimeout; // 10 min.
            //IsolationLevel = DefaultIsolationLevel;
            BulCopyTimeout = DefaultBulkCopyTimeout;
            ReplayCount = DefaultReplayCount;
            TransactionUse = SmartTransactionUse.None;
            PauseAfterDeadlock = DefaultPauseAfterDeadlock;
            //SessionIdRequested = true;
#if SQL_LOG
            LogSql = ConfigurationService.GetValue<bool>(DataManager.IsLogSqlEnabled, false);
#endif
#if SQL_STAT
            LogStatisticsThresholdMs = ConfigurationService.GetValue<int>("Debug.LogSqlStatisticsThresholdMs", 0);
#endif
        }

        public SmartDataContextOptions GetClone()
        {
            return (SmartDataContextOptions)MemberwiseClone();
        }
    }

    public class QueryInfo //: ICancelable
    {
        // Since we have ConnectionId, we don't need database_id and user_id because connection is taken from the pool,
        // and different database or user will force taking different connection. For the same reason we don't need
        // process_id because another process will have its own pool, and thus, different connection_id.
        public short SessionId { get; set; }
        public Guid ConnectionId { get; set; }
        public int RequestId { get; set; }
        public DateTime StartTime { get; set; }
        public SmartDataContext Context { get; set; }
        public bool Canceling { get; private set; }

        #region Members of ICancelable interface

        public string Id
        {
            get { return ConnectionId.ToString(); }
        }

        //public void Cancel()
        //{
        //    // If we couldn't read StartTime, we can't kill the query
        //    if (StartTime > DateTime.MinValue)
        //    {
        //        // Read session and compare
        //        string sql = @"SELECT session_id AS SessionId, start_time AS StartTime FROM sys.dm_exec_requests WHERE connection_id = @connectionId";
        //        var adminDataManager = new DataManager(Context.Settings.CloneAsAdmin());
        //        QueryInfo request = adminDataManager.ExecuteQuery<QueryInfo>(sql, new SqlParameter("@connectionId", ConnectionId)).SingleOrDefault();
        //        // If request not found or not compared, then it has already terminated.
        //        if (request != null && request.SessionId == SessionId && request.StartTime == StartTime)
        //        {
        //            // Kill it; execute by admin credentials
        //            string killSql = string.Format("KILL {0}", SessionId);
        //            Canceling = true;
        //            adminDataManager.ExecuteCommand(killSql);
        //            LoggingService.Info(DbLogger.DbLayer, string.Format("SQL query/command canceled with SPID={0}", SessionId));
        //        }
        //    }
        //}

        #endregion Members of ICancelable interface
    }

    public enum SmartTransactionUse
    {
        None,
        Exclusive,
        Shared
    }

    public enum SmartLockMode
    {
        Shared,
        Exclusive,
        Update,
        IntentShared,
        IntentExclusive
    }

    public enum SmartLockStatus
    {
        Ok = 0,
        OkAfterWait = 1,
        Timeout = -1,
        Canceled = -2,
        Deadlock = -3,
        Error = -999,
        Undefined = -777
    }

    public class SmartLock : IDisposable
    {
        const string LOGGER = "SmartLock";
        public string Resource { get; private set; }
        private SmartLockOptions m_Options = null;
        public SmartLockOptions Options
        {
            get { return new SmartLockOptions(m_Options); }
        }
        public SmartLockStatus Status { get; private set; }
        public SmartDataContext Ctx { get; private set; }
        public SqlConnection Connection { get; private set; }

        public SmartLock(string resource, SmartLockOptions options, SmartDataContext ctx, SqlConnection conn)
        {
            if (ctx == null)
                throw new ArgumentNullException();

            Ctx = ctx;
            Connection = conn;
            //LoggingService.Debug(string.Format("SmartLock. InitialLock={0}", options.InitialLock));
            Init(resource, options);
        }

        public bool Lock(bool sessionWide)
        {
            string sql = @"
                DECLARE @lockResult int
                EXEC @lockResult = sp_getapplock @Resource = @resource,  @LockMode = @mode, @LockOwner = @owner, @LockTimeout = @timeout
                SELECT @lockResult";

            string strMode = (m_Options.Mode == SmartLockMode.Shared)
                ? "Shared"
                : (m_Options.Mode == SmartLockMode.Exclusive)
                    ? "Exclusive"
                    : (m_Options.Mode == SmartLockMode.Update)
                        ? "Update"
                        : (m_Options.Mode == SmartLockMode.IntentShared)
                        ? "IntentShared" : "IntentExclusive";

            int timeout = m_Options.Timeout.Ticks < 0 ? -1 : (int)m_Options.Timeout.TotalMilliseconds;

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@resource", Resource),
                new SqlParameter("@mode", strMode),
                new SqlParameter("@owner", sessionWide ? "Session" : "Transaction"),
                new SqlParameter("@timeout", timeout)
            };

            //LoggingService.Debug(string.Format("SmartLock.Lock. Resource='{0}'; sessionWide={1}", Resource, sessionWide));
            if (Connection == null)
                Status = Ctx.ExecuteQuery<SmartLockStatus>(sql, parameters).Single();
            else
                Status = Ctx.ExecuteQuery<SmartLockStatus>(Connection, sql, parameters).Single();
            //if (Status >= 0)
            //    LoggingService.Debug(LOGGER, string.Format("SmartLock.Lock. Resource='{0}'; sessionWide={1}; Status={2}; ThreadId={3}", Resource, sessionWide, Status, Thread.CurrentThread.ManagedThreadId));
            //else
            //{
            //    int heldBy = -1;
            //    //lock (Sync)
            //    AllLocks.TryGetValue(Resource, out heldBy);
            //    LoggingService.Debug(LOGGER, string.Format("SmartLock.Lock. Resource='{0}'; sessionWide={1}; Status={2}; Held by {3}", Resource, sessionWide, Status, heldBy));
            //}

            //debug
            //if (Status >= 0)
            //{
            //    //lock (Sync)
            //    AllLocks[Resource] = Thread.CurrentThread.ManagedThreadId;
            //}
            //enddebug
            return (Status >= 0);
        }

        public void Unlock()
        {
            if (Status == SmartLockStatus.Ok || Status == SmartLockStatus.OkAfterWait)
            {
                bool session = false;
                string sql = @"
                DECLARE @lockResult int
                EXEC @lockResult = sp_releaseapplock @Resource = @resource,  @LockOwner = @owner
                SELECT @lockResult";
                Status = Ctx.ExecuteQuery<SmartLockStatus>(new QueryDescriptor(sql),
                    new SqlParameter("@resource", Resource),
                    new SqlParameter("@owner", session ? "Session" : "Transaction"))
                    .Single();
                //Ctx.ActiveLocks.Remove(this);
                //debug
                //int oldValue;
                //AllLocks.TryRemove(Resource, out oldValue);
                //enddebug
                LoggingService.Debug(DbLogger.Locking, string.Format("Explicitly released lock: {0}", Resource));
            }
        }

        public void Dispose()
        {
            //LoggingService.Debug("SmartLock.Dispose.");
            //Unlock();
        }

        void Init(string resource, SmartLockOptions options)
        {
            Resource = resource;
            m_Options = new SmartLockOptions(options);
            Status = SmartLockStatus.Undefined;
            if (m_Options.InitialLock)
                Lock(false);
        }
    }

    public class SmartLockOptions
    {
        public SmartLockMode Mode { get; set; }
        public TimeSpan Timeout { get; set; }
        public bool InitialLock { get; set; }

        public SmartLockOptions()
        {
            Mode = SmartLockMode.Shared;
            Timeout = TimeSpan.Zero;
            InitialLock = true;
            //InitialLock = false;
        }
        public SmartLockOptions(SmartLockOptions other)
        {
            Mode = other.Mode;
            Timeout = other.Timeout;
            InitialLock = other.InitialLock;
        }
    }

#if MYDEBUG  
    public class SmartLockResourceInfo
    {
        public string ResourceName { get; set; }
        public DateTimeOffset? LastTimeAcquired { get; set; }
        public DateTimeOffset? LastTimeReleased { get; set; }
        public DateTimeOffset? UnsuccessfullyAcquiredSince { get; set; }

        public SmartLockResourceInfo(string resource)
        {
            ResourceName = resource;
            LastTimeAcquired = DateTimeOffset.Now;
        }
    }
#endif
}
