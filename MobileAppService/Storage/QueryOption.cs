﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;

namespace Storage.Storage
{
    [Serializable]
    [DataContract]
    public class QueryOption
    {
        // public int? Top { get; set; } // get/set the max number of returned records for each paln definition
        //public int? Skip { get; set; }
        [DataMember]
        public FilterOption Filter { get; set; }
        [DataMember]
        public SelectOption Select { get; set; }
        [DataMember]
        public OrderOption Order { get; set; }

        
        public bool HasOrder
        {
            get { return Order != null && !string.IsNullOrEmpty(Order.Statement); }
        }
       
        public bool HasSelect
        {
            get { return Select != null && !string.IsNullOrEmpty(Select.Statement); }
        }

        public bool HasFilter
        {
            get { return Filter != null && !string.IsNullOrEmpty(Filter.Statement); }
        }
       
        public virtual bool IsEmpty
        {
            get
            {
                return !HasFilter && !HasOrder && !HasSelect;
                // &&  (Top == null || Top == 0);
                // && (Skip == null || Skip == 0);
            }
        }

    }

    [Serializable]
    public class OptionBase
    {
        public string Statement { get; set; }
       
        public bool IsEmpty { get { return string.IsNullOrEmpty(Statement); } }
    }

    [Serializable]
	public class FilterOption : OptionBase
    {
    }

    [Serializable]
	public class SelectOption : OptionBase
	{
		//TODO Tuple don't work in 3.5 
		//public static IEnumerable<Tuple<string, string>> Parse(string statement)
		//{
		//    List<Tuple<string, string>> l = new List<Tuple<string,string>>();
		//    if (!string.IsNullOrEmpty(statement))
		//    {
		//        string[] opts = statement.Split(',');
		//        for (int i = 0; i < opts.Length; i++)
		//        {
		//            var res = new Tuple<string, string>(opts[i].Trim(), null);
		//            l.Add(res);
		//        }
		//    }
		//    return l;
		//}

	}

	[Serializable]
	public class OrderOption : OptionBase
	{
        public int Top { get; set; }
	}

   
}
