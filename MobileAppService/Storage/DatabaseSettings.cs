﻿using System;

namespace Storage.Storage
{

    //[FactoryType]
    [Serializable]
    public class DatabaseSettings
    {
        #region Fields

        const string DEFAULT_DATASOURCE = "localhost";
        const string DEFAULT_DATABASE = "MyDatabase";
        const string DEFAULT_USER_ID = "xmuser";
        const string ADMIN_USER_ID = "xmadmin";
        const string DEFAULT_PASSWORD = "As$$$1TecHnoloOo-gy#cATALYST!Xm";
        const string ADMIN_PASSWORD = "As$$$1TecHnoloOo-gy#cATALYST!Xm-Savigent2012";
        const bool DEFAULT_IS_WINDOWS_AUTHENTICATION = false;
        const int DEFAULT_CONNECTION_TIMEOUT = 60;  // in seconds
        const int MICROSOFT_DEFAULT_CONNECTION_TIMEOUT = 15;  // in seconds

        string m_DataSource;
        string m_Database;
        bool m_IsWindowsAuthentication;
        bool m_WindowsAuthenticationExplicitlySet = false;
        string m_UserId;
        string m_Password;
        bool m_UserOrPasswordExplicitlySet = false;
        string m_Other = "MultipleActiveResultSets=True;";
        string m_ConnectionString = string.Empty;
        int m_ConnectionTimeout = DEFAULT_CONNECTION_TIMEOUT;

        #endregion Fields

        #region Constructors

        public DatabaseSettings()
        {
            DataSource = DEFAULT_DATASOURCE;
            Database = DEFAULT_DATABASE;
            IsWindowsAuthentication = DEFAULT_IS_WINDOWS_AUTHENTICATION;
            UserId = DEFAULT_USER_ID;
            Password = DEFAULT_PASSWORD;
            MakeConnectionString();
        }

        #endregion Constructors

        #region Public Properties

  
        public virtual string DataSource
        {
            get { return m_DataSource; }
            set
            {
                m_DataSource = value;
                MakeConnectionString();
            }
        }

        public virtual string Database
        {
            get { return m_Database; }
            set
            {
                m_Database = value;
                MakeConnectionString();
            }
        }

        public bool IsWindowsAuthentication
        {
            get { return m_IsWindowsAuthentication; }
            set
            {
                m_IsWindowsAuthentication = value;
                MakeConnectionString();
            }
        }

        public string UserId
        {
            get { return m_UserId; }
            set
            {
                m_UserId = value;
                MakeConnectionString();
            }
        }

        public string Password
        {
            get { return m_Password; }
            set
            {
                m_Password = value;
                MakeConnectionString();
            }
        }

        public string Other
        {
            get { return m_Other; }
            set
            {
                m_Other = value;
                MakeConnectionString();
            }
        }

        public string ConnectionString
        {
            get { return m_ConnectionString; }
            set
            {
                m_ConnectionString = value;
                ParseConnectionString();
            }
        }

        public static DatabaseSettings AdminSettings
        {
            get
            {
                DatabaseSettings admin = new DatabaseSettings();
                admin.UserId = ADMIN_USER_ID;
                admin.Password = ADMIN_PASSWORD;
                admin.IsWindowsAuthentication = false;
                admin.MakeConnectionString();
                return admin;
            }
        }

        public System.Transactions.IsolationLevel? IsolationLevel { get; set; }

        public int ConnectionTimeout
        {
            get { return m_ConnectionTimeout; }
            set
            {
                m_ConnectionTimeout = value;
                MakeConnectionString();
            }
        }

        #endregion Public Properties

        #region Public Methods

        //public DatabaseSettings Clone()
        //{
        //    return (DatabaseSettings)this.MemberwiseClone();
        //}

        //public DatabaseSettings CloneAsAdmin()
        //{
        //    var result = (DatabaseSettings)this.MemberwiseClone();
        //    result.IsWindowsAuthentication = false;
        //    result.UserId = ADMIN_USER_ID;
        //    result.Password = ADMIN_PASSWORD;
        //    result.MakeConnectionString();
        //    return result;
        //}

        public string GetSourceAndDatabase()
        {
            return DataSource + "/" + Database;
        }

        #endregion Public Methods

        #region Private Methods

        protected void MakeConnectionString()
        {
            string userIdAndPassword = string.Empty;
            //if (!IsWindowsAuthentication)
            //    userIdAndPassword = string.Format("User Id={0};Password={1};",
            //        m_UserId, m_Password);
            string connectionTimeout = m_ConnectionTimeout == MICROSOFT_DEFAULT_CONNECTION_TIMEOUT
                ? "" : string.Format("Connection Timeout={0};", m_ConnectionTimeout);

            m_ConnectionString = string.Format(
                "Data Source={0};Initial Catalog={1};{2}{3}{4}{5}",
                DataSource,
                Database,
                userIdAndPassword,
                connectionTimeout,
                "Integrated Security = true;",// m_IsWindowsAuthentication ? "Integrated Security=SSPI;" : "",
                m_Other);
        }

        private void ParseConnectionString()
        {
            m_DataSource = "?";
            m_Database = "?";
            //m_IsWindowsAuthentication = true;
            m_UserId = "";
            m_Password = "";
            m_Other = "";

            // Split by ";"
            string[] tokens = m_ConnectionString.Split(new char[] { ';' });
            foreach (string token in tokens)
            {
                ParseToken(token);
            }

            // Windows authentication is set by the following rules:
            // if it's set explicitly then that value is honored;
            // if it's not set but user id or password is set, then false is used;
            // otherwise default value is used.
            if (!m_WindowsAuthenticationExplicitlySet)
            {
                m_IsWindowsAuthentication = m_UserOrPasswordExplicitlySet
                    ? false
                    : DEFAULT_IS_WINDOWS_AUTHENTICATION;
            }
        }

        private void ParseToken(string token)
        {
            string[] pair = token.Split(new char[] { '=' }, 2);
            bool match = false;
            if (pair.Length == 2)
            {
                string key = pair[0].Trim().ToLower();
                string value = pair[1];
                match = true;
                switch (key)
                {
                    case "data source":
                        m_DataSource = value;
                        break;
                    case "initial catalog":
                        m_Database = value;
                        break;
                    case "integrated security":
                        if (value.Equals("sspi", StringComparison.InvariantCultureIgnoreCase))
                            m_IsWindowsAuthentication = true;
                        else
                            m_IsWindowsAuthentication = Convert.ToBoolean(value);
                        m_WindowsAuthenticationExplicitlySet = true;
                        break;
                    case "user id":
                        m_UserId = value;
                        m_UserOrPasswordExplicitlySet = true;
                        break;
                    case "password":
                        m_Password = value;
                        m_UserOrPasswordExplicitlySet = true;
                        break;
                    default:
                        match = false;
                        break;
                }
            }

            if (!match)
            {
                if (!string.IsNullOrEmpty(m_Other))
                    m_Other += ";";
                m_Other += token;
            }
        }

        #endregion Private Methods
    }
}
