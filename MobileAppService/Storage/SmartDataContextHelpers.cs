﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;

namespace Storage.Storage
{
  

    public class QueryStatistics
    {
        public QueryDescriptor Descriptor { get; set; }
        public TimeSpan ExecutionTime { get; set; }
        public TimeSpan SqlTime { get; set; }
        public QueryStatistics(QueryDescriptor descriptor)
        {
            Descriptor = descriptor;
        }
    }

    public enum QueryImplementation { LinqByPass, AdoNet }

    public enum QueryStatus { Undefined, Started, Finished }

    public class SqlInfo
    {
        public SqlStmtType StmtType { get; set; }
        public string Statement { get; set; }
        public object[] Parameters { get; set; }
        public QueryStatistics Statistics { get; set; }

        public SqlInfo(SqlStmtType stmtType, string statement, object[] parameters)
        {
            StmtType = stmtType;
            Statement = statement;
            Parameters = parameters;
        }
    }

    internal class QueryAdoNetImplement<TResult> : IEnumerable<TResult>
    {
        SmartDataContext m_Ctx = null;
        public QueryAdoNetImplement(SmartDataContext ctx)
        {
            m_Ctx = ctx;
        }

        #region IEnumerable<TResult> Members

        public IEnumerator<TResult> GetEnumerator()
        {
            return new QueryAdoNetImplement<TResult>.QueryAdoNetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion

        public class QueryAdoNetEnumerator : IEnumerator<TResult>
        {
            public QueryAdoNetEnumerator()
            {
                Reset();
            }

            #region IEnumerator<TResult> Members

            public TResult Current
            {
                get { throw new NotImplementedException(); }
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            #endregion

            #region IEnumerator Members

            object System.Collections.IEnumerator.Current
            {
                get { throw new NotImplementedException(); }
            }

            public bool MoveNext()
            {
                throw new NotImplementedException();
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }

            #endregion
        }
    }

    public class QueryDescriptor
    {
        public string Description { get; set; }
        public QueryImplementation Implementation { get; set; }
        public string Query { get; set; } // more generally, sql stmt, or store procedure name.
        //public Dictionary<string, string> ColumnMap { get; set; }
        //public Dictionary<string, Type> ReplaceTypes { get;  private set; }

        public QueryDescriptor()
            : this("Unnamed")
        {
        }

        public QueryDescriptor(string query)
            : this(query, query.PadLeft(80).Substring(0, 80), QueryImplementation.AdoNet)
        {
        }

        public QueryDescriptor(string query, string description)
            : this(query, description, QueryImplementation.AdoNet)
        {
        }

        public QueryDescriptor(string query, string description, QueryImplementation implementation)
        {
            Query = query;
            Description = description;
            Implementation = implementation;
            //ColumnMap = new Dictionary<string, string>();
        }

        public static explicit operator QueryDescriptor(string query)
        {
            return new QueryDescriptor(query);
        }

        public QueryDescriptor Map(string column, string property)
        {
            //ColumnMap.Add(column, property);
            return this;
        }

    }

    internal class StatisticsHelper : IDisposable
    {
        DateTime m_QueryStart;
        SmartDataContext m_Ctx;
        public QueryStatistics QueryStatistics { get; private set; }

        public StatisticsHelper(QueryDescriptor descriptor, SmartDataContext ctx)
        {
            m_QueryStart = DateTime.Now;
            ctx.StatisticsHelper = this;
            m_Ctx = ctx;
            QueryStatistics = new QueryStatistics(descriptor);
        }

        #region IDisposable Members

        public void Dispose()
        {
            QueryStatistics.ExecutionTime = DateTime.Now - m_QueryStart;
            //m_Ctx.AllStatistics.Add(QueryStatistics);
            List<SqlInfo> infoList = m_Ctx.GetSqlInfo();
            if (infoList.Count > 0)
                infoList[infoList.Count - 1].Statistics = QueryStatistics;
        }

        #endregion
    }

    internal class StatisticsHelper2 : IDisposable
    {
        DateTime m_SqlStart { get; set; }
        DateTime m_SqlEnd { get; set; }
        SmartDataContext m_Ctx;

        public StatisticsHelper2(SmartDataContext ctx)
        {
            m_Ctx = ctx;
            m_SqlStart = DateTime.Now;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (m_Ctx.StatisticsHelper != null)
            {
                m_Ctx.StatisticsHelper.QueryStatistics.SqlTime = DateTime.Now - m_SqlStart;
            }
        }

        #endregion
    }

    internal class TypeInfo
    {
        public bool IsPrimitive { get; set; }
        public Dictionary<string, PropertyInfo> PropertyMapByName { get; private set; }
        public TypeInfo()
        {
            PropertyMapByName = new Dictionary<string, PropertyInfo>();
        }
    }

    //internal class TypeInfo2
    //{
    //    public bool IsPrimitive { get; set; }
    //    public Dictionary<string, object> PropertyMapByName { get; private set; }

    //    public TypeInfo2()
    //    {
    //        PropertyMapByName = new Dictionary<string, object>();
    //    }
    //}

    internal class TypesCache
    {
        static Dictionary<Type, TypeInfo> m_TypesCache = new Dictionary<Type, TypeInfo>();
        static object m_SyncObject = new object();

        public TypeInfo GetTypeInfo(Type type)
        {
            TypeInfo ti = GetTypeInfoImpl(type);
            if (ti == null)
            {
                ti = new TypeInfo();
                ti.IsPrimitive = (int)Type.GetTypeCode(type) >= 3;
                if (!ti.IsPrimitive)
                {
                    PropertyInfo[] properties = type.GetProperties();
                    foreach (PropertyInfo pi in properties)
                    {
                        ti.PropertyMapByName[pi.Name] = pi;
                    }
                }
                SetTypeInfoImpl(type, ti);
            }
            return ti;
        }

        private TypeInfo GetTypeInfoImpl(Type type)
        {
            TypeInfo ti = null;
            lock (m_SyncObject)
            {
                if (!m_TypesCache.TryGetValue(type, out ti))
                    ti = null;
            }
            return ti;
        }

        private void SetTypeInfoImpl(Type type, TypeInfo ti)
        {
            lock (m_SyncObject)
            {
                m_TypesCache[type] = ti;
            }
        }

        //public TypeInfo2 GetTypeInfo2(Type type)
        //{
        //    TypeInfo2 ti = GetTypeInfo2Impl(type);
        //    if (ti == null)
        //    {
        //        ti = new TypeInfo2();
        //        ti.IsPrimitive = (int)Type.GetTypeCode(type) >= 3;
        //        if (!ti.IsPrimitive)
        //        {
        //            PropertyInfo[] properties = type.GetProperties();
        //            foreach (PropertyInfo pi in properties)
        //            {
        //                ti.PropertyMapByName[pi.Name] = pi;
        //                if ((int)Type.GetTypeCode(pi.PropertyType) < 3 && !(pi.PropertyType.IsValueType || (pi.PropertyType.IsArray && pi.PropertyType.GetElementType().IsPrimitive)))
        //                    ti.PropertyMapByName[pi.Name] = GetTypeInfo2(pi.PropertyType);
        //            }
        //        }
        //        SetTypeInfo2Impl(type, ti);
        //    }
        //    return ti;
        //}

        //TypeInfo2 GetTypeInfo2Impl(Type type)
        //{
        //    TypeInfo2 ti = null;
        //    lock (m_SyncObject)
        //    {
        //        if (!m_TypesCache2.TryGetValue(type, out ti))
        //            ti = null;
        //    }
        //    return ti;
        //}

        //void SetTypeInfo2Impl(Type type, TypeInfo2 ti)
        //{
        //    lock (m_SyncObject)
        //    {
        //        m_TypesCache2[type] = ti;
        //    }
        //}
    }

    //public class QueryItem<T1, T2>
    //{
    //    public T1 Item1 { get; set; }
    //    public T2 Item2 { get; set; }

    //    public QueryItem()
    //    {
    //    }

    //    public QueryItem(T1 item1, T2 item2)
    //    {
    //        Item1 = item1;
    //        Item2 = item2;
    //    }
    //}

    //public class QueryItem<T1, T2, T3>
    //{
    //    public T1 Item1 { get; set; }
    //    public T2 Item2 { get; set; }
    //    public T3 Item3 { get; set; }

    //    public QueryItem()
    //    {
    //    }

    //    public QueryItem(T1 item1, T2 item2, T3 item3)
    //    {
    //        Item1 = item1;
    //        Item2 = item2;
    //        Item3 = item3;
    //    }
    //}

    public class RateStat
    {
        public int Count { get; set; }
        private Stopwatch m_Stopwatch = new Stopwatch();
        public string Description { get; set; }

        public void Start()
        {
            Count++;
            m_Stopwatch.Start();
        }

        public void Stop()
        {
            m_Stopwatch.Stop();
        }

        public void Reset()
        {
            m_Stopwatch.Reset();
            Count = 0;
        }

        public string GetRate()
        {
            double rate = 0;
            if (Count > 0)
                rate = m_Stopwatch.Elapsed.TotalMilliseconds / Count;
            return rate.ToString("0.000");
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}: {1}/{2}", Description, Count, GetRate());
            return sb.ToString();
        }

    }

    [Serializable]
    public class SQLParameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public SqlDbType? SqlDbType { get; set; }
        public int Length { get; set; }

        public SQLParameter()
        {
        }

        public SQLParameter(string name, object value, SqlDbType? dbType = null, int length = -1)
        {
            Name = (name[0] != '@' ? "@" : "") + name;
            Value = value ?? DBNull.Value;
            SqlDbType = dbType;
            Length = length >= 0 ? length : -1;
        }

        public SqlParameter MakeSqlParameter()
        {
            if (Name[0] != '@')
                Name = '@' + Name;
            SqlParameter result = null;
            if (!SqlDbType.HasValue)
                result = new SqlParameter(Name, Value);
            else
            {
                result = new SqlParameter(Name, SqlDbType.Value, Length);
                result.Value = Value;
            }
            return result;
        }

        public static implicit operator SQLParameter(SqlParameter other)
        {
            return new SQLParameter(other.ParameterName, other.Value, new SqlDbType?(other.SqlDbType), other.Size);
        }

        public static implicit operator SqlParameter(SQLParameter other)
        {
            return other != null ? other.MakeSqlParameter() : null;
        }

    }

}
