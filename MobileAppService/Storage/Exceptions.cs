﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Storage.Storage
{
    

    [Serializable]
    public class DatabaseConnectionFailureException : Exception
    {
        const string stdMessage = "Cannot connect to database.";

        public DatabaseConnectionFailureException()
            : base(stdMessage)
        {

        }
        public DatabaseConnectionFailureException(string message)
            : base(message)
        {
        }
        public DatabaseConnectionFailureException(string message, Exception ex)
            : base(message, ex)
        {
        }
        protected DatabaseConnectionFailureException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    [Serializable]
    public class RecoverableDatabaseException : Exception
    {
        const string stdMessage = "Database recoverable error occurred.";
        public string Query { get; private set; }
        public DatabaseErrorReason Reason { get; private set; }

        public RecoverableDatabaseException()
            : base(stdMessage)
        {
        }
        public RecoverableDatabaseException(string query, DatabaseErrorReason reason)
            : this(stdMessage, query, reason)
        {
        }
        public RecoverableDatabaseException(string message, string query, DatabaseErrorReason reason)
            : base(message)
        {
            Query = query;
            Reason = reason;
        }
        public RecoverableDatabaseException(string message, string query, DatabaseErrorReason reason, Exception ex)
            : base(message, ex)
        {
            Query = query;
            Reason = reason;
        }

        protected RecoverableDatabaseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    [Serializable]
    public class UnrecoverableDatabaseException : Exception
    {
        const string stdMessage = "Database unrecoverable error occurred.";

        public UnrecoverableDatabaseException()
            : base(stdMessage)
        {
        }
        public UnrecoverableDatabaseException(string message)
            : base(string.Format("{0}: {1}", stdMessage, message))
        {
        }

        public UnrecoverableDatabaseException(string message, Exception ex)
            : base(string.Format("{0}: {1}", stdMessage, message), ex)
        {
        }

        protected UnrecoverableDatabaseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    [Serializable]
    public class DatabaseQueryCanceled : Exception
    {
        const string stdMessage = "Database query has been programmatically canceled.";

        public DatabaseQueryCanceled()
            : base(stdMessage)
        {
        }
        public DatabaseQueryCanceled(string message)
            : base(message)
        {
        }

        public DatabaseQueryCanceled(string message, Exception ex)
            : base(string.Format("{0}: {1}", stdMessage, message), ex)
        {
        }

        protected DatabaseQueryCanceled(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    [Serializable]
    public class SpaceAllocationDatabaseException : Exception
    {
        const string stdMessage = "'Could not allocate database space.";

        public SpaceAllocationDatabaseException()
            : base(stdMessage)
        {
        }
        public SpaceAllocationDatabaseException(string message)
            : base(string.Format("{0}: {1}", stdMessage, message))
        {
        }

        public SpaceAllocationDatabaseException(string message, Exception ex)
            : base(string.Format("{0}: {1}", stdMessage, message), ex)
        {
        }

        public SpaceAllocationDatabaseException(Exception ex)
            : base(stdMessage, ex)
        {
        }

        protected SpaceAllocationDatabaseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
    public enum DatabaseErrorReason
    {
        Unknown,
        Timeout,
        ConnectionTimeout,
        NetworkTimeout,
        Deadlock,
        UpdateConflict
    }

    //public enum DatabaseLockErrorReason
    //{
    //    Unknown,
    //    Timeout,
    //    Canceled,
    //    Deadlock
    //}
   
    //[Serializable]
    //class DatabaseLockException : Exception
    //{
    //    const string stdMessage = "Error occurred while locking the resource.";
    //    public string Resource { get; set; }
    //    public DatabaseLockErrorReason Reason { get; set; }

    //    public DatabaseLockException()
    //        : base(stdMessage)
    //    {
    //    }

    //    public DatabaseLockException(string resource, DatabaseLockErrorReason reason)
    //        : base(stdMessage)
    //    {
    //    }

    //    public DatabaseLockException(string message, string resource, DatabaseLockErrorReason reason)
    //        : base(message)
    //    {
    //        Resource = resource;
    //        Reason = reason;
    //    }

    //    public DatabaseLockException(string message, string resource, DatabaseLockErrorReason reason, Exception ex)
    //        : base(message, ex)
    //    {
    //        Resource = resource;
    //        Reason = reason;
    //    }

    //    protected DatabaseLockException(SerializationInfo info, StreamingContext context)
    //        : base(info, context)
    //    {
    //    }

    //}

}
