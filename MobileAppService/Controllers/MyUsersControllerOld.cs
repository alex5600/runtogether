﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using RunTogetherService.Models;
using RunTogether.DataObjects;

namespace RunTogetherService.Controllers
{
    public class MyUserController : TableController<MyUser>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            AppContext context = new AppContext();
            DomainManager = new EntityDomainManager<MyUser>(context, Request);
        }

        // GET tables/Users
        public IQueryable<MyUser> GetAllTodoItem()
        {
            //return Query(); 
            var q = Query();
            var l = q.ToList();
            return q;
        }

        // GET tables/Users/48D68C86-6EA6-4C25-AA33-223FC9A27959
        //public SingleResult<TodoItem> GetTodoItem(string id)
        //{
        //    return Lookup(id);
        //}

        //// PATCH tables/Users/48D68C86-6EA6-4C25-AA33-223FC9A27959
        //public Task<TodoItem> PatchTodoItem(string id, Delta<TodoItem> patch)
        //{
        //     return UpdateAsync(id, patch);
        //}

        // POST tables/Users
        public async Task<IHttpActionResult> PostTodoItem(MyUser item)
        {
            MyUser current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        //// DELETE tables/Users/48D68C86-6EA6-4C25-AA33-223FC9A27959
        //public Task DeleteTodoItem(string id)
        //{
        //     return DeleteAsync(id);
        //}
    }
}
