﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using RunTogetherService.Models;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Config;
using Microsoft.Azure.Mobile.Server.Tables;
using Storage.Storage;
using RunTogether.DataObjects;
using RunTogether;
using System.Reflection;

namespace RunTogetherService.Controllers
{


    [MobileAppController]
    public class MyTableController<T> : ApiController where T : class
    {
        protected  string TableName;
        protected DataManager DataManager;
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            // AppContext context = new AppContext();
            DataManager = new DataManager(AppDatabaseSettings.Settings);

        }

      
        protected virtual string SelectQuery => $"select * from {TableName}";


        public virtual IEnumerable<T> GetItems()
        {
           
            var q = DataManager.ExecuteQuery<T>(SelectQuery);
            // var l = q.ToList();
            return q.ToList();
        }

        [HttpPost]
        public virtual string Insert( T item)
        {
            IBaseDataObject i = item as IBaseDataObject;
            string res = null;
            if (i != null && !string.IsNullOrEmpty(i.Id))
            {
                res = GuidFactory.CreateGuid().ToString();
                i.Id = res;
            }

            var props = typeof(T).GetProperties().Where(p => CanWrite(p));
            string fields = string.Join(",", props.Select(p => p.Name));
            List<object> data = new List<object>();
            foreach(var p in props)
            {
                data.Add(p.GetValue(item));
            }
            DataManager.ExecuteInsertCommand(TableName, fields, data.ToArray());
            return res;
        }

        bool CanWrite(PropertyInfo prop)
        {

            var code = Type.GetTypeCode(prop.PropertyType);
            return code != TypeCode.Object ||
                (
                prop.PropertyType == typeof(Guid)
                );
        }


    }
}