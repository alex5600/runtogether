﻿using System.Data.Entity;

using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Tables;
using RunTogether.DataObjects;

namespace RunTogetherService.Models
{
    public class AppContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        private const string connectionStringName = "name=Model1";

        public AppContext() : base(connectionStringName)
        {

        } 

        public DbSet<MyUser> MyUsers { get; set; }
        public DbSet<AccountInfo> Accounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));


         
            modelBuilder.Entity<MyUser>()
                .HasMany<MyUser>(s => s.Friends)
                .WithMany(c => c.FriendOf)
                .Map(cs =>
               {
                   cs.ToTable("FriendShip");

               });
                //.Map(cs =>
                //{
                //    cs.MapLeftKey("UserRefId");
                //    cs.MapRightKey("UserRefId");
                //    cs.ToTable("FriendShip");
                //});



            modelBuilder.Entity<AccountInfo>().ToTable("AccountInfo");



        }
    }

}
