﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Storage.Storage;

namespace RunTogetherService.Models
{
    public class AppDatabaseSettings
    {
        static DatabaseSettings settings;
        public static DatabaseSettings Settings => settings ?? (settings = CreateSettings());

        static DatabaseSettings CreateSettings()
        {
            string name = "Model1";
            var s = new DatabaseSettings()
            {
                ConnectionString = ConfigurationManager.ConnectionStrings[name].ConnectionString,
                IsolationLevel = System.Transactions.IsolationLevel.Serializable
            };
            return s;
             
        }

    }
}