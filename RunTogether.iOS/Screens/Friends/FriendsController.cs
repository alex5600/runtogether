using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;

namespace RunTogether.Screens.Friends
{
    
    [Register("FriendsController")]
    public class FriendsController : UIViewController
    {
        public FriendsController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            

            base.ViewDidLoad();
            Title = "Friends";
            View.BackgroundColor = UIColor.Blue;

            // Perform any additional setup after loading the view
        }
    }
}