﻿using CoreGraphics;
using Shared.iOS.Components;
using SpriteKit;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace RunTogetherApp.Screens.Run
{
    class UserInfoDisplay
    {

        public SKLabelNode Label;
        public SKShapeNode Node;
        public UserInfoDisplay(CGSize size) : this(CGPoint.Empty, size)
        {


        }
        public UserInfoDisplay(CGPoint position, CGSize size)
        {

            Node = SKShapeNode.FromRect(size, 5);
            Node.FillColor = UIColor.FromRGBA(0, 0, 0, 0.3f);
            Label = new SKLabelNode("Arial");
            Label.FontColor = UIColor.White;
            Label.UserInteractionEnabled = false;

            Label.FontSize = 20;
            HorizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left;
            Label.VerticalAlignmentMode = SKLabelVerticalAlignmentMode.Center;
            Node.Add(Label);
            Node.SetPosition(position);
        }
        public SKLabelHorizontalAlignmentMode HorizontalAlignmentMode
        {
            get { return Label.HorizontalAlignmentMode; }
            set
            {
                Label.HorizontalAlignmentMode = value;
                switch (value)
                {
                    case SKLabelHorizontalAlignmentMode.Center:
                        Label.Position = new CGPoint(0, 0);
                        break;
                    case SKLabelHorizontalAlignmentMode.Left:
                        Label.Position = new CGPoint(-Node.Frame.Width / 2, 0);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
