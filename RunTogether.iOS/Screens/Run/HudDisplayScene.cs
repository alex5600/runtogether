﻿using CoreAnimation;
using CoreGraphics;
using Foundation;
using Shared;
using Shared.Core;
using Shared.iOS.Components;
using SpriteKit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using UIKit;
using RunTogetherApp.Code;
using RunTogether.Shared.RunSpace;
using RunTogether.DataObjects;

namespace RunTogetherApp.Screens.Run
{

    class HudDisplayScene : SpriteKit.SKScene
    {
        public HudDisplayScene()
        {
           
        }

        SKLabelNode Vel;
        UIView view;
        DisplayLabel DisplayVelocity;
        DisplayLabel DistanceNode;
        LocalRunSpaceManager sessionManager;
        public HudDisplayScene(UIView view, CGSize size, LocalRunSpaceManager sessionManager) : base(size)
        {

            this.view = view;
            this.sessionManager = sessionManager;
            if (sessionManager != null)
            {
                sessionManager.PositionUpdated += SessionManager_PositionUpdate;
               // sessionManager.StateChanged += SessionManager_StateChanged;
            }
            var s = new CGSize(120, 40);
            DisplayVelocity = new DisplayLabel(s);
            DisplayVelocity.Node.SetPosition(new CGPoint((size.Width - DisplayVelocity.Node.Frame.Width) / 2, 0));
            DisplayVelocity.Label.Text = "AAAAAA";
            Add(DisplayVelocity.Node);

            DistanceNode = new DisplayLabel(s);
            DistanceNode.Node.Position = DisplayVelocity.Node.Position + new CGSize(120, 0);
            DistanceNode.Label.Text = "AAAAAA";
            Add(DistanceNode.Node);

            Vel = new SKLabelNode("Arial");

            Vel.FontColor = UIColor.White;

            Vel.FontSize = 30;
            Vel.Position = new CGPoint(size.Width / 2, size.Height / 2);
            Vel.Alpha = 0;
            Add(Vel);
            Velocity = sessionManager == null ? 5: sessionManager.Actor.Velocity;
            AddPanRecognizer();
            NSNotificationCenter.DefaultCenter.AddObserver((NSString)NSObject.FromObject(NotificationName.VelocityChanged), (n) =>
           {
               NSNumber v = (NSNumber)n.Object;
               Velocity = v.DoubleValue;
           });
            
        }

        //private void SessionManager_StateChanged(StateChangeEvent obj)
        //{
        //    //throw new NotImplementedException();
        //    var runners = sessionManager.GetRunners();
        //    RemoveChildren(runnerNodes.Select(i => i.Frame).ToArray());
        //    runnerNodes.Clear();
        //    int start = 50;
        //    foreach (var r in runners)
        //    {
        //        DisplayRunnerState(r, start);
        //        start += 50;
        //    }
        //}
        DateTime lastUpdate;
        
        object gate = new object();
        private void SessionManager_PositionUpdate()
        {
            var now = DateTime.Now;
            if ((now- lastUpdate).TotalSeconds > 1)
            {
                BeginInvokeOnMainThread(() =>
                {
                    lastUpdate = now;
                    var d = UnitsConverter.Distance.MsToMiles(sessionManager.Actor.Distance);
                    string s = string.Format("{0:F2} mi", d);
                    DistanceNode.Label.Text = s;

                    var runners = sessionManager.GetRunners().Where(r => Math.Abs(r.Distance - sessionManager.Actor.Distance) <= Settings.Fov).OrderByDescending(r => r.Distance);
                    var deleted = runnerNodes.Where(r => !runners.Contains(r.Tag as Runner)).ToList();
                    foreach (var item in deleted)
                    { 
                        runnerNodes.Remove(item);
                    }
                    RemoveChildren(deleted.Select(i => i.Node).ToArray());
                    //runnerNodes.Clear();        
                    foreach (var r in runners)
                    {
                        DisplayRunnerState(r);
                       
                    }
                });
            }
        }
        List<DisplayLabel> runnerNodes = new List<DisplayLabel>();
        void DisplayRunnerState(Runner item)
        {
            var s = new CGSize(160, 40);
            var n = runnerNodes.Where(r => r.Tag == item).SingleOrDefault();
            if (n == null)
            {
                int pos = 50 + 50 * runnerNodes.Count;
                n = new DisplayLabel(s) { Tag = item };
                n.Label.FontSize = 15;
                n.HorizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left;
                runnerNodes.Add(n);
                n.Node.SetPosition(new CGPoint(this.Frame.Width - n.Node.Frame.Width, pos));
                Add(n.Node);
            }
            string distance = UnitsPresenter.Distance.ToSmallerUnit(item.Distance - sessionManager.Actor.Distance);
            string vel = UnitsPresenter.Speed.ToString(item.Velocity);
            var text = string.Format("{0}: {1}, {2}", item.User.Name, distance, vel);
            n.Label.Text = text;
        }

        private double velocity;
        public double Velocity
        {
            get { return velocity; }
            set
            {
                velocity = value;
                string vel =  UnitsPresenter.Speed.ToString(velocity);
                Vel.Text = vel;
                DisplayVelocity.Label.Text = vel;
            }
        }
        void AddPanRecognizer()
        {
            var gs = new TrackingPanGectureRecognizer(HandleDrag);
            //  gs.MinimumNumberOfTouches = 1;
            // gs.NumberOfTouchesRequired = 1;
            // skContainerOverlay.Ad

            var touch = new UITapGestureRecognizer(OnTouch);
            touch.NumberOfTapsRequired = 1;
            touch.NumberOfTouchesRequired = 1;
            view.GestureRecognizers = new UIGestureRecognizer[] { gs};
        }
        private void OnTouch(UITapGestureRecognizer recognizer)
        {
            LoggingService.LogDebug("OnTouch began");
            Vel.RemoveAllActions();
            Vel.Alpha = 1;
            SKAction a = SKAction.FadeOutWithDuration(2);
            Vel.RunAction(a);
            //if (recognizer.State == UIGestureRecognizerState.Began)
            //{
            //    LoggingService.LogDebug("OnTouch began");
            //    Vel.Alpha = 1;
            //}
            //else
            //{
            //    LoggingService.LogDebug("OnTouch end");
            //    SKAction a = SKAction.FadeOutWithDuration(2);
            //    Vel.RunAction(a);
            //}
        }
        private void HandleDrag(UIPanGestureRecognizer recognizer)
        {
            double largeStep = 0.01;
            double smallStep = 0.001;
            // If it's just began, cache the location of the image
            if (recognizer.State == UIGestureRecognizerState.Began)
            {
              //  LoggingService.LogDebug("HandleDrag began");
                Vel.RemoveAllActions();
                Vel.Alpha = 1;
            }

            // Move the image if the gesture is valid
            if (recognizer.State != (UIGestureRecognizerState.Cancelled | UIGestureRecognizerState.Failed
                | UIGestureRecognizerState.Possible))
            {
                CGPoint start = (recognizer as TrackingPanGectureRecognizer).Start;
                CGPoint t = recognizer.TranslationInView(view);
                var dx = t.X - start.X;
                var dy = -(t.Y - start.Y);
                bool large = (Math.Abs(dx) > Math.Abs(dy) / 2);
                double vel;
                if (large)
                    vel = velocity + largeStep * dx;
                else
                    vel = velocity + smallStep * dy;
                if (vel < 0)
                    vel = 0;
                if (vel > Settings.MaxVelocity)
                    vel = Settings.MaxVelocity;
                // Velocity = vel;

                NSNotificationCenter.DefaultCenter.PostNotificationName(NotificationName.VelocityChanged, NSObject.FromObject(vel));
               // start = t;

            }
            if (recognizer.State == UIGestureRecognizerState.Ended)
            {
                 SKAction a = SKAction.FadeOutWithDuration(2);
                 Vel.RunAction(a);

            }
        }

    }
}
