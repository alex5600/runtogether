using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CoreGraphics;
using Foundation;
using RunTogether.DataObjects;
using Shared.Geometry;
using RunTogether.Extensions;
using RunTogether.Shared.RunSpace;
using RunTogetherApp.Code;
using SceneKit;
using Shared.Core;
using UIKit;
namespace RunTogetherApp.Screens.Run
{

    [Register("BaseSessionViewController")]
    class BaseSessionViewController : UIViewController
    {

        protected SCNScene scene;
        protected internal RunnerNode ActorNode;
        public Runner Actor;
        protected List<RunnerNode> RunnerNodes;
        protected SCNView ScnView;

       
        public BaseSessionViewController()
        {
            Backward = true;
            RunnerNodes = new List<RunnerNode>();
            CameraPosition = new SCNVector3(0, 2, 0);
        }
        public BaseSessionViewController(IntPtr handle) : base(handle)
        {
            Backward = true;
            RunnerNodes = new List<RunnerNode>();
            CameraPosition = new SCNVector3(0, 2, 0);
        }
        protected LocalRunSpaceManager sessionManager;
        protected bool Backward { get; set; }
       
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
           //  sessionManager.Start()
            
        }
        public override void ViewDidDisappear(bool animated)
        {
           // UpdateTimer.Dispose();
            base.ViewDidDisappear(animated);
        }

       
        protected virtual void CreateBackground()
        {
            //var floorGeo = new SCNFloor();
            //floorGeo.Reflectivity = 0.0f;

            // var floorGeo = SCNTube.Create(98, 102, 2f);
            var floorGeo = SCNPlane.Create(2, 1000);
            //floorGeo
            var floorMaterial = new SCNMaterial();
            var image = UIImage.FromFile("art.scnassets/road.jpg");
            floorMaterial.Diffuse.Contents = image;
            floorMaterial.Normal.Contents = image;
            floorMaterial.DoubleSided = true;
            floorMaterial.Diffuse.WrapS = SCNWrapMode.Repeat;
            floorMaterial.Diffuse.WrapT = SCNWrapMode.Repeat;
            floorMaterial.Normal.WrapS = SCNWrapMode.Repeat;
            floorMaterial.Normal.WrapT = SCNWrapMode.Repeat;
          //  floorMaterial.Normal.ContentsTransform = SCNMatrix4.Scale(5f);
           // floorMaterial.Diffuse.ContentsTransform = SCNMatrix4.Scale(5f);

            floorGeo.FirstMaterial = floorMaterial;
            var floorNode = SCNNode.FromGeometry(geometry: floorGeo);
            floorNode.Position = new SCNVector3(0, 0, 0);
            floorNode.Rotation = new SCNVector4(1, 0, 0, (float) Math.PI / 2);
            scene.RootNode.AddChildNode(floorNode);

            scene.Background.Contents = UIImage.FromFile("art.scnassets/skybox.jpg");
            scene.Background.WrapS = SCNWrapMode.Repeat;
            scene.Background.WrapT = SCNWrapMode.Repeat;
        }


        protected virtual void CreateScene()
        {
            scene = SCNScene.Create();
            //// retrieve the SCNView
            CGRect rc = new CGRect(0, 0, 150, 150);
            ScnView = new SCNView(rc);
            ScnView.Frame = rc;
            Add(ScnView);
          
            ScnView.Scene = scene;
        }
      
        protected virtual void CreateActorNode()
        {
            ActorNode  = RunnerNode.CreateActor(Actor);
            var camera = new SCNCamera() { ZFar = Settings.Fov };
            camera.XFov = 20;
            camera.YFov = 20;
            ActorNode.Node.Camera = camera;
            ActorNode.Node.Position = Actor.Location.ConvertToVector()  + CameraPosition;
            scene.RootNode.AddChildNode(ActorNode.Node);
            ScnView.PointOfView = ActorNode.Node;
        }
      

        protected virtual SCNVector3 CameraPosition { get;  set; }
        
        public override void ViewDidLoad()
        {
           // UIApplication.SharedApplication.De
            base.ViewDidLoad();
            sessionManager = ServiceContainer.GetService<LocalRunSpaceManager>();
            //sessionManager.StateChanged += SessionManager_StateChanged;
            sessionManager.PositionUpdated += OnPositionUpdate;
            CreateScene();
            CreateActorNode();
            CreateBackground();
            OnPositionUpdate();
            //SessionManager_StateChanged(new StateChangeEvent() { Items = sessionManager.GetRunners(), Status = UpdateStatus.Added });
            Start();
        }


        protected virtual void OnPositionUpdate()
        {
            lock (gate)
            {
                UpdateState();
                int pos = 0;
                foreach (var runner in RunnerNodes)
                {
                    //bool pass = Math.Abs(runner.Runner.Distance - Actor.Distance) <= 10;
                    runner.Node.Position = GeometryExtensions.ConvertToVector(runner.Runner.Location) + new SCNVector3(pos++ - 2, 0, 0);
                    if (runner.Runner.NextStep != null)
                        LookAt(runner.Node, runner.Runner.NextStep.Location.ConvertToVector(), 0, 0);
                }
                ActorNode.Node.Position = ActorNode.Runner.Location.ConvertToVector();// + CameraPosition;
                LookAt(ActorNode.Node, Actor.NextStep.Location.ConvertToVector(), Backward ? 0 : Math.PI, 0.3);
            }
        }

        protected static SCNVector3 direction = new SCNVector3(0, 0, 1);
        public virtual void LookAt(SCNNode node, SCNVector3 target, double offset, double pitch)
        {
            //var loc = new SCNVector3(target);
            //loc.Normalize();
            //var da = (float)(offset+ Math.Acos(SCNVector3.Dot(direction, loc)));
            //var axis = SCNVector3.Cross(direction, loc);
            //axis.NormalizeFast();
            //node.Rotation = new SCNVector4(axis, da);
            //node.Rotation = new SCNVector4(0, 1, 0, (float)offset);
          //  node.EulerAngles = new SCNVector3(,  (float)offset, 0);
        }

        object gate = new object();

        private void UpdateState()
        {
            var runners = sessionManager.GetRunnersInFov();
            var deletedItems = this.RunnerNodes.Where(r => !runners.Select(s => s.Id).Contains(r.Runner.Id)).ToArray();
            foreach (var s in deletedItems)
            {
                RunnerNodes.Remove(s);
                s.RemoveFromScene();
            }

            foreach (var runner in runners)
            {
                var node = RunnerNodes.Where(i => i.Runner.Id == runner.Id).SingleOrDefault();
                if (node == null)
                {
                    node = CreateRunner(runner);
                    node.Node.Position = new SCNVector3(0, 0, 15);
                    scene.RootNode.AddChildNode(node.Node);
                    RunnerNodes.Add(node);
                }
                node.UpdateRunner(runner);
            }
        }

                
        //private void SessionManager_StateChanged(StateChangeEvent evt)
        //{
        //    lock (gate)
        //    {
                
        //        if (evt.Status == UpdateStatus.Deleted)
        //        {
        //            var deletedItems = this.RunnerNodes.Where(r => evt.Items.Select(s => s.UserId).Contains(r.Runner.UserId)).ToArray();
        //            foreach (var s in deletedItems)
        //            {
        //                RunnerNodes.Remove(s);
        //                s.RemoveFromScene();             
        //            }
        //            return;
        //        }

        //        if (evt.Status == UpdateStatus.Added)
        //        {
        //            IEnumerable<Runner> newItems = evt.Items;

        //            //newItems = runners.Where(r => !RunnerNodes.Select(s => s.Runner.UserId).Contains(r.UserId)).ToArray();
        //            foreach (var s in newItems)
        //            {
        //                if (s.UserId != Actor.UserId)
        //                {
        //                    //    var pos  =   new SCNVector3(0, 0, 0);
        //                    // var runner = RunnerNode.Create(s, scene, "art.scnassets/Messi.dae", "art.scnassets/MessiWalk.dae");
        //                    var runner = CreateRunner(s);
        //                    runner.Node.Position = new SCNVector3(0, 0, 15);
        //                    scene.RootNode.AddChildNode(runner.Node);
        //                    RunnerNodes.Add(runner);
        //                }
        //            }
        //        }
        //        if (evt.Status == UpdateStatus.Updated)
        //        {
        //            foreach (var s in evt.Items)
        //            {
        //                var node = RunnerNodes.Where(r => r.Runner.UserId == s.UserId).FirstOrDefault();
        //                if (node != null)
        //                    node.UpdateRunner(s);
        //            }
        //        }
        //    }
        //}

        protected virtual RunnerNode CreateRunner(Runner r)
        {
            return RunnerNode.Create(r, scene, "art.scnassets/Messi.dae", "art.scnassets/MessiWalk.dae");
        }

        public void Start()
        {
            activateWalking(true);
           
        }
        public void Stop()
        {
            activateWalking(false);
           
        }
        public void Pause()
        {

        }
        PositionUpdater updater;
        void activateWalking(bool start)
        {
            if (start)
            {
                updater = new PositionUpdater();
                updater.Start(100);
                foreach (var r in RunnerNodes.Where(i => i.Runner.State == TripState.Running))
                {
                    r.ActivateWalking(start);
                }
                sessionManager.Start(updater);
            }
            else
            {
                sessionManager.Stop();
                updater.Stop();
            }
        }
          
      
     

        public override bool ShouldAutorotate()
        {
            return true;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return UIInterfaceOrientationMask.AllButUpsideDown;
        }
    }

    class PositionUpdater : IPositionUpdater
    {
        Timer timer;
        public void Start(int interval)
        {
            timer = new Timer((_) =>
            {
                Update(DateTimeOffset.Now);
            }, null, interval, interval);
        }
        public void Stop()
        {
            if (timer != null)
                timer.Dispose();
        }
        public event Action<DateTimeOffset> Update;
    }
}

