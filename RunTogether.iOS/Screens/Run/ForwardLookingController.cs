﻿using CoreGraphics;
using SceneKit;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using UIKit;
using RunTogetherApp.Code;
using System.Threading;
using Foundation;
using AVFoundation;
using SpriteKit;
using CoreMedia;
using Shared;
using Shared.Core;
using Shared.iOS.Extensions;
using Shared.Geometry;
using RunTogether.Shared.RunSpace;
using RunTogether.Extensions;

namespace RunTogetherApp.Screens.Run
{
    [Register("ForwardLookingController")]
    class ForwardLookingController:BaseSessionViewController
    {
       
        public ForwardLookingController()
        {
            Backward = false;
            CameraPosition = new SCNVector3(0, 2, -2);
        }
        public ForwardLookingController(IntPtr handle) : base(handle)
        {
            Backward = false;
            CameraPosition = new SCNVector3(0, 2, -2);
        }

        protected override void CreateBackground()
        {

        }

        protected override void CreateActorNode()
        {
            //base.CreateActorNode();
            //ActorNode.Node.Camera.ZFar = Settings.Fov;
            //ActorNode.Node.Camera.XFov = 0;
            //ActorNode.Node.Camera.YFov = 0;
            //ActorNode.Node.AddChildNode(screenNode);
            ActorNode = RunnerNode.CreateActor(Actor);
            var camera = new SCNCamera() { ZFar = Settings.Fov };
            camera.XFov = 0;
            camera.YFov = 0;
            ActorNode.Node.AddChildNode(screenNode);
            var cameraNode = SCNNode.Create();
            cameraNode.Camera = camera;
            cameraNode.Position = CameraPosition;// new SCNVector3(0, 0, -2);
            ActorNode.Node.AddChildNode(cameraNode);
            // ActorNode.Node.Camera = camera;
            ActorNode.Node.Position = Actor.Location.ConvertToVector();// + CameraPosition;
            scene.RootNode.AddChildNode(ActorNode.Node);
            ScnView.PointOfView = cameraNode;// ActorNode.Node;
        }

        public override void LookAt(SCNNode node, SCNVector3 target, double offset, double pitch)
        {
            //var loc = new SCNVector3(target);
            //loc.Normalize();
            //var da = (float)(offset+ Math.Acos(SCNVector3.Dot(direction, loc)));
            //var axis = SCNVector3.Cross(direction, loc);
            //axis.NormalizeFast();
            //node.Rotation = new SCNVector4(axis, da);
            //node.Rotation = new SCNVector4(0, 1, 0, (float)offset);
            node.EulerAngles = new SCNVector3((float)pitch, (float)offset , 0);
        }
        //public override void LookAt(SCNNode node, SCNVector3 target)
        //{
        //    var loc = new SCNVector3(target);
        //    loc.Normalize();
        //    var da = (float)(Math.PI - Math.Acos(SCNVector3.Dot(direction, loc)));
        //    var axis = SCNVector3.Cross(direction, loc);
        //    axis.NormalizeFast();
        //    node.Rotation = new SCNVector4(direction, da);
        //}
        //protected override RunnerNode CreateRunner(Runner r)
        //{
        //    return RunnerNode.Create(r, scene, "art.scnassets/Messi.dae", "art.scnassets/MessiWalk.dae");
        //}
        //protected override void UpdatePositions(object state)
        //{
        //    base.UpdatePositions(state);
        //    var d = Actor.Distance;
        //    if (ActorNode.Node.Position.Z - sphereNode.Position.Z > 10)
        //        sphereNode.Position = ActorNode.Node.Position;

        //}

        AVPlayer player;

        SCNNode screenNode;
        protected override void CreateScene()
        {
            scene = SCNScene.Create();
            //// retrieve the SCNView
            //CGRect rc = Backward ? new CGRect(0, 0, 150, 150) : View.Frame;
            ////new CGRect(0, 0, UIScreen.MainScreen.ApplicationFrame.Width, UIScreen.MainScreen.ApplicationFrame.Height);
            //ScnView = new SCNView(rc);
            //ScnView.Frame = rc;
            //Add(ScnView);
            ScnView = View as SCNView;

            ScnView.UserInteractionEnabled = true;
            ScnView.Scene = scene;
            //ScnView.ShowsStatistics = true;
            screenNode = CreateScreen();
            CreateHud();

        }

        SCNNode CreateScreen()
        {
            float screenDistance = 50;
            float scaleFactor = 300f;
            var videoWidth = 1280;
            var videoHeight = 720;
            //View.AddConstraint()
            string fileName = sessionManager.Route.VideoInfo.File;
            var _asset = AVAsset.FromUrl(NSUrl.FromFilename(fileName));
            var _playerItem = new AVPlayerItem(_asset);
            SKVideoNode videoNode;
            player = new AVPlayer(_playerItem);
            player.Play();
            //player.Rate = 0.5f;
            NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification, (n) =>
            {
                var t1 = new CMTime(5, 100);
                player.Seek(t1);
                player.Play();
            });

            NSNotificationCenter.DefaultCenter.AddObserver((NSString)NSObject.FromObject(NotificationName.VelocityChanged), (n) =>
            {
                var v = ((NSNumber)n.Object).DoubleValue;
                Actor.Velocity = v;
                sessionManager.ChangeState(Actor, UpdateStatus.Updated);
                var rate = v / sessionManager.Route.VideoInfo.Speed;
                player.Rate = (float)rate;
            });

           
            var screen = SCNPlane.Create(View.Frame.Width * screenDistance / scaleFactor, View.Frame.Height * screenDistance / scaleFactor);
            var screenNode = new SCNNode() { Geometry = screen };

            // scene.RootNode.AddChildNode(screenNode);

            // assign singleton AVVIdeoPlayer As VideoNode within SKSc
            videoNode = SKVideoNode.FromPlayer(player);
            var spritescene = new SKScene(new CGSize(videoWidth, videoHeight));
            videoNode.Position = new CGPoint(x: spritescene.Size.Width / 2, y: spritescene.Size.Height / 2);

            videoNode.Size = spritescene.Size;
            videoNode.XScale = 1.0f;
            spritescene.AddChild(videoNode);
            videoNode.ZRotation = (float)Math.PI;

            // assign SKScene-embedded video to tube geometry
            var mat = new SCNMaterial();
            mat.DoubleSided = true;
            mat.Diffuse.Contents = spritescene;

            screen.Materials = new SCNMaterial[] { mat };
            screenNode.Position = new SCNVector3(0, 0, -screenDistance);
            screenNode.Rotation = new SCNVector4(0, 1, 0, (float)Math.PI);
            return screenNode;
        }

        HudDisplayScene hudDisplay;
        protected void CreateHud()
        {
            hudDisplay = new HudDisplayScene(ScnView, View.Bounds.Size, sessionManager);
            ScnView.OverlayScene = hudDisplay;
            ScnView.OverlayScene.Hidden = false;
            ScnView.OverlayScene.ScaleMode = SKSceneScaleMode.ResizeFill; // Make sure SKScene bounds are the same as our SCNScene
            ScnView.OverlayScene.UserInteractionEnabled = false;
            
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            LoggingService.LogDebug("TouchesBegan");
            UITouch touch = touches.AnyObject as UITouch;
            base.TouchesBegan(touches, evt);
            var viewTouchLocation = touch.LocationInView(ScnView);
            var c1 = ScnView.OverlayScene.ConvertPointFromView(viewTouchLocation);
            var touched = ScnView.OverlayScene.GetNodesAtPoint(c1);

        }

        //protected override void OnPositionUpdate()
        //{
        //    base.OnPositionUpdate();
        //    hudDisplay.Distance = Actor.Distance;
        //}
    }
}
