using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using RunTogetherApp.Code;
using CoreGraphics;
using Shared.Core;

using RunTogether.Screens.Chat;
using RunTogether.Shared.RunSpace;

namespace RunTogetherApp.Screens.Run
{
   
    [Register("MainController")]
    public class MainController : UIViewController
    {
        public MainController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }
       
        BaseSessionViewController normal;
        UIButton stop;
       // BaseSessionViewController backward;
        //HUDController hud;
        public override void ViewDidLoad()
        {
                   
            base.ViewDidLoad();
            var lm = ServiceContainer.GetService<LocalRunSpaceManager>();
            normal =  AppDelegate.Storyboard.InstantiateInitialViewController() as ForwardLookingController;
          //  normal.View.Frame = new CGRect(0, 50, UIScreen.MainScreen.ApplicationFrame.Width, UIScreen.MainScreen.ApplicationFrame.Height - 50);
            normal.Actor = lm.Actor;
            Add(normal.View);
            AddChildViewController(normal);

            stop = new UIButton(new CGRect(0, 0, 40, 40));
            stop.BackgroundColor = UIColor.Red;
            stop.TouchUpInside += (s, e) =>
            {
                this.NavigationController.PopViewController(false);
            };
            Add(stop);
            View.BringSubviewToFront(stop);

            var call = new UIButton(new CGRect(42, 0, 40, 40));
            call.Layer.CornerRadius = 10;
           // call.ImageView.Image = UIImage.FromFile("Images/video_camera.png");
            call.TouchUpInside += Call;
            call.BackgroundColor = UIColor.Blue;
            View.Add(call);
         
            View.BringSubviewToFront(call);
            this.NavigationController.NavigationBarHidden = true;
            //backward = new BaseSessionViewController();
            //backward.Actor = lm.Actor;
            ////  backward.Backward = true;
            //backward.View.Frame = new CGRect(0, 0, 150, 150);
            //AddChildViewController(backward);
            //Add(backward.View);
            //View.BringSubviewToFront(normal.View);
            
        }

        OpenTokController chat = null;
        private void Call(object sennder, EventArgs e)
        {
            if (chat == null)
            {
                chat = new OpenTokController();
                this.AddChildViewController(chat);

                Add(chat.View);
                nfloat y = View.Frame.Height - chat.View.Frame.Height;
                var frame = new CGRect(0, y, View.Frame.Width, chat.View.Frame.Height);
                //frame.Offset(0, 200);
                chat.View.Frame = frame;
                View.BringSubviewToFront(chat.View);
                chat.Start();
            }

        }

        public override bool ShouldAutorotate()
        {
            return true;// base.ShouldAutorotate();
        }
        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return UIInterfaceOrientationMask.Landscape;
        }


    }
}