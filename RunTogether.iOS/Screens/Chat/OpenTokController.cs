using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using System.Diagnostics;
using OpenTok;
using AVFoundation;

namespace RunTogether.Screens.Chat
{


    public class OpenTokController : UIViewController, IOTPublisherDelegate
    {
        string _apiKey;
        string _token;
        string _sessionId;

        OTSession _session;
        OTPublisher _publisher;
        OTSubscriber _subscriber;
        string _archiveId;

        int Width = 100;
        SessionDelegate handler;
        MyOTPublisherDelegate pubDel;
        public OpenTokController()
        {
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.BackgroundColor = UIColor.Clear;
            this.View.Frame = new CoreGraphics.CGRect(0, 0, UIScreen.MainScreen.ApplicationFrame.Width, Width);
            AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video);
            // Releases the view if it doesn't have a superview.
            handler = new SessionDelegate(this);
            pubDel = new MyOTPublisherDelegate(this);
           // getCredentials();
        }

        public void Start()
        {
            getCredentials();
        }

        void getCredentials()
        {

            _apiKey = @"45711182";
            _token = @"T1==cGFydG5lcl9pZD00NTcxMTE4MiZzaWc9Y2NjNDU5NGI0YzRiODIwMTFkMmE0YWI5ODliMjg3NWMzMTcwYzExODpzZXNzaW9uX2lkPTJfTVg0ME5UY3hNVEU0TW41LU1UUTNPRE13T0RjM05UWXhOMzVrZWtoVlVtOVljR1p0VldSeWJFSXdNR2haTVVSSFpqaC1mZyZjcmVhdGVfdGltZT0xNDc4MzA4ODE3Jm5vbmNlPTAuNzQ1NzM3Njk0MTkzNDAzNSZyb2xlPW1vZGVyYXRvciZleHBpcmVfdGltZT0xNDgwOTA0NDM1";
            _sessionId = @"2_MX40NTcxMTE4Mn5-MTQ3ODMwODc3NTYxN35kekhVUm9YcGZtVWRybEIwMGhZMURHZjh-fg";
            Connect();
        }
        void Connect()
        {
            //Initialize a new instance of OTSession and begin the connection process.
            _session = new OTSession(_apiKey, _sessionId, handler);

            OTError error = null;
            _session.ConnectWithToken(_token, out error); ;
            if (error != null)
            {
                Debug.WriteLine($"Unable to connect to session {error.LocalizedDescription}");
            }
            else
                Debug.WriteLine("Connected to session");

        }
        
        public void doPublish()
        {
            _publisher = new OTPublisher(pubDel);
           
            _publisher.View.Frame = new CoreGraphics.CGRect(0, 0, Width, Width);
            this.View.AddSubview(_publisher.View);

            OTError error = null;
            _session.Publish(_publisher, out error);
            if (error != null)
            {
                Debug.WriteLine($"Unable to publish to session {error.LocalizedDescription}");
            }
            else
                Debug.WriteLine("publish to session");
        }
        public void StreamCreated(OTSession session, OTStream stream)
        {
            Debug.WriteLine($"session streamCreated {stream.StreamId}");
            _subscriber = new OTSubscriber(stream, null);
          
            _subscriber.DidConnectToStream += _subscriber_DidConnectToStream;
            OTError error = null;
            _session.Subscribe(_subscriber, out error);
            if (error != null)
            {
                Debug.WriteLine($"Unable to Subscribe to session {error.LocalizedDescription}");
            }
            else
                Debug.WriteLine("Subscribe to session");

        }

        private void _subscriber_DidConnectToStream(object sender, EventArgs e)
        {
            Debug.WriteLine($"subscriberDidConnectToStream {_subscriber.Stream.Connection.ConnectionId}") ;
            _subscriber.View.Frame = new CoreGraphics.CGRect(View.Frame.Width - Width, 0, Width, Width);
            View.AddSubview(_subscriber.View);
        }
        class MyOTPublisherDelegate : OTPublisherDelegate
    {
        IOTPublisherDelegate handler;
        public MyOTPublisherDelegate(IOTPublisherDelegate handler)
        {
            this.handler = handler;

        }

    }
    class MyOTSubsriberDelegate : OTSubscriberKitDelegate
    {
        IOTSubscriberKitDelegate handler;
        public MyOTSubsriberDelegate(IOTSubscriberKitDelegate handler)
        {
            this.handler = handler;

        }

    }

    class SessionDelegate : OTSessionDelegate
    {
            OpenTokController c;
        public SessionDelegate(OpenTokController c)
        {
            this.c = c;
        }
        public override void DidConnect(OTSession session)
        {

            c.doPublish();
        }

        public override void DidFailWithError(OTSession session, OTError error)
        {
            // base.DidFailWithError(session, error);

            Debug.WriteLine($"session DidFailWithError {error.Description}");
        }
        public override void StreamCreated(OTSession session, OTStream stream)
        {
            Debug.WriteLine($"session streamCreated {stream.StreamId}");
            c.StreamCreated(session, stream);
        }

        public override void ConnectionDestroyed(OTSession session, OTConnection connection)
        {

        }
        
        public override void DidDisconnect(OTSession session)
        {

        }
        
        public override void ReceivedSignalType(OTSession session, string type, OTConnection connection, string stringData)
        {

        }
      
       
       
        public override void StreamDestroyed(OTSession session, OTStream stream)
        {

        }


    }
    }

   
}