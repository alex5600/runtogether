using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using RunTogether.Screens.Routes;

namespace RunTogether.Screens
{


    public class TabController : UITabBarController
    {

        UIViewController tab1, tab2;

        public TabController()
        {
            tab1 = new RoutesViewController();
          //  tab1.Title = "Routes";
          //  tab1.View.BackgroundColor = UIColor.Green;

            tab2 = new Friends.FriendsController();
          //  tab2.Title = "Orange";
            //tab2.View.BackgroundColor = UIColor.Orange;

          

            var tabs = new UIViewController[] {
                                tab1, tab2
                        };

            ViewControllers = tabs;
        }
    }
}