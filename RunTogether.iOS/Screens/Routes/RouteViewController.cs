using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using RunTogether.DataObjects;
using RunTogether.Shared.ViewModel;
using System.Linq;
using DSoft.UI.Grid;
using DSoft.Datatypes.Grid.Data;
using System.Collections.ObjectModel;
using DSoft.Datatypes.Formatters;
using DSoft.Datatypes.Types;
using Shared;
using CoreGraphics;
using Cirrious.FluentLayouts.Touch.RowSet;
using Cirrious.FluentLayouts.Touch;
using RunTogetherApp.Screens.Run;

namespace RunTogether.Screens.Routes
{
  

    [Register("RouteViewController")]
    public class RouteViewController : UIViewController
    {
        RouteInfo route;
        RouteViewModel model;
        UserDataTable table;
        private DSGridView mGridView;
        int height = 40;
        public RouteViewController( RouteInfo route)
        {
            this.route = route;
          
           
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

           
        }

        public override async void ViewDidLoad()
        {
           
            base.ViewDidLoad();
            Title = "Route";
            model = new RouteViewModel(route);
            await  model.LoadModelAsync();

            table = new UserDataTable();
            this.View.BackgroundColor = UIColor.White;

            //var aFrame = this.View.Frame;
            ////aFrame.Height -= 50;
            var h = this.NavigationController.NavigationBar.Frame.Height;
            mGridView = new DSGridView(new CGRect(0, h, View.Frame.Width, View.Frame.Height -h -height));
            foreach (var u in model.ActiveRunners)
                table.Runners.Add(u);
            mGridView.DataSource = table;
            //turn on showing of the selection
            mGridView.ShowSelection = true;
            //allow the scrolling to bounce
            mGridView.Bounces = false;
            
            View.Add(mGridView);
            CreateButtonPanel();
          

        }
        public override void ViewDidAppear(bool animated)
        {
            NavigationController.NavigationBarHidden = false;
            base.ViewDidAppear(animated);
        }

        void CreateButtonPanel()
        {
            var view = new UIView(new CGRect(0, 0, 200, 50));
            view.Alpha = 0.5f;

            View.Add(view);
            //View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            var b = new UIButton();
            b.Layer.CornerRadius = 10;
            b.TouchUpInside += Start;
            b.SetTitle("Start", UIControlState.Normal);
            b.BackgroundColor = UIColor.Red;
            view.Add(b);

            var b2 = new UIButton();
            b2.Layer.CornerRadius = 10;
            b2.TouchUpInside += Join;
            b2.SetTitle("Join", UIControlState.Normal);
            b2.BackgroundColor = UIColor.Red;
            view.Add(b2);

          
            var rowSet = new RowSetTemplate()
            {
                TopMargin = 1f,
                BottomMargin = 2f,
                VInterspacing = 1f
            };
            var equalWeightRowTemplate = new RowTemplate()
            {
                HInterspacing = 12f,
                LeftMargin = 12f,
                RightMargin = 12f,

            };

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            View.AddConstraints(
               view.WithSameBottom(View),
               view.WithSameCenterX(View),
               view.Height().EqualTo(height),
               view.Width().EqualTo(250)
               );

            view.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            view.AddConstraints(
                rowSet.Generate(view,
                    new Row(equalWeightRowTemplate, b, b2)
                ));
            view.AddConstraints(
                b.Height().EqualTo(30),
                b.Width().EqualTo(100),
                b.WithSameCenterY(view),
                b2.Height().EqualTo(30),
                b2.Width().EqualTo(100),
                b2.WithSameCenterY(view)
               // call.WithSameCenterY(view),
                );

        }

        private void Start(object sender, EventArgs e)
        {
            this.NavigationController.PushViewController(new MainController(), false);
        }

        private void Call(object sender, EventArgs e)
        {
            this.NavigationController.PushViewController(new MainController(), false);
        }
        private void Join(object sender, EventArgs e)
        {
            this.NavigationController.PushViewController(new MainController(), false);
        }
    }


    public class UserDataTable : DSDataTable
    {

       
        public ObservableCollection<Runner> Runners { get; private set; }

        public bool HasData
        {
            get
            {
                return (Runners.Count != 0);
            }
        }
       

      
        public UserDataTable()
        {
            Runners = new ObservableCollection<Runner>();
            var dc1 = new DSDataColumn("ICON");
            dc1.Caption = "";
            dc1.ReadOnly = true;
            dc1.DataType = typeof(UIImage);
            dc1.AllowSort = false;
            dc1.Width = 75.0f;
            dc1.Formatter = new DSImageFormatter(new DSSize(dc1.Width, dc1.Width))
            {
                Margin = new DSInset(2.0f),
            };

            this.Columns.Add(dc1);


            // Create a column
            var dc2 = new DSDataColumn("User");
            dc2.Caption = "User";
            dc2.ReadOnly = true;
            dc2.DataType = typeof(String);
            dc2.AllowSort = true;
            dc2.Width = 200;
            this.Columns.Add(dc2);

            dc2 = new DSDataColumn("Velocity");
            dc2.Caption = "Velocity";
            dc2.ReadOnly = true;
            dc2.DataType = typeof(string);
            dc2.AllowSort = true;
            dc2.Width = 200;
            this.Columns.Add(dc2);

            dc2 = new DSDataColumn("Distance");
            dc2.Caption = "Distance";
            dc2.ReadOnly = true;
            dc2.DataType = typeof(string);
            dc2.AllowSort = true;
            dc2.Width = 100;
            this.Columns.Add(dc2);
            //// Create a column
            //dc2 = new DSDataColumn("Artist");
            //dc2.Caption = "Artist";
            //dc2.ReadOnly = true;
            //dc2.DataType = typeof(String);
            //dc2.AllowSort = false;
            //dc2.Width = 200;
            //this.Columns.Add(dc2);
        }

        

        #region Methods
        /// <summary>
        /// Clears the rows.
        /// </summary>
        public void ClearRows()
        {
            Rows.Clear();
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Gets the row count.
        /// </summary>
        /// <returns>The row count.</returns>
        public override int GetRowCount()
        {
            return Runners.Count;
        }

        /// <summary>
        /// Gets the row at the specified indexs
        /// </summary>
        /// <returns>The row.</returns>
        /// <param name="Index">Index.</param>
        public override DSDataRow GetRow(int Index)
        {
            DSDataRow aRow = null;
            var item = Runners[Index];

            if (Index < Rows.Count)
            {
                aRow = Rows[Index];
            }
            else
            {
                aRow = new DSDataRow();
                Rows.Add(aRow);
            }

            aRow["User"] = item.User.Name;
            aRow["Velocity"] = UnitsPresenter.Speed.ToString(item.Velocity);
            aRow["Distance"] = UnitsPresenter.Distance.ToBigggerUnit(item.Velocity);
            
            aRow["ICON"] = UIImage.FromFile("Images/monkey.png");

            return aRow;
        }

        #endregion
    }
}