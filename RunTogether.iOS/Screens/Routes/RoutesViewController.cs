﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CoreAnimation;
using System.Linq;
using CoreGraphics;
using Foundation;
using RunTogether.Shared.ViewModel;
using UIKit;
using RunTogether.DataObjects;
using Shared.iOS.Extensions;

using ObjCRuntime;
using Shared.Core;

namespace RunTogether.Screens.Routes
{

    //class RoutesSource : UICollectionViewSource
    //{

    //    RouteInfoModel model;
    //    public RoutesSource(RouteInfoModel model)
    //    {
    //        this.model = model;
    //    }

    //    //public List<Route> Rows { get; private set; }

    //    public Single FontSize { get; set; }

    //    // public SizeF ImageViewSize { get; set; }

    //    public override nint NumberOfSections(UICollectionView collectionView)
    //    {
    //        return 1;
    //    }

    //    public override nint GetItemsCount(UICollectionView collectionView, nint section)
    //    {
    //        // return base.GetItemsCount(collectionView, section);
    //        return model.Routes.Count();
    //    }
    //    //public override nint GetItemsCount(UICollectionView collectionView, Int32 section)
    //    //{
    //    //    return Rows.Count;
    //    //}

    //    public override Boolean ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        return true;
    //    }

    //    public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        var cell = (RouteInfoCell)collectionView.CellForItem(indexPath);
    //        cell.ImageView.Alpha = 0.5f;
    //    }

    //    public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        var cell = (RouteInfoCell)collectionView.CellForItem(indexPath);
    //        cell.ImageView.Alpha = 1;

    //        RouteInfo row = model.Routes.ElementAt(indexPath.Row);
    //        //row.Tapped?.Invoke(row);
    //        Tapped?.Invoke(row);
    //    }

    //    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        var cell = (RouteInfoCell)collectionView.DequeueReusableCell(RouteInfoCell.CellID, indexPath);

    //        RouteInfo row = model.GetElement(indexPath.Row);

    //        cell.UpdateRow(row, FontSize);//, ImageViewSize);

    //        return cell;
    //    }

    //    public Action<RouteInfo> Tapped { get; set; }
    //}



    //class RouteInfoCell : UICollectionViewCell
    //{
    //    public static NSString CellID = new NSString("UserSource");

    //    [Export("initWithFrame:")]
    //    public RouteInfoCell(RectangleF frame)
    //        : base(frame)
    //    {
    //        ImageView = new UIImageView();
    //        ImageView.Layer.BorderColor = UIColor.DarkGray.CGColor;
    //        ImageView.Layer.BorderWidth = 1f;
    //        ImageView.Layer.CornerRadius = 3f;
    //        //   ImageView.Layer.MasksToBounds = true;
    //        ContentView.AddSubview(ImageView);


    //        // ImageView.Frame = new CGRect(0, 0, 150, 150);
    //        LabelView = new UILabel();
    //        LabelView.BackgroundColor = UIColor.Clear;
    //        LabelView.TextColor = UIColor.DarkGray;
    //        LabelView.TextAlignment = UITextAlignment.Center;

    //        //ContentView.AddSubview(LabelView);


    //       // ContentView.Frame = new CGRect(0, 0, 160, 150);//ImageView.Frame;

    //        ImageView.Center = ContentView.Center;
    //        ImageView.F

    //    }

    //    public UIImageView ImageView { get; private set; }

    //    public UILabel LabelView { get; private set; }


    //    public void UpdateRow(RouteInfo element, Single fontSize)//, SizeF imageViewSize)
    //    {
    //        LabelView.Font = UIFont.FromName("HelveticaNeue-Bold", fontSize);
    //        LabelView.Text = element.Name;
    //        var h = element.Name.StringSize(LabelView.Font).Height + 3;

    //        ImageView.Image = ImageExtensions.ToImage(element.Picture);
    //        // ImageView.ContentMode = UIViewContentMode.ScaleAspectFill;
    //        ImageView.Frame = new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height - h);


    //        LabelView.Frame = new CGRect(0, ImageView.Frame.Bottom, ContentView.Frame.Width,
    //                                         h);
    //    }
    //}

    public class RouteInfoCollectionCell : UICollectionViewCell
    {
        UIImageView imageView;
        UILabel labelView;
        public static NSString CellID = new NSString("UserSource");
        [Export("initWithFrame:")]
        public RouteInfoCollectionCell(CGRect frame) : base(frame)
        {
            // BackgroundView = new UIView { BackgroundColor = UIColor.Orange };

            // SelectedBackgroundView = new UIView { BackgroundColor = UIColor.Green };

            ContentView.Layer.BorderColor = UIColor.LightGray.CGColor;
            ContentView.Layer.BorderWidth = 1.0f;
            ContentView.Layer.CornerRadius = 3f;
            ContentView.BackgroundColor = UIColor.White;
            //ContentView.Transform = CGAffineTransform.MakeScale(0.8f, 0.8f);

            imageView = new UIImageView();// UIImage.FromBundle("Images/Route1.png"));
            imageView.ContentMode = UIViewContentMode.ScaleToFill;
            imageView.Center = ContentView.Center;

            labelView = new UILabel();
             labelView.BackgroundColor = UIColor.Clear;
             labelView.TextColor = UIColor.DarkGray;
             labelView.TextAlignment = UITextAlignment.Center;

            labelView.Font = UIFont.FromName("HelveticaNeue-Bold", 12);
            ContentView.AddSubview(labelView);
            var h = 15;
            // imageView.Transform = CGAffineTransform.MakeScale(0.7f, 0.7f);

            ContentView.AddSubview(imageView);
            imageView.Frame = new CGRect(0, 0, frame.Width, frame.Height - h);
            labelView.Frame = new CGRect(0, frame.Height - h, frame.Width, h);
        }

        public UIImage Image
        {
            set
            {
                imageView.Image = value;
            }
        }

        public string Text {  set { labelView.Text = value; } }
        //[Export("custom")]
        //void Custom()
        //{
        //    // Put all your custom menu behavior code here
        //    Console.WriteLine("custom in the cell");
        //}


        //public override bool CanPerform(Selector action, NSObject withSender)
        //{
        //    if (action == new Selector("custom"))
        //        return true;
        //    else
        //        return false;
        //}
    }

    public class GridLayout : UICollectionViewFlowLayout
    {
        public GridLayout()
        {
        }

        public override bool ShouldInvalidateLayoutForBoundsChange(CGRect newBounds)
        {
            return true;
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForItem(NSIndexPath path)
        {
            return base.LayoutAttributesForItem(path);
        }

        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(CGRect rect)
        {
            return base.LayoutAttributesForElementsInRect(rect);
        }

        //        public override SizeF CollectionViewContentSize {
        //            get {
        //                return CollectionView.Bounds.Size;
        //            }
        //        }
    }


    public class RoutesViewController : UICollectionViewController
    {
       // static NSString animalCellId = new NSString("AnimalCell");
        // static NSString headerId = new NSString("Header");
        // List<IAnimal> animals;
        RoutesViewModel ViewModel;
        public RoutesViewController(): 
            base(new UICollectionViewFlowLayout()
            {
               // HeaderReferenceSize = new CGSize(100, 40),
                // SectionInset = new UIEdgeInsets(20, 20, 20, 20),
                ScrollDirection = UICollectionViewScrollDirection.Vertical,
               // MinimumInteritemSpacing = 10, // minimum spacing between cells
                //MinimumLineSpacing = 10,
                ItemSize = new CGSize(200, 180)
            })
        {
            CollectionView.ContentInset = new UIEdgeInsets(2, 2, 2,2);
            View.BackgroundColor = UIColor.White;
        }
        public RoutesViewController(UICollectionViewLayout layout) : base(layout)
        {

        }

        public async override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewModel = new RoutesViewModel();
            await ViewModel.LoadRoutesAsync();
            CollectionView.RegisterClassForCell(typeof(RouteInfoCollectionCell), RouteInfoCollectionCell.CellID);
            CollectionView.BackgroundColor = UIColor.White;
           
        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return ViewModel.Routes.Count;
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (RouteInfoCollectionCell)collectionView.DequeueReusableCell(RouteInfoCollectionCell.CellID, indexPath);

            var item = ViewModel.GetElement(indexPath.Row);

            cell.Image = ImageExtensions.ToImage(item.Picture);
            cell.Text = item.Name;

            return cell;
        }

        //public override UICollectionReusableView GetViewForSupplementaryElement(UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
        //{
        //    var headerView = (Header)collectionView.DequeueReusableSupplementaryView(elementKind, headerId, indexPath);
        //    headerView.Text = "Supplementary View";
        //    return headerView;
        //}

        public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.CellForItem(indexPath);
            //cell.ContentView.BackgroundColor = UIColor.Yellow;
        }

        public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.CellForItem(indexPath);
            cell.ContentView.BackgroundColor = UIColor.White;
        }

        public override bool ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return true;
        }


        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var route = ViewModel.GetElement(indexPath.Row);
            LoggingService.LogDebug($"Selected {route.Name}");
            this.NavigationController.PushViewController(new RouteViewController(route), true);
           // base.ItemSelected(collectionView, indexPath);
        }

        // for edit menu
        public override bool ShouldShowMenu(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return true;
        }

        //public override bool CanPerformAction(UICollectionView collectionView, Selector action, NSIndexPath indexPath, NSObject sender)
        //{
        //    // Selector should be the same as what's in the custom UIMenuItem
        //    if (action == new Selector("custom"))
        //        return true;
        //    else
        //        return false;
        //}

        public override void PerformAction(UICollectionView collectionView, Selector action, NSIndexPath indexPath, NSObject sender)
        {
            System.Diagnostics.Debug.WriteLine("code to perform action");
        }

        // CanBecomeFirstResponder and CanPerform are needed for a custom menu item to appear
        //public override bool CanBecomeFirstResponder
        //{
        //    get
        //    {
        //        return true;
        //    }
        //}

        /*public override bool CanPerform (Selector action, NSObject withSender)
		{
			if (action == new Selector ("custom"))
				return true;
			else
				return false;
		}*/


    }

    //public class CustomFlowLayoutDelegate : UICollectionViewDelegateFlowLayout
    //{
    //    public CustomFlowLayoutDelegate()
    //    {
    //        // 
    //        //			HeaderReferenceSize = new SizeF (100, 100);
    //        //		SectionInset = new UIEdgeInsets (20, 20, 20, 20);
    //        //			ScrollDirection = UICollectionViewScrollDirection.Vertical;
    //    }

    //    public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
    //    {
    //        return new SizeF(160, 150);
           
    //    }
        
    //}


    //public class SimpleCollectionViewController : UICollectionViewController
    //{
    //    static NSString animalCellId = new NSString("AnimalCell");
    //    static NSString headerId = new NSString("Header");

    //    RouteInfoModel ViewModel;

    //    public SimpleCollectionViewController(UICollectionViewLayout layout) : base(layout)
    //    {

    //       //var flowLayout = new UICollectionViewFlowLayout(){
    //       //     HeaderReferenceSize = new CGSize (100, 100),
    //       //      SectionInset = new UIEdgeInsets (20,20,20,20),
    //       //     ScrollDirection = UICollectionViewScrollDirection.Vertical,
    //       //   MinimumInteritemSpacing = 50, // minimum spacing between cells 
    //       //   MinimumLineSpacing = 50 // minimum spacing between rows if ScrollDirection is Vertical or between columns if Horizontal 
    //       //  };

    //    }

    //    public override void ViewDidLoad()
    //    {
    //        base.ViewDidLoad();

    //        ViewModel = new RouteInfoModel();
    //        await ViewModel.LoadRoutesAsync();
    //        //CollectionView.RegisterClassForCell(typeof(AnimalCell), animalCellId);
    //        CollectionView.RegisterClassForCell(typeof(UserCell), UserCell.CellID);
    //        CollectionView.RegisterClassForSupplementaryView(typeof(Header), UICollectionElementKindSection.Header, headerId);

    //        UIMenuController.SharedMenuController.MenuItems = new UIMenuItem[] {
    //            new UIMenuItem ("Custom", new Selector ("custom"))
    //        };
    //    }

    //    public override nint NumberOfSections(UICollectionView collectionView)
    //    {
    //        return 1;
    //    }

    //    public override nint GetItemsCount(UICollectionView collectionView, nint section)
    //    {
    //        return ViewModel.Routes.Count;
    //    }

    //    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        var cell = (UserCell)collectionView.DequeueReusableCell(animalCellId, indexPath);

    //        var r = animals[indexPath.Row];

    //        animalCell.Image = animal.Image;

    //        return animalCell;
    //    }

    //    public override UICollectionReusableView GetViewForSupplementaryElement(UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
    //    {
    //        var headerView = (Header)collectionView.DequeueReusableSupplementaryView(elementKind, headerId, indexPath);
    //        headerView.Text = "Supplementary View";
    //        return headerView;
    //    }

    //    public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        var cell = collectionView.CellForItem(indexPath);
    //        cell.ContentView.BackgroundColor = UIColor.Yellow;
    //    }

    //    public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        var cell = collectionView.CellForItem(indexPath);
    //        cell.ContentView.BackgroundColor = UIColor.White;
    //    }

    //    public override bool ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        return true;
    //    }

    //    //      public override bool ShouldSelectItem (UICollectionView collectionView, NSIndexPath indexPath)
    //    //      {
    //    //          return false;
    //    //      }

    //    // for edit menu
    //    public override bool ShouldShowMenu(UICollectionView collectionView, NSIndexPath indexPath)
    //    {
    //        return true;
    //    }

    //    public override bool CanPerformAction(UICollectionView collectionView, Selector action, NSIndexPath indexPath, NSObject sender)
    //    {
    //        // Selector should be the same as what's in the custom UIMenuItem
    //        if (action == new Selector("custom"))
    //            return true;
    //        else
    //            return false;
    //    }

    //    public override void PerformAction(UICollectionView collectionView, Selector action, NSIndexPath indexPath, NSObject sender)
    //    {
    //        System.Diagnostics.Debug.WriteLine("code to perform action");
    //    }

    //    // CanBecomeFirstResponder and CanPerform are needed for a custom menu item to appear
    //    public override bool CanBecomeFirstResponder
    //    {
    //        get
    //        {
    //            return true;
    //        }
    //    }

    //    /*public override bool CanPerform (Selector action, NSObject withSender)
    //    {
    //        if (action == new Selector ("custom"))
    //            return true;
    //        else
    //            return false;
    //    }*/

    //    public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
    //    {
    //        base.WillRotate(toInterfaceOrientation, duration);

    //        //var lineLayout = CollectionView.CollectionViewLayout as LineLayout;
    //        //if (lineLayout != null)
    //        //{
    //        //    if ((toInterfaceOrientation == UIInterfaceOrientation.Portrait) || (toInterfaceOrientation == UIInterfaceOrientation.PortraitUpsideDown))
    //        //        lineLayout.SectionInset = new UIEdgeInsets(400, 0, 400, 0);
    //        //    else
    //        //        lineLayout.SectionInset = new UIEdgeInsets(220, 0.0f, 200, 0.0f);
    //        //}
    //    }

    //}

    //public class AnimalCell : UICollectionViewCell
    //{
    //    UIImageView imageView;

    //    [Export("initWithFrame:")]
    //    public AnimalCell(CGRect frame) : base(frame)
    //    {
    //        BackgroundView = new UIView { BackgroundColor = UIColor.Orange };

    //        SelectedBackgroundView = new UIView { BackgroundColor = UIColor.Green };

    //        ContentView.Layer.BorderColor = UIColor.LightGray.CGColor;
    //        ContentView.Layer.BorderWidth = 2.0f;
    //        ContentView.BackgroundColor = UIColor.White;
    //        ContentView.Transform = CGAffineTransform.MakeScale(0.8f, 0.8f);

    //        imageView = new UIImageView(UIImage.FromBundle("placeholder.png"));
    //        imageView.Center = ContentView.Center;
    //        imageView.Transform = CGAffineTransform.MakeScale(0.7f, 0.7f);

    //        ContentView.AddSubview(imageView);
    //    }

    //    public UIImage Image
    //    {
    //        set
    //        {
    //            imageView.Image = value;
    //        }
    //    }

    //    [Export("custom")]
    //    void Custom()
    //    {
    //        // Put all your custom menu behavior code here
    //        Console.WriteLine("custom in the cell");
    //    }


    //    public override bool CanPerform(Selector action, NSObject withSender)
    //    {
    //        if (action == new Selector("custom"))
    //            return true;
    //        else
    //            return false;
    //    }
    //}

    public class Header : UICollectionReusableView
    {
        UILabel label;

        public string Text
        {
            get
            {
                return label.Text;
            }
            set
            {
                label.Text = value;
                SetNeedsDisplay();
            }
        }

        [Export("initWithFrame:")]
        public Header(CGRect frame) : base(frame)
        {
            label = new UILabel() { Frame = new CGRect(0, 0, 300, 50), BackgroundColor = UIColor.Yellow };
            AddSubview(label);
        }
    }
}