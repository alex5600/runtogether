using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using CoreGraphics;
using Cirrious.FluentLayouts.Touch;
using Cirrious.FluentLayouts.Touch.RowSet;

namespace RunTogether.Sandbox
{
    

    [Register("FluentTestController")]
    public class FluentTestController : UIViewController
    {
        public FluentTestController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {


            base.ViewDidLoad();

            var view = new UIView(new CGRect(0, 100, View.Frame.Width, 200));
            view.BackgroundColor = UIColor.Green;
            view.Alpha = 0.5f;

            View.Add(view);
            //View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            var b = new UIButton();
            b.Layer.CornerRadius = 10;

            b.SetTitle("press", UIControlState.Normal);
            b.BackgroundColor = UIColor.Red;
            view.Add(b);
            var b2 = new UIButton();
            b2.Layer.CornerRadius = 10;

            b2.SetTitle("press1", UIControlState.Normal);
            b2.BackgroundColor = UIColor.Red;
            view.Add(b2);
            var rowSet = new RowSetTemplate()
            {
                TopMargin = 10f,
                BottomMargin = 20f,
                VInterspacing = 10f
            };
            var equalWeightRowTemplate = new RowTemplate()
            {
                HInterspacing = 12f,
                LeftMargin = 12f,
                RightMargin = 12f,

            };
           
            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            View.AddConstraints(
               // view.WithSameCenterY(View),
               view.Width().EqualTo(200),
               view.Height().EqualTo(200),
               view.WithSameCenterX(View)
               );

            view.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
            view.AddConstraints(

                rowSet.Generate(view,

                    new Row(equalWeightRowTemplate, b, b2)
                ));
            view.AddConstraints(
                b.Height().EqualTo(30),
                b2.Height().EqualTo(30),
                b.WithSameCenterY(view),
                b2.WithSameCenterY(view)
                );

          //  view.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
          ////  view.TranslatesAutoresizingMaskIntoConstraints = true;
          //  view.AddConstraints
          //      (

          //          b.AtTopOf(view).Plus(10),
          //          b.WithSameCenterX(view),
          //          b.Height().EqualTo(40),
          //          b.WithSameWidth(view).Minus(30),

          //          b2.Below(b).Plus(10),
          //          b2.WithSameCenterX(view),
          //          b2.Height().EqualTo(40),
          //          b2.WithSameWidth(b)
          //      );
            
           
            // Perform any additional setup after loading the view
        }
    }
}