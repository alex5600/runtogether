using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using SceneKit;
using CoreGraphics;
using SpriteKit;
using Shared.Core;
using CoreAnimation;
using RunTogetherApp.Screens;
using RunTogetherApp.Screens.Run;

namespace RunTogetherApp.Sandbox
{

    //class MyPan:UIPanGestureRecognizer
    //{
    //    public CGPoint Start;
    //    public override void TouchesBegan(NSSet touches, UIEvent evt)
    //    {
    //        base.TouchesBegan(touches, evt);
    //        Start  = this.TranslationInView(this.View);
    //    }
    //    public MyPan(Action<UIPanGestureRecognizer> action):base(action)
    //    {

    //    }
    //}
   
    public class ShapeController : UIViewController
    {
        public ShapeController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }
        SCNView view;
        SCNScene scene;
        HudDisplayScene hudDisplay;
        public override void ViewDidLoad()
        {
        

            base.ViewDidLoad();

            view = new SCNView(View.Frame);
          //  view.AllowsCameraControl = true;
            view.ShowsStatistics = true;
            // view.AutoenablesDefaultLighting = true;
            this.View.AddSubview(view);
            
   
            //View.AddConstraint()
            scene = new SCNScene();
            view.Scene = scene;

            hudDisplay = new HudDisplayScene(view, View.Bounds.Size, null);
            hudDisplay.BackgroundColor = UIColor.Blue;
            view.OverlayScene = hudDisplay;
            view.OverlayScene.Hidden = false;
            view.OverlayScene.ScaleMode = SKSceneScaleMode.ResizeFill; // Make sure SKScene bounds are the same as our SCNScene
            view.OverlayScene.UserInteractionEnabled = false;

            
            var camera = new SCNCamera();
            camera.ZFar = 500;
           
            var cameraNode = new SCNNode() { Camera = camera };
            cameraNode.Position = new SCNVector3(0, 0, 10);
            var li = new SCNLight();
            li.LightType = SCNLightType.Ambient;
            cameraNode.Light = li;
            view.AutoenablesDefaultLighting = true;
            view.UserInteractionEnabled = true;
            scene.RootNode.AddChildNode(cameraNode);
          //    view.BackgroundColor = UIColor.White;
          //  var doubleTap = new UITapGestureRecognizer(OnTap);
          //  doubleTap.NumberOfTapsRequired = 1;
          //  doubleTap.NumberOfTouchesRequired = 1;
          //  // skContainerOverlay.Ad
          ////  View.GestureRecognizers = new UIGestureRecognizer[] { doubleTap };
            
          //  var gs = new MyPan(HandleDrag);
          ////  gs.MinimumNumberOfTouches = 1;
          // // gs.NumberOfTouchesRequired = 1;
          //  // skContainerOverlay.Ad
          //  view.GestureRecognizers = new UIGestureRecognizer[] { gs };

            view.Playing = true;
            // LayoutControls();

        }

        //private void HandleDrag(UIPanGestureRecognizer recognizer)
        //{

        //    // If it's just began, cache the location of the image
        //    if (recognizer.State == UIGestureRecognizerState.Began)
        //    {
        //        LoggingService.LogDebug("HandleDrag began");
        //    }

        //    // Move the image if the gesture is valid
        //    if (recognizer.State != (UIGestureRecognizerState.Cancelled | UIGestureRecognizerState.Failed
        //        | UIGestureRecognizerState.Possible))
        //    {
        //        CGPoint start = (recognizer as MyPan).Start;
        //        CGPoint t = recognizer.TranslationInView(view);
                
        //        //LoggingService.LogDebug("Point {0}, {1}", t.X - start.X, t.Y );
        //        hudDisplay.Vel.Text = string.Format("Point {0}, {1}", t.X - start.X, t.Y - start.Y);

        //    }
        //    if (recognizer.State == UIGestureRecognizerState.Ended)
        //    {
        //        LoggingService.LogDebug("HandleDrag end");
        //        CATransaction.Begin();
        //       UIView.Animate(3, 5, UIViewAnimationOptions.CurveEaseInOut, ()=>
        //           {
        //               hudDisplay.Vel.Alpha = 0;
        //         }, null);
        //       // SKAction a = SKAction.FadeOutWithDuration(2);
        //       // skContainerOverlay.Vel.RunAction(a);

        //    }
        //}

        
        //void LayoutControls()
        //{
        //    View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
        //    view.TranslatesAutoresizingMaskIntoConstraints = true;
        //    FluentLayout[] c =
        //        {
        //        //hud.View.AtBottomOf(View),
        //        //hud.View.AtLeftOf(View),
        //        //hud.View.WithSameWidth(View),
        //        //hud.View.Height().EqualTo(40),

        //        view.AtTopOf(View),
        //        view.AtLeftOf(View),
        //        View.WithSameWidth(View),
        //        view.WithSameHeight(View)
        //        //vN.AtTopOf(View),
        //        //vN.AtLeftOf(View),
        //        //vN.WithSameWidth(View),
        //        //vN.WithSameHeight(View),

        //        //vB.AtTopOf(View),
        //        //vB.AtLeftOf(View),
        //        //vB.Width().EqualTo(200),
        //        //vB.Height().EqualTo(200)
        //    };
        //    View.AddConstraints(c);
        //}

        //bool hidden = true;
        //void OnTap(UIGestureRecognizer r)
        //{
        //    LoggingService.LogDebug("OnTap, hidden {0}", hidden);
        //    skContainerOverlay.Vel.Hidden = hidden;
        //    hidden = !hidden;
          
        //    view.SetNeedsDisplay();
        //}

        //public override void ViewWillTransitionToSize(CGSize toSize, IUIViewControllerTransitionCoordinator coordinator)
        //{
        //    LoggingService.LogDebug("ViewWillTransitionToSize:  {0}", toSize);
        //    base.ViewWillTransitionToSize(toSize, coordinator);
        //    view.Frame = new CGRect(new CGPoint(0, 0), toSize);
        //}
        //public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
        //{
        //    base.DidRotate(fromInterfaceOrientation);
        //}
        public override bool ShouldAutorotate()
        {
            return true;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return UIInterfaceOrientationMask.AllButUpsideDown;
        }

        //public override void TouchesBegan(NSSet touches, UIEvent evt)
        //{

        //    LoggingService.LogDebug("TouchesBegan");
        //    base.TouchesBegan(touches, evt);
           
        //    SKAction a = SKAction.FadeInWithDuration(0.1);
        //    hudDisplay.Vel.RunAction(a);

        //}
        //public override void TouchesEnded(NSSet touches, UIEvent evt)
        //{
        //    LoggingService.LogDebug("TouchesEnded");
        //    base.TouchesEnded(touches, evt);
        //    SKAction a = SKAction.FadeOutWithDuration(2);
        //    hudDisplay.Vel.RunAction(a);
        //}
    }
    //class SKContainerOverlay:SpriteKit.SKScene
    //{
    //    public SKContainerOverlay()
    //    {
    //        BackgroundColor = UIColor.Blue;
    //    }

    //    public SKLabelNode Vel;
    //    public SKContainerOverlay(CGSize size):base(size)
    //    {
    //        SKLabelNode l = new SKLabelNode();
           
    //        l.FontColor = UIColor.Black;
    //        l.Text = "1234560";
    //        l.FontSize = 8;
    //        var frame = SKShapeNode.FromRect(l.Frame.Size, 5);
    //        frame.FillColor = new UIColor(255, 0, 0, 0.1f);
    //       // frame.Alpha = 0.1f;
    //        frame.Position = new CGPoint(size.Width - frame.Frame.Width / 2, frame.Frame.Height / 2);
    //        l.Position = new CGPoint(0, 0);
    //        frame.Add(l);
    //       // l.Position = new CGPoint(x: size.Width / 2, y: size.Height / 2);

    //        Add(frame);

    //        Vel = new SKLabelNode("Arial");

    //        Vel.FontColor = UIColor.Black;
    //        Vel.Text = "560";
    //        Vel.FontSize = 20;
    //        Vel.Position = new CGPoint((size.Width - Vel.Frame.Width) / 2, size.Height / 2);
    //        //Vel.Alpha = 0;
    //        Add(Vel);
    //    }
    //}
}