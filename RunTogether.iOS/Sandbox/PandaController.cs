using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using SceneKit;

namespace RunTogetherApp.Sandbox
{
    
    [Register("PandaController")]
    public class PandaController : UIViewController
    {
        public PandaController()
        {
        }

       
        SCNView view;
        SCNScene scene;
       

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {


            base.ViewDidLoad();

            view = new SCNView(View.Frame);
            view.AllowsCameraControl = true;
            view.AutoenablesDefaultLighting = true;
            Add(view);
            scene = SCNScene.Create();////.FromFile("art.scnassets/level.scn");
            view.Scene = scene;


            //var l = SCNLight.Create();
            //l.LightType = SCNLightType.Directional;
            //l.Color = UIColor.White;
            //var lNode = SCNNode.Create();
            //lNode.Light = l;

            //scene.RootNode.AddChildNode(lNode);
           // SCNSceneRenderer
            SCNNode cameraNode = new SCNNode();
            cameraNode.Camera = new SCNCamera();
            cameraNode.Position = new SCNVector3(0, 0, 2);
            scene.RootNode.AddChildNode(cameraNode);
           
            var characterScene = SCNScene.FromFile("art.scnassets/panda.scn");
            var characterTopLevelNode = characterScene.RootNode.ChildNodes[0];
            //node.addChildNode(characterTopLevelNode)
            var node = SCNNode.Create();
            node.AddChildNode(characterTopLevelNode);

            scene.RootNode.AddChildNode(node);

            var cube = new SCNBox() { Width = 1, Height = 1, Length = 1, ChamferRadius = 0.2f };
            var mat = new SCNMaterial();
            cube.Materials = new SCNMaterial[] { mat };
            mat.Diffuse.ContentColor = UIColor.Blue;
            mat.Normal.ContentColor = UIColor.Blue;
            node = new SCNNode();
            node.Geometry = cube;
            node.Position = new SCNVector3(0, 0, -2);
            scene.RootNode.AddChildNode(node);

        }
    }
}