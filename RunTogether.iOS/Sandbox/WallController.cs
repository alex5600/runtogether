using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using SceneKit;

namespace RunTogetherApp.Sandbox
{
   
    public class WallController : UIViewController
    {

        SCNView view;
        SCNScene scene;
        public WallController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
          

            base.ViewDidLoad();

            view = new SCNView(View.Frame);
            view.AllowsCameraControl = true;
            view.AutoenablesDefaultLighting = true;
            Add(view);
            scene = new SCNScene();
            view.Scene = scene;
            var cube = new SCNBox() { Width = 1, Height = 1, Length = 1 , ChamferRadius = 0.2f};
            var mat = new SCNMaterial();
            cube.Materials = new SCNMaterial[] { mat };
            mat.Diffuse.ContentColor = UIColor.Red;
            mat.Normal.ContentColor = UIColor.Red;
            var node = new SCNNode();
            node.Geometry = cube;
            node.Position = new SCNVector3(0, 0, -2);
            scene.RootNode.AddChildNode(node);

            var l = SCNLight.Create();
            l.LightType = SCNLightType.Ambient;
            var lNode = SCNNode.Create();
            lNode.Light = l;

            scene.RootNode.AddChildNode(lNode);

            cube = new SCNBox() { Width = 1, Height = 1, Length = 1, ChamferRadius = 0.2f };
            mat = new SCNMaterial();
            cube.Materials = new SCNMaterial[] { mat };
            mat.Diffuse.ContentColor = UIColor.Blue;
            mat.Normal.ContentColor = UIColor.Blue;
            node = new SCNNode();
            node.Geometry = cube;
            node.Position = new SCNVector3(0, 0, 2);
            scene.RootNode.AddChildNode(node);

            // var plane = new SCNTube() { Height = 10, OuterRadius = 10, InnerRadius = 9.9f };
            var plane = SCNSphere.Create(10);
            mat = new SCNMaterial();
            mat.DoubleSided = true;
            plane.Materials = new SCNMaterial[] { mat };
            var image  = UIImage.FromFile("Images/panoramaH.png");
            mat.Diffuse.Contents = image;
            mat.Normal.Contents = image;
            mat.DoubleSided = true;
            node = new SCNNode();
            node.Geometry = plane;
           // node.Position = new SCNVector3(-4, 0, -4);
            scene.RootNode.AddChildNode(node);

            SCNNode cameraNode = new SCNNode();
            cameraNode.Camera = new SCNCamera();
            cameraNode.Position = new SCNVector3(0, 0, 0);
            scene.RootNode.AddChildNode(cameraNode);
        }
    }
}