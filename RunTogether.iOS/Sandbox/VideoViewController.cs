using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using SceneKit;
using AVFoundation;
using SpriteKit;
using CoreGraphics;
using CoreMedia;
using RunTogetherApp.Screens;
using System.Threading;
using RunTogetherApp.Screens.Run;

namespace RunTogetherApp.Sandbox
{

    public class VideoViewController : UIViewController
    {
        SCNView view;
        SCNScene scene;
       
        public VideoViewController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        
        public override void ViewDidLoad()
        {


            base.ViewDidLoad();
            view = new SCNView(View.Frame);
            //  view.AllowsCameraControl = true;
            view.ShowsStatistics = true;
            // view.AutoenablesDefaultLighting = true;
            this.View.AddSubview(view);

            //View.AddConstraint()
            scene = new SCNScene();
            view.Scene = scene;
            view.Playing = true;
           
            var camera = new SCNCamera();
            camera.ZFar = 500;

            var cameraNode = new SCNNode() { Camera = camera };
            cameraNode.Position = new SCNVector3(0, 0, 10);
            var screenNode = CreateScreen();
            cameraNode.AddChildNode(screenNode);
            
            cameraNode.Rotation = new SCNVector4(0, 1, 0, 0.2f);
            //var li = new SCNLight();
            //li.LightType = SCNLightType.Ambient;
            // cameraNode.Light = li;
            // view.AutoenablesDefaultLighting = true;
           // view.AllowsCameraControl = true;
            view.UserInteractionEnabled = true; 
            scene.RootNode.AddChildNode(cameraNode);
            CreateHud();
            new Timer((_) =>
            {
                cameraNode.Position += new SCNVector3(0, 0, 1);
            }, null, 1000, 1000);
        }

        HudDisplayScene hudDisplay;
        protected void CreateHud()
        {
            hudDisplay = new HudDisplayScene(view, View.Bounds.Size, null);
            hudDisplay.BackgroundColor = UIColor.Blue;
            view.OverlayScene = hudDisplay;
            view.OverlayScene.Hidden = false;
            view.OverlayScene.ScaleMode = SKSceneScaleMode.ResizeFill; // Make sure SKScene bounds are the same as our SCNScene
            view.OverlayScene.UserInteractionEnabled = false;
        }
        SCNNode CreateScreen()
        {
            float screenDistance = 50;
            float scaleFactor = 300f;
            var videoWidth = 1280;
            var videoHeight = 720;
            //View.AddConstraint()
          
            var _asset = AVAsset.FromUrl(NSUrl.FromFilename("Images/short.mp4"));
            var _playerItem = new AVPlayerItem(_asset);
            AVPlayer player;
            SKVideoNode videoNode;
            player = new AVPlayer(_playerItem);
            player.Play();
            NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification, (n) =>
            {
                var t1 = new CMTime(5, 100);
                player.Seek(t1);
                player.Play();
            });

            //            NSNotificationCenter.defaultCenter().addObserverForName(AVPlayerItemDidPlayToEndTimeNotification, object: player.currentItem, queue: nil)
            //{ notification in
            //   let t1 = CMTimeMake(5, 100);
            //   player.seekToTime(t1)
            //   player.play()
            //}
            // create the tube
            var screen = SCNPlane.Create(View.Frame.Width * screenDistance / scaleFactor, View.Frame.Height * screenDistance / scaleFactor);
            var screenNode = new SCNNode() { Geometry = screen };

            // scene.RootNode.AddChildNode(screenNode);

            // assign singleton AVVIdeoPlayer As VideoNode within SKSc
            videoNode = SKVideoNode.FromPlayer(player);
            var spritescene = new SKScene(new CGSize(videoWidth, videoHeight));
            videoNode.Position = new CGPoint(x: spritescene.Size.Width / 2, y: spritescene.Size.Height / 2);

            videoNode.Size = spritescene.Size;
            videoNode.XScale = 1.0f;
            spritescene.AddChild(videoNode);
            videoNode.ZRotation = (float)Math.PI;
            // assign SKScene-embedded video to tube geometry
            var mat = new SCNMaterial();
            mat.DoubleSided = true;
            mat.Diffuse.Contents = spritescene;

            screen.Materials = new SCNMaterial[] { mat };
            screenNode.Position = new SCNVector3(0, 0, -screenDistance);
            return screenNode;
        }
    }
}