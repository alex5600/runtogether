﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;

using ObjCRuntime;
using RunTogether.Shared.ViewModel;

using Shared.iOS.Extensions;

namespace RunTogether.Sandbox
{
    public class UserSource : UICollectionViewSource
    {
        public UserSource()
        {
            Rows = new List<UserElement>();
        }

        public List<UserElement> Rows { get; private set; }

        public Single FontSize { get; set; }

        public SizeF ImageViewSize { get; set; }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            // return base.GetItemsCount(collectionView, section);
            return Rows.Count;
        }
        //public override nint GetItemsCount(UICollectionView collectionView, Int32 section)
        //{
        //    return Rows.Count;
        //}

        public override Boolean ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return true;
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            base.ItemSelected(collectionView, indexPath);
        }
        public override void ItemDeselected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            base.ItemDeselected(collectionView, indexPath);
        }
        public override bool ShouldSelectItem(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return base.ShouldSelectItem(collectionView, indexPath);
        }
        public override bool ShouldDeselectItem(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return base.ShouldDeselectItem(collectionView, indexPath);
        }
        public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (UserCell)collectionView.CellForItem(indexPath);
            cell.ImageView.Alpha = 0.5f;
        }

        public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (UserCell)collectionView.CellForItem(indexPath);
            cell.ImageView.Alpha = 1;

            UserElement row = Rows[indexPath.Row];
            //row.Tapped?.Invoke(row);
            Tapped?.Invoke(row);
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (UserCell)collectionView.DequeueReusableCell(UserCell.CellID, indexPath);

            UserElement row = Rows[indexPath.Row];

            cell.UpdateRow(row, FontSize, ImageViewSize);

            return cell;
        }

        public Action<UserElement> Tapped { get; set; }
    }

    public class UserElement
    {
        public UserElement(String caption, UIImage image)
        {
            Caption = caption;
            Image = image;
            //Tapped = tapped;
        }

        public String Caption { get; set; }

        public UIImage Image { get; set; }

        // public Action<UserElement> Tapped { get; set; }
    }

    public class UserCell : UICollectionViewCell
    {
        public static NSString CellID = new NSString("UserSource");

        [Export("initWithFrame:")]
        public UserCell(RectangleF frame)
            : base(frame)
        {
            ImageView = new UIImageView();
            ImageView.Layer.BorderColor = UIColor.DarkGray.CGColor;
            ImageView.Layer.BorderWidth = 1f;
            ImageView.Layer.CornerRadius = 3f;
            ImageView.Layer.MasksToBounds = true;
            //ImageView.ContentMode = UIViewContentMode.ScaleAspectFill;

            ContentView.AddSubview(ImageView);


            ImageView.Frame = new CGRect(0, 0, 150, 150);
            //LabelView = new UILabel();
            //LabelView.BackgroundColor = UIColor.Clear;
            //LabelView.TextColor = UIColor.DarkGray;
            //LabelView.TextAlignment = UITextAlignment.Center;

            //ContentView.AddSubview(LabelView);
            ContentView.Frame = ImageView.Frame;
        }

        public UIImageView ImageView { get; private set; }

        // public UILabel LabelView { get; private set; }

        public void UpdateRow(UserElement element, Single fontSize, SizeF imageViewSize)
        {
            // LabelView.Text = element.Caption;
            ImageView.Image = element.Image;

            // LabelView.Font = UIFont.FromName("HelveticaNeue-Bold", fontSize);

            //LabelView.Frame = new CoreGraphics.CGRect(0, ImageView.Frame.Bottom, imageViewSize.Width,
            //                                 ContentView.Frame.Height - ImageView.Frame.Bottom);
        }
    }

    public partial class UserController : UIViewController
    {
        private UserSource userSource;
        UICollectionView collectionViewUser;
        public UserController()

        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var l = new UICollectionViewFlowLayout();
            l.SectionInset = new UIEdgeInsets(10, 10, 10, 10);
            Title = "Collection";
            collectionViewUser = new UICollectionView(this.View.Frame, l);
            View.AddSubview(collectionViewUser);
            View.BringSubviewToFront(collectionViewUser);
            collectionViewUser.UserInteractionEnabled = true;
            userSource = new UserSource();
            userSource.Tapped = elementTapped;
            userSource.FontSize = 11f;
            userSource.ImageViewSize = new SizeF(90, 52.64f);

            // collectionViewUser.ContentInset = new UIEdgeInsets(50, 0, 0, 0);

            collectionViewUser.RegisterClassForCell(typeof(UserCell), UserCell.CellID);
            collectionViewUser.ShowsHorizontalScrollIndicator = false;
            collectionViewUser.Source = userSource;
            //  collectionViewUser.Delegate = new CustomFlowLayoutDelegate();
            var image = UIImage.FromFile("Images/Monument.jpg");
            userSource.Rows.Add(new UserElement("Name 1", image));
            userSource.Rows.Add(new UserElement("Name 2", image));
            userSource.Rows.Add(new UserElement("Name 3", image));

            collectionViewUser.ReloadData();
        }

        private void elementTapped(UserElement elt)
        {
            new UIAlertView("Tapped", elt.Caption, null, "OK", null).Show();
        }
    }

    public class CustomFlowLayoutDelegate : UICollectionViewDelegateFlowLayout
    {
        public CustomFlowLayoutDelegate()
        {
            // 
            //			HeaderReferenceSize = new SizeF (100, 100);
            //		SectionInset = new UIEdgeInsets (20, 20, 20, 20);
            //			ScrollDirection = UICollectionViewScrollDirection.Vertical;
        }

        public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
        {
            return new SizeF(150, 150);
            //if (indexPath.Row % 2 == 0)
            //{
            //    return new SizeF(100, 100);
            //}
            //else
            //{
            //    return new SizeF(50, 50);
            //}
        }
        //public override SizeF GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout,
        //                                      NSIndexPath indexPath)
        //{
        //    if (indexPath.Row % 2 == 0)
        //    {
        //        return new SizeF(100, 100);
        //    }
        //    else
        //    {
        //        return new SizeF(50, 50);
        //    }
        //}
    }

    public interface IAnimal
	{ 
 		string Name { get; } 

 
 		UIImage Image { get; }
    }
    public class Monkey : IAnimal
    {
        public Monkey()
        {
        }

        public string Name
        {
            get
            {
                return "Monkey";
            }
        }

        public UIImage Image
        {
            get
            {
                return UIImage.FromFile("Images/Route1.png");
            }
        }

    }


    public class SimpleCollectionViewController : UICollectionViewController
    {
        static NSString animalCellId = new NSString("AnimalCell");
        // static NSString headerId = new NSString("Header");
        // List<IAnimal> animals;
        RoutesViewModel ViewModel;
        public SimpleCollectionViewController(UICollectionViewLayout layout) : base(layout)
        {
            
        }

        public  async override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewModel = new RoutesViewModel();
            await ViewModel.LoadRoutesAsync();
            CollectionView.RegisterClassForCell(typeof(AnimalCell), animalCellId);
           // CollectionView.RegisterClassForSupplementaryView(typeof(Header), UICollectionElementKindSection.Header, headerId);

            UIMenuController.SharedMenuController.MenuItems = new UIMenuItem[] {
                new UIMenuItem ("Custom", new Selector ("custom"))
            };
        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return ViewModel.Routes.Count;
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var animalCell = (AnimalCell)collectionView.DequeueReusableCell(animalCellId, indexPath);

            var animal = ViewModel.GetElement(indexPath.Row);

            animalCell.Image = ImageExtensions.ToImage(animal.Picture);

            return animalCell;
        }

        //public override UICollectionReusableView GetViewForSupplementaryElement(UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
        //{
        //    var headerView = (Header)collectionView.DequeueReusableSupplementaryView(elementKind, headerId, indexPath);
        //    headerView.Text = "Supplementary View";
        //    return headerView;
        //}

        public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.CellForItem(indexPath);
            cell.ContentView.BackgroundColor = UIColor.Yellow;
        }

        public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.CellForItem(indexPath);
            cell.ContentView.BackgroundColor = UIColor.White;
        }

        public override bool ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return true;
        }



        //      public override bool ShouldSelectItem (UICollectionView collectionView, NSIndexPath indexPath)
        //      {
        //          return false;
        //      }

        // for edit menu
        public override bool ShouldShowMenu(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return true;
        }

        public override bool CanPerformAction(UICollectionView collectionView, Selector action, NSIndexPath indexPath, NSObject sender)
        {
            // Selector should be the same as what's in the custom UIMenuItem
            if (action == new Selector("custom"))
                return true;
            else
                return false;
        }

        public override void PerformAction(UICollectionView collectionView, Selector action, NSIndexPath indexPath, NSObject sender)
        {
            System.Diagnostics.Debug.WriteLine("code to perform action");
        }

        // CanBecomeFirstResponder and CanPerform are needed for a custom menu item to appear
        public override bool CanBecomeFirstResponder
        {
            get
            {
                return true;
            }
        }

        /*public override bool CanPerform (Selector action, NSObject withSender)
		{
			if (action == new Selector ("custom"))
				return true;
			else
				return false;
		}*/

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            base.WillRotate(toInterfaceOrientation, duration);

            //var lineLayout = CollectionView.CollectionViewLayout as LineLayout;
            //if (lineLayout != null)
            //{
            //    if ((toInterfaceOrientation == UIInterfaceOrientation.Portrait) || (toInterfaceOrientation == UIInterfaceOrientation.PortraitUpsideDown))
            //        lineLayout.SectionInset = new UIEdgeInsets(400, 0, 400, 0);
            //    else
            //        lineLayout.SectionInset = new UIEdgeInsets(220, 0.0f, 200, 0.0f);
            //}
        }

    }

    public class AnimalCell : UICollectionViewCell
    {
        UIImageView imageView;

        [Export("initWithFrame:")]
        public AnimalCell(CGRect frame) : base(frame)
        {
           // BackgroundView = new UIView { BackgroundColor = UIColor.Orange };

           // SelectedBackgroundView = new UIView { BackgroundColor = UIColor.Green };

            ContentView.Layer.BorderColor = UIColor.LightGray.CGColor;
            ContentView.Layer.BorderWidth = 2.0f;
            ContentView.BackgroundColor = UIColor.White;
            //ContentView.Transform = CGAffineTransform.MakeScale(0.8f, 0.8f);

            imageView = new UIImageView(UIImage.FromBundle("Images/Route1.png"));
            imageView.ContentMode = UIViewContentMode.ScaleToFill;
            imageView.Center = ContentView.Center;
            
           // imageView.Transform = CGAffineTransform.MakeScale(0.7f, 0.7f);

            ContentView.AddSubview(imageView);
            imageView.Frame = new CGRect(0, 0, frame.Width, frame.Height);
        }

        public UIImage Image
        {
            set
            {
                imageView.Image = value;
            }
        }

        [Export("custom")]
        void Custom()
        {
            // Put all your custom menu behavior code here
            Console.WriteLine("custom in the cell");
        }


        public override bool CanPerform(Selector action, NSObject withSender)
        {
            if (action == new Selector("custom"))
                return true;
            else
                return false;
        }
    }

    public class Header : UICollectionReusableView
    {
        UILabel label;

        public string Text
        {
            get
            {
                return label.Text;
            }
            set
            {
                label.Text = value;
                SetNeedsDisplay();
            }
        }

        [Export("initWithFrame:")]
        public Header(CGRect frame) : base(frame)
        {
            label = new UILabel() { Frame = new CGRect(0, 0, 300, 50), BackgroundColor = UIColor.Yellow };
            AddSubview(label);
        }
    }

}