using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;

using RunTogether.Screens.Chat;

namespace RunTogether.Sandbox
{
    

    [Register("MainViewController")]
    public class MainTokController : UIViewController
    {

        OpenTokController chat;
        public MainTokController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
           
            base.ViewDidLoad();
            View.BackgroundColor = UIColor.Red;
            chat = new OpenTokController();
            this.AddChildViewController(chat);

            Add(chat.View);
            var frame = chat.View.Frame;
            frame.Offset(0, 200);
            chat.View.Frame = frame;
            View.BringSubviewToFront(chat.View);
            chat.Start();
            // Perform any additional setup after loading the view
        }
    }
}