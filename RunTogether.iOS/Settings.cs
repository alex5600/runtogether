﻿using Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace RunTogetherApp
{
    static class Settings
    {
        public static double MaxVelocity = UnitsConverter.Speed.MphToMps(10);
        public static double Fov = 100;
        //public static bool MetricUnit { get; set; }
    }
   
}
