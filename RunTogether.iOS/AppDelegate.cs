﻿using System;

using Foundation;
using UIKit;
using RunTogetherApp.Screens;
using RunTogetherApp.Code;

using System.Threading;
using System.Collections.Generic;
using Shared.Core;
using RunTogetherApp.Sandbox;
using System.Linq;
using RunTogether.Screens;
using RunTogether.Sandbox;
using RunTogether.Shared.Utils;
using RunTogether.Screens.Routes;
using CoreGraphics;
using RunTogether.Shared.RunSpace;
using RunTogether.DataObjects;
using RunTogether.Shared.Routes;

namespace RunTogetherApp
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {

        // public ISessionController SessionController;
        public static UIStoryboard Storyboard = UIStoryboard.FromName("Main1", null);
        public override UIWindow Window
        {
            get;
            set;
        }
        //public Runner Actor;
        // Runner user;
       
        public override  bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {

            //Google.Maps.MapServices.ProvideAPIKey(MapsApiKey);
            // Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();
            RunTogether.Shared.ViewModel.ViewModelBase.Init(true);
            CreateSampleSession();
            //SessionController.UpdateRunner(mn.CreateThirdUserRunner());
            // create a new window instance based on the screen size
            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            // If you have defined a root view controller, set it here:

            Window.RootViewController =  new UINavigationController( new RoutesViewController());// simpleCollectionViewController;

            // Window.RootViewController = new UserController();//  TabController();
            // make the window visible
            Window.MakeKeyAndVisible();
            // StartUpdateRunner();

            
            return true;

        }
        Timer timer;
        LocalRunSpaceManager lm;
        SessionManager mn = new SessionManager();
        MockRuntimeManager proxy;
        void CreateSampleSession()
        {
            proxy = new MockRuntimeManager();
            lm = new LocalRunSpaceManager(proxy);
            var route = new RouteFactory().GetRoute(Guid.Empty);
            lm.Route = route;
            lm.Init();
            ServiceContainer.AddService<LocalRunSpaceManager>(lm);
            //var mn = new SessionManager();
            var actor = mn.CreateCurrentUserRunner(route);
            lm.Actor = actor;
            var users = mn.CreateUsers(3);
            var runners = new List<Runner>();
            runners.Add(actor);
            runners.AddRange(users);

            foreach (var r in runners)
            {
                proxy.ChangeState(r, UpdateStatus.Added);
                //lm.Runners[r.Id] = r;

            }
               


           
           
            timer = new Timer(ManageUsers, null, 20000, 20000);
        }
        int index = 0;
        void ManageUsers(object state)
        {
            // bool add = index % 4 != 0;
            bool add = true;
            if (add)
            {
               Runner r = new Runner()
                {
                    Id = Guid.NewGuid().ToString(),
                    Velocity = 3,

                    User  = mn.GetSecondUser(),
                   
                    RouteId = StraightRoute.RouteId,
                    Distance = lm.Actor.Distance + 10 * (index % 3),
                    State = TripState.Running,
                };

                proxy.ChangeState(r, UpdateStatus.Added);
            }
            else
            {
                var r = lm.GetRunners();
                foreach(var _item in r)
                proxy.ChangeState(_item, UpdateStatus.Deleted);
            }
            index++;
        }

        public override void OnResignActivation(UIApplication application)
        {
        }

        // This method should be used to release shared resources and it should store the application state.
        // If your application supports background exection this method is called instead of WillTerminate
        // when the user quits.
        public override void DidEnterBackground(UIApplication application)
        {
        }

        // This method is called as part of the transiton from background to active state.
        public override void WillEnterForeground(UIApplication application)
        {
        }

        // This method is called when the application is about to terminate. Save data, if needed.
        public override void WillTerminate(UIApplication application)
        {
        }
    }
}
