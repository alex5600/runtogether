﻿using CoreAnimation;
using IOSSupport.Extensions;
using SceneKit;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Shared.Core;
using RunTogether.DataObjects;

namespace RunTogetherApp.Code
{

    class RunnerNode
    {
        public SCNNode Node { get;  private set; }
        SCNNode Skeleton;
        CAAnimation WalkAnimation;
        
        public Runner Runner { get; private set; }
        public static RunnerNode CreateActor(Runner runner)
        {
            var node = SCNNode.Create();
            //node.Position = new SCNVector3(0, 5, runner.Origin);
            return new RunnerNode()
            {
                Node = node,
                Runner = runner
            };
        }

        public virtual void RemoveFromScene()
        {
            Skeleton.RemoveAllAnimations();
            Skeleton.RemoveFromParentNode();
            Node.RemoveFromParentNode();
        }
        
        public static RunnerNode Create(Runner runner,SCNScene parent, string sceneFile, string animationFile)
        {
           // var s = SCNScene.FromFile(sceneFile);
            var root = SCNScene.FromFile(sceneFile).RootNode;
            parent.RootNode.AddChildNode(root.ChildNodes[1]);
            //if (removeLights)
            //{
            //    var lights = root.ChildNodes.Where(i => i.Light != null).ToArray();
            //    foreach (var l in lights)
            //        l.RemoveFromParentNode();
            //}
            //var s  = 
            var skeleton = root.FindChildNode("Armtr", recursively: true);
            skeleton.Scale = new SCNVector3(x: 0.3f, y: 0.3f, z: 0.3f);
            //UIColor color;
            //switch(index++)
            //{
            //    case 1: color = UIColor.Blue;
            //        break;
            //    case 2: color = UIColor.Red;
            //        break;
            //    default:
            //        color = null;
            //        break;
            //}
            //if (color != null)
            //{
            //    var mat = root.FindChildNode("Messi", true).Geometry.Materials.Where(m => m.Name == "Jersey").SingleOrDefault();
            //    if (mat != null)
            //    {
            //        mat.Diffuse.ContentColor = color;
            //        mat.Normal.ContentColor = color;
            //    }
            //}
            var node  = SCNNode.Create();
          
            node.AddChildNode(skeleton);
            var anim = AnimationExtensions.AnimationWithSceneNamed(animationFile);
           // anim.FadeInDuration = 0.3f;

           // anim.FadeOutDuration = 0.5f;
           
            RunnerNode r = new RunnerNode()
            {
                Runner = runner,
                Node = node,
                Skeleton = skeleton,
                WalkAnimation = anim

            };
            
            if (runner.State == TripState.Running)
                r.ActivateWalking(true);
           
            return r;
        }
        
      
        public void UpdateRunner(Runner item)
        {
            if (Runner.State == TripState.Running && item.State != TripState.Running)
                ActivateWalking(false);
            if (Runner.State != TripState.Running && item.State == TripState.Running)
                ActivateWalking(true);
            Runner = item;
            WalkAnimation.Duration = 1 + (Runner.Velocity - 5) / 5f;
           // LoggingService.LogDebug("user {0}, velocity {1}", Runner.UserId, Runner.Velocity);
        }
        bool actionNotAdded = true;
        public void ActivateWalking(bool start)
        {

            if (start)
            {
                if (actionNotAdded)
                {
                    Skeleton.AddAnimation(WalkAnimation, key: "Walk");
                    actionNotAdded = false;
                }
            }
            else
            {
                Skeleton.RemoveAnimation(key: "Walk", duration: 0.3f);
                actionNotAdded = true;
            }
        }

    }
}
