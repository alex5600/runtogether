﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using RunTogether.DataObjects;
using Shared.iOS.Extensions;
using RunTogether.Shared.Routes;

namespace RunTogetherApp.Code
{
    class SessionManager
    {
        static User mUser;
        static User mUser2;
        static User mUser3;
        public User GetCurrentUser()
        {
            if (mUser == null)
            {
                mUser = new User()
                {
                    Name = "Alex",
                    Id = "CAA9F1C6-3D35-4570-9F50-B6581DCD7C1F"
                };
            }
            return mUser;

        }
        public User GetSecondUser()
        {
            if (mUser2 == null)
            {
                mUser2 = new User()
                {
                    Name = "Lena",
                    Id = "61B815F6-EA74-4CB4-801D-D80BFB90A4BE"
                };
            }
            return mUser2;

        }

        public User GetThirdUser()
        {
            if (mUser3 == null)
            {
                mUser3 = new User()
                {
                    Name = "Gena",
                    Id = "A570146D-306D-4B57-8FFB-7AEF91DAE019"
                };
            }
            return mUser3;

        }

        string NewGuid
        {
            get { return Guid.NewGuid().ToString(); }
        }
        public Runner CreateCurrentUserRunner(RouteInfo route)
        {
            Runner r = new Runner();
            r.Id = NewGuid;
            r.Velocity = 2;
            r.User = GetCurrentUser();
          
            r.LinkedRunners = new List<string>() {
                GetSecondUser().Id
             //  , GetThirdUser().Id
            };
            r.State = TripState.Running;
            r.RouteId = route.Id;
            r.CurrentStep = route.Steps.First();
            r.NextStep = r.CurrentStep;
            return r;
        }

        public Runner CreateSecondUserRunner(RouteInfo route)
        {
            Runner r = new Runner()
            {
                Id = NewGuid,
                Velocity = 2,
                //// Origin = 25,
                User = GetSecondUser(),
                
                LinkedRunners = new List<string>() { GetCurrentUser().Id },
                State = TripState.Running,
                RouteId = route.Id,
                CurrentStep = route.Steps.First(),
                NextStep = route.Steps.First(),
                Distance = 20,
            };
            return r;
        }
        public Runner CreateThirdUserRunner(RouteInfo route)
        {
            Runner r = new Runner()
            {
                Id = NewGuid,
                Velocity = 2,
               // Origin = -25,
                User = GetThirdUser(),
               
                LinkedRunners = new List<string>() { GetCurrentUser().Id },
                RouteId = route.Id,
                CurrentStep = route.Steps.First(),
                NextStep = route.Steps.First(),
                Distance = -25,
                State = TripState.Running,
            };
            return r;
        }

        public IEnumerable<Runner> CreateUsers(int count)
        {

            List<Runner> l = new List<Runner>();
            var vmin = 2f;
            var vmax = 4f;
            var vstep = (vmax - vmin) / (count - 1);
            for (int i = 0; i < count; i++)
            {
                Runner r = new Runner()
                {
                    Id = NewGuid,
                    Velocity = vmin + (i - 1) * vstep,
                    // Origin = -25,
                    User = GetSecondUser(),

                    LinkedRunners = new List<string>() { GetCurrentUser().Id },
                    RouteId = StraightRoute.RouteId,
                    Distance = -25,
                    State = TripState.Running,
                };
                l.Add(r);
            }
            return l;
        }
    }
}
