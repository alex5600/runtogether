using System;
using System.Linq;
using System.Collections.Generic;

using CoreGraphics;
using SceneKit;
using UIKit;
using CoreAnimation;
using System.Threading;


namespace IOSSupport.Extensions
{
   
    static class AnimationExtensions
    {
        public static CAAnimation AnimationWithSceneNamed(string name)
        {
            CAAnimation animation = null;
            var scene = SCNScene.FromFile(name);
            if (scene != null)
            {

                scene.RootNode.EnumerateChildNodes(new SCNNodeHandler((SCNNode child, out bool stop) =>
                {
                    stop = false;
                    var key = child.GetAnimationKeys().FirstOrDefault();
                    if (!string.IsNullOrEmpty(key))
                    {
                        animation = child.GetAnimation(key);
                        stop = true;

                    }
                }));

            }
            return animation;
        }

        public static SCNCamera CameraWithSceneNamed(string name)
        {
            SCNCamera camera = null;
            var scene = SCNScene.FromFile(name);
            if (scene != null)
            {

                scene.RootNode.EnumerateChildNodes(new SCNNodeHandler((SCNNode child, out bool stop) =>
                {
                    stop = false;
                    if (child.Camera != null)
                    {
                        camera = child.Camera;
                        stop = true;
                    }
                    //var key = child.GetAnimationKeys().FirstOrDefault();
                    //if (!string.IsNullOrEmpty(key))
                    //{
                    //    animation = child.GetAnimation(key);
                    //    stop = true;

                    //}
                }));

            }
            return camera;
        }

    }
}

