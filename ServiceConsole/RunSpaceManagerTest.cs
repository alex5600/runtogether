﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RunTogether.DataObjects;
using RunTogether.Shared.RunSpace;
using Shared.Core;

namespace ServiceConsole
{
    public class RunSpaceManagerTest
    {

        public void Test1()
        {
            MockRuntimeManager server = new MockRuntimeManager();
            LocalRunSpaceManager local = new LocalRunSpaceManager(server);
            var actor = new Runner();
            local.Actor = actor;
            local.PositionUpdated += Local_PositionUpdated;
            PositionUpdater u = new PositionUpdater();
            local.Start(u);
            u.Start();
            int count = 0;
            local.StateChanged += (e) =>
            {
                count++;
                int runners = local.Runners.Count;
            };
            var user = new Runner();
            server.ChangeState(user, UpdateStatus.Added);
            actor.Velocity = 100;
            local.ChangeState(actor, UpdateStatus.Updated);
            int i = 0;
            Thread.Sleep(5000);
        }

        private void Local_PositionUpdated()
        {
            LoggingService.LogDebug("Updated");
        }
    }
    class PositionUpdater : IPositionUpdater
    {
        Timer timer;
        public void Start()
        {
            timer = new Timer((_) =>
            {
                Update(DateTimeOffset.Now);
            }, null, 1000, 1000);
        }
        public event Action<DateTimeOffset> Update;
    }
}
