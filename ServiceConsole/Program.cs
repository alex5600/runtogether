﻿using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using RunTogether.DataObjects;
using RunTogether.Shared.AzureClient;
using RunTogether.Shared.DataStore;

namespace ServiceConsole
{
    class Program
    {
        static void Main2(string[] args)
        {
            //using (var ctx = new SchoolContext())
            //{
            //    User stud = new User() { Name = "New Student" };

            //    ctx.Users.Add(stud);
            //    ctx.SaveChanges();
            //}
           string url = "http://localhost:59742/";
            MobileClient.InitializeClient(url);
            StoreManager.UseMock = true;
            var store = StoreManager.Instance.UserStore;
            var users = store.GetItemsAsync().Result;

            var routeStore = StoreManager.Instance.RouteStore;
            var routes = routeStore.GetItemsAsync().Result;
            // int n = users.Count();
            RunTogether.DataObjects.User u = new RunTogether.DataObjects.User() { Name = "User ", LastName = "Test" };
            var s = store.InsertAsync(u).Result;
            ////  string url = "http://localhost/BackendService";
            //  var client = new MobileServiceClient(url);
            //  //var todoTable = client.GetTable<ToDoItem>();
            //  //var t =  todoTable
            //  //            .Where(todoItem => todoItem.Complete == false).ToListAsync();
            //  //var items = t.Result;
            //  var val = client.InvokeApiAsync("Values", HttpMethod.Get, null).Result;

            //  Dictionary<string, string> pars = new Dictionary<string, string>
            //  {
            //      { "data", "test" }
            //  };

            ////  var va3 = client.InvokeApiAsync<IEnumerable<MyUser>>("Values/Get1", HttpMethod.Get, pars).Result;


            //  //MyUser u = new MyUser() { Name = "User ", LastName = "Test"};
            //  //var res = client.InvokeApiAsync<MyUser, string>("Values/AddUser", u, HttpMethod.Post, null).Result;

            //  //res = client.InvokeApiAsync<string, string>("Values/AddNumber", "alex", HttpMethod.Post,null).Result;
            //  //res = client.InvokeApiAsync<int, string>("Values/AddNumber1", 10, HttpMethod.Post, null).Result;
            //  var users = client.GetTable<MyUser>();
            //  var s = users.ToListAsync().Result;
            //  //var s1 = users.Where( i=> i.Name  != "AAA").IncludeTotalCount().ToListAsync().Result.Count;
            //  //Add().Wait();
            //  //int r = users.ToListAsync().Result.Count;


        }

        static void Main(string[] args)
        {
            RunSpaceManagerTest t = new RunSpaceManagerTest();
            t.Test1();

        }

    }

    public class User
    {
        public int UserId { get; set; }
        public string Name { get; set; }
    }

    public class SchoolContext : DbContext
    {
        public SchoolContext() : base()
        {

        }

        public DbSet<User> Users { get; set; }
       

    }

    public class ToDoItem
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "complete")]
        public bool Complete { get; set; }
    }

    public class Test
    {
        public string Name => getname();

        string getname()
        {
            return "A";
        }
    }
}