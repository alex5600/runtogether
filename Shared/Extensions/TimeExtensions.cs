﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Extensions
{
    public static class TimeExtensions
    {
        static DateTime unixStart = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
        public static int ToUnixUtc(this DateTime time)
        {
            return (int)Math.Floor((time.ToUniversalTime() - unixStart).TotalSeconds);
        }

        //public static int CompareTo(this DateTime item, DateTime item1, TimeSpan interval)
        //{
        //    double s1 = (item - item1).TotalSeconds;
        //    if (Math.Abs(s1) <= interval.TotalSeconds)
        //        return 0;
        //    else
        //        return (s1 > 0) ? 1 : -1;
        //}
    }
}
