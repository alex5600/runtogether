﻿using RestSharp;
using Shared.Maps;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Maps
{
    public class CoogleApiClient
    {
        protected readonly RestClient restClient;
        protected readonly string mToken;
        string baseUri;

        // https://maps.googleapis.com/maps/api/directions/json?origin=Brooklyn&destination=Queens&departure_time=1464274007&key=AIzaSyDLpKNHlsGlAqGOGO7bRIVg3OKOMcnLogQ
        // https://maps.googleapis.com/maps/api/distancematrix/json?origins=Brooklyn&destinations=Queens&departure_time=now&key=AIzaSyDLpKNHlsGlAqGOGO7bRIVg3OKOMcnLogQ
        public CoogleApiClient(string baseUri)
        {
            this.baseUri = baseUri;
            restClient = new RestClient(baseUri);

            // mToken = token;
            mToken = Constants.ApiKey;
        }
        protected RestRequest CreateRequest(string url = null)
        {
            RestRequest r = new RestRequest(url);
            r.RequestFormat = DataFormat.Json;
            r.AddQueryParameter("key", mToken);
            return r;

        }

    }
}
