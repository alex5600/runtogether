﻿using System;
using System.Collections.Generic;
using System.Text;
#if __IOS__
using CoreLocation;
using Google.Maps;
#endif
namespace Shared.Maps
{

    public static class MapHelper
    {
        /// <summary>
        /// Decode google style polyline coordinates.
        /// </summary>
        /// <param name="encodedPoints"></param>
        /// <returns></returns>
        public static IEnumerable<RouteStep> Decode(string encodedPoints)
        {
            if (string.IsNullOrEmpty(encodedPoints))
                throw new ArgumentNullException("encodedPoints");

            char[] polylineChars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLng = 0;
            int next5bits;
            int sum;
            int shifter;

            while (index < polylineChars.Length)
            {
                // calculate next latitude
                sum = 0;
                shifter = 0;
                do
                {
                    next5bits = (int)polylineChars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length)
                    break;

                currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                //calculate next longitude
                sum = 0;
                shifter = 0;
                do
                {
                    next5bits = (int)polylineChars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length && next5bits >= 32)
                    break;

                currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                yield return new RouteStep
                {
                    Latitude = Convert.ToDouble(currentLat) / 1E5,
                    Longitude = Convert.ToDouble(currentLng) / 1E5
                };
            }
        }

        /// <summary>
        /// Encode it
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static string Encode(IEnumerable<MapCoordinate> points)
        {
            var str = new StringBuilder();

            var encodeDiff = (Action<int>)(diff =>
            {
                int shifted = diff << 1;
                if (diff < 0)
                    shifted = ~shifted;

                int rem = shifted;

                while (rem >= 0x20)
                {
                    str.Append((char)((0x20 | (rem & 0x1f)) + 63));

                    rem >>= 5;
                }

                str.Append((char)(rem + 63));
            });

            int lastLat = 0;
            int lastLng = 0;

            foreach (var point in points)
            {
                int lat = (int)Math.Round(point.Latitude * 1E5);
                int lng = (int)Math.Round(point.Longitude * 1E5);

                encodeDiff(lat - lastLat);
                encodeDiff(lng - lastLng);

                lastLat = lat;
                lastLng = lng;
            }

            return str.ToString();
        }
        public static double CalculateHeading(double lat1, double long1, double lat2, double long2)
        {
#if __IOS__
            return Google.Maps.GeometryUtils.Heading(new CLLocationCoordinate2D(lat1, long1), new CLLocationCoordinate2D(lat2, long2));
#else
            double a = lat1 * Math.PI / 180;
            double b = long1 * Math.PI / 180;
            double c = lat2 * Math.PI / 180;
            double d = long2 * Math.PI / 180;

            if (Math.Cos(c) * Math.Sin(d - b) == 0)
                if (c > a)
                    return 0;
                else
                    return 180;
            else
            {
                double angle = Math.Atan2(Math.Cos(c) * Math.Sin(d - b), Math.Sin(c) * Math.Cos(a) - Math.Sin(a) * Math.Cos(c) * Math.Cos(d - b));
                return (angle * 180 / Math.PI + 360) % 360;

            }
#endif
        }

        public static double HeadingEps = 60;
        public static bool SameHeading(double h1, double h2, double eps = 0)
        {
            if (eps == 0)
                eps = HeadingEps;
            var p = Math.Abs(h1 - h2);
            return p <= eps|| (360 - p) <= eps;
        } 
        public static double CalculateHeading(this MapCoordinate start, MapCoordinate dest)
        {
            return CalculateHeading(start.Latitude, start.Longitude, dest.Latitude, dest.Longitude);
        }

#if __IOS__
        public static double Distance(this CLLocationCoordinate2D fromCoord, CLLocationCoordinate2D toCoord)
        {
            return Google.Maps.GeometryUtils.Distance(fromCoord, toCoord);
        }

        public static bool IsLocationOnPath(this CLLocationCoordinate2D point, CLLocationCoordinate2D start, CLLocationCoordinate2D end)
        {
           
            var path = new MutablePath();
            path.AddCoordinate(start);
            path.AddCoordinate(end);
            return Google.Maps.GeometryUtils.IsLocationOnPath(point, path, false, 1);
        }
        public static CLLocationCoordinate2D Offset(this CLLocationCoordinate2D fromCoord, double distance, double heading)
        {
            return Google.Maps.GeometryUtils.Offset(fromCoord, distance, heading);
        }

        public static double Heading(this CLLocationCoordinate2D fromCoord, CLLocationCoordinate2D toCoord)
        {
            return Google.Maps.GeometryUtils.Heading(fromCoord, toCoord);
        }
#endif
        public static double CalculateDistance(this MapCoordinate start, MapCoordinate dest)
        {

            return Distance(start.Latitude, start.Longitude, dest.Latitude, dest.Longitude);
        }

        public static double Distance(this MapCoordinate start, double lat, double lng)
        {
            return Distance(start.Latitude, start.Longitude, lat, lng);
        }
        public static double Distance(double lat1, double lon1, double lat2, double lon2)
        {
#if __IOS__
            return Google.Maps.GeometryUtils.Distance(new CLLocationCoordinate2D(lat1, lon1), new CLLocationCoordinate2D(lat2, lon2));
#else
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 111190;// 60 * 1.1515 * 1.609344 * 1000;

            return (dist);
           
#endif
        }

       
        const double EarthRadius = 6378137.0;
        const double DegreesToRadians = 0.0174532925;
        const double RadiansToDegrees = 57.2957795;
        /// <summary> 
        /// Calculates the new-point from a given source at a given range (meters) and bearing (degrees). . 
        /// </summary> 
        /// <param name="source">Orginal Point</param> 
        /// <param name="range">Range in meters</param> 
        /// <param name="bearing">Bearing in degrees</param> 
        /// <returns>End-point from the source given the desired range and bearing.</returns> 

        public static MapCoordinate Offset(this MapCoordinate source, double distance, double heading)
        {
#if __IOS__
            return Google.Maps.GeometryUtils.Offset(source.ToLocationCoordinate(), distance, heading).ToMapCoordinate();
#else
            double latA = source.Latitude * DegreesToRadians;
            double lonA = source.Longitude * DegreesToRadians;
            double angularDistance = distance / EarthRadius;
            double trueCourse = heading * DegreesToRadians;

            double lat = Math.Asin(Math.Sin(latA) * Math.Cos(angularDistance) + Math.Cos(latA) * Math.Sin(angularDistance) * Math.Cos(trueCourse));

            double dlon = Math.Atan2(Math.Sin(trueCourse) * Math.Sin(angularDistance) * Math.Cos(latA), Math.Cos(angularDistance) - Math.Sin(latA) * Math.Sin(lat));
            double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

            return new MapCoordinate(lat * RadiansToDegrees, lon * RadiansToDegrees);
#endif
        }

        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }


    }

#if __IOS__
    static class LocationExtensions
    {
        public static CLLocationCoordinate2D ToLocationCoordinate(this MapCoordinate item)
        {
            return new CLLocationCoordinate2D(item.Latitude, item.Longitude);
        }

        public static MapCoordinate ToMapCoordinate(this CLLocationCoordinate2D item)
        {
            return new MapCoordinate(item.Latitude, item.Longitude);
        }
    }
#endif
}
