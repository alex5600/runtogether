﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Maps
{
    public enum RouteType
    {
        walking,
        biking
    }
    public class RouteDefinition
    {
        public RouteType RouteType { get; set; }
        public Guid Id { get; set; }
        public double Distance { get; set; }
        public double Duration { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
        public string Category { get; set; }
        public string ImageContent { get; set; }

        public MapCoordinate Origin { get; set; }
        public MapCoordinate Destination { get; set; }
        public List<RouteStep> Steps { get; set; }
        public bool MapOnly { get; set; }

    }

    public interface IRouteManager
    {
        Task<IEnumerable<RouteDefinition>> GetRoutes();
         Task<RouteDefinition> GetRoute(RouteDefinition route);

        Task PopulateRoutes();

        void Clear();

        void Update(RouteDefinition route);
        Task<bool> Add(RouteDefinition route);
        void Delete(RouteDefinition route);

    }
    
}
