﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace Shared.Maps
{

   
    public class MapCoordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        
        public MapCoordinate()
        {

        }
        public MapCoordinate(double lat, double lng)
        {
            Latitude = lat;
            Longitude = lng;
        }
        public override string ToString()
        {
            return string.Join(",", Latitude, Longitude);
        }
    }

   
    public class RouteStep : MapCoordinate
    {
        public double Heading { get; set; }
        public double Distance { get; set; }
        public string PanoramaID { get; set; }
        [NonSerialized]
        public object Tag;
#if __IOS__
        public static implicit operator Google.Maps.Panorama( RouteStep item)
        {
            return item != null ? item.Tag as Google.Maps.Panorama : null;
        }
#endif

    }
    //public class RouteDefinition
    //{
    //    public double Distance { get; set; }
    //    public double Duration { get; set; }
    //    public string Name { get; set; }
    //    public MapCoordinate Origin
    //    {
    //        get { return Steps.First(); }
    //    }
    //    public MapCoordinate Destination { get { return Steps.Last(); } }
    //    public List<MapCoordinate> Steps { get; set; }
    //}

}
