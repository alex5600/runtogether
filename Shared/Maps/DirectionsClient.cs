﻿
using RestSharp;
using RestSharp.Deserializers;
using Shared.Factories;
using Shared.Impl;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Maps
{
    class BaseData
    {
        public double Value { get; set; }
    }
    class Direction
    {
        public List<Route> routes { get; set; }
    }
    class Route
    {
        public List<Leg> legs { get; set; }
    }
    class Leg
    {
        public BaseData Distance { get; set; }
        public BaseData Duration { get; set; }
        public List<Step> steps { get; set; }
    }
    class Step
    {
        public Polyline polyline { get; set; }
    }
    class Polyline
    {
        public string points { get; set; }
    }

   
    public class DirectionsClient: CoogleApiClient
    {
        
         public DirectionsClient(string baseUri = "https://maps.googleapis.com/maps/api/directions/json"):
            base(baseUri)
        {
            
        }

        public RouteDefinition GetDirections(MapCoordinate origin, MapCoordinate destination)
        {
            return GetDirections(origin.ToString(), destination.ToString());
        }
        public RouteDefinition GetDirections(string origin, string destination)
        {
            return GetDirectionsAsync(origin, destination).Result;
        }


        public async Task<RouteDefinition> GetDirectionsAsync(MapCoordinate origin, MapCoordinate destination)
        {
            return await GetDirectionsAsync(origin.ToString(), destination.ToString());
        }
        public async Task<RouteDefinition> GetDirectionsAsync(string origin, string destination)
        {
            var request = CreateRequest();
            request.AddQueryParameter("origin", origin);
            request.AddQueryParameter("destination", destination);
            request.AddQueryParameter("mode", "walking");

            var response = await restClient.ExecuteGetTaskAsync(request).ConfigureAwait(false);

            var deserial = SharedFactory.CreateSerializer();
            var r = deserial.Deserialize<Direction>(response.Content);
            
            RouteDefinition result = new RouteDefinition();
            var leg = r.routes[0].legs[0];
            result.Distance = leg.Distance.Value;
            result.Duration = leg.Duration.Value;
            result.Steps = new List<RouteStep>();
            var steps = leg.steps;
            bool first = true;
            foreach (var step in steps)
            {
                string ps = step.polyline.points;
                var line = MapHelper.Decode(ps);
                if (!first)
                    line = line.Skip(1);
                result.Steps.AddRange(line);
                first = false;
            }
            double d = 0;
            RouteStep last = result.Steps.First();
            for(int i = 1; i < result.Steps.Count ; i++)
            {
                var next = result.Steps[i];
                var delta = MapHelper.CalculateDistance(last, next);
                last.Heading = MapHelper.CalculateHeading(last,next);
                last.Distance = d;
                d += delta;
                last = next;
            }
            result.Steps.Last().Distance = d;
            result.Origin = result.Steps.First();
            result.Destination = result.Steps.Last();
            return result;
            
        }

        public async Task<bool> PopulateDirectionsAsync(RouteDefinition route)
        {
            var request = CreateRequest();
            request.AddQueryParameter("origin", route.Origin.ToString());
            request.AddQueryParameter("destination", route.Destination.ToString());
            request.AddQueryParameter("mode", route.RouteType.ToString());

            var response = await restClient.ExecuteGetTaskAsync(request).ConfigureAwait(false);
            var ds = SharedFactory.CreateSerializer();
           //JsonDeserializer deserial = new JsonDeserializer();
            var r = ds.Deserialize<Direction>(response.Content);

            var leg = r.routes[0].legs[0];
            route.Distance = leg.Distance.Value;
            route.Duration = leg.Duration.Value;
            route.Steps = new List<RouteStep>();
            var steps = leg.steps;
            bool first = true;
            foreach (var step in steps)
            {
                string ps = step.polyline.points;
                var line = MapHelper.Decode(ps);
                if (!first)
                    line = line.Skip(1);
                route.Steps.AddRange(line);
                first = false;
            }
            double d = 0;
            RouteStep last = route.Steps.First();
            for (int i = 1; i < route.Steps.Count; i++)
            {
                var next = route.Steps[i];
                var delta = MapHelper.CalculateDistance(last,next);
                last.Heading = last.CalculateHeading(next);
                last.Distance = d;
                d += delta;
                last = next;
            }
            route.Origin = route.Steps.First();
            route.Destination = route.Steps.Last();
            route.Steps.Last().Distance = d;
            return true;
        }
    }


    public class StreetImageClient : CoogleApiClient
    {


         public StreetImageClient(string baseUri = "https://maps.googleapis.com/maps/api/streetview") :
            base(baseUri)
        {

        }

        public async Task<string> GetImageAsync(MapCoordinate location, double heading, Size? size = null, double fov = 90)
        {
            var request = CreateRequest();
            request.AddQueryParameter("location", location.ToString());
            return await GetImageImplAsync(heading, size, fov, request);
        }

        public async Task<string> GetImageAsync(string panorama, double heading, Size? size = null, double fov = 90)
        {
            var request = CreateRequest();
            request.AddQueryParameter("pano ", panorama);
            return await GetImageImplAsync(heading, size, fov, request);
        }

        public async Task<byte[]> GetImageBytesAsync(string panorama, double heading, Size? size = null, double fov = 90)
        {
            var request = CreateRequest();
            request.AddQueryParameter("pano", panorama);
            request.AddQueryParameter("heading", Convert.ToString(heading));
            request.AddQueryParameter("fov", Convert.ToString(fov));
            if (size != null && size.HasValue)
                request.AddQueryParameter("size", $"{size.Value.Width}x{size.Value.Height}");
            else
                request.AddQueryParameter("size", $"128x128");

            var response = await restClient.ExecuteGetTaskAsync(request).ConfigureAwait(false);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.RawBytes;
            }
            return null;
        }

        private async Task<string> GetImageImplAsync(double heading, Size? size, double fov, RestRequest request)
        {
            request.AddQueryParameter("heading", Convert.ToString(heading));
            request.AddQueryParameter("fov", Convert.ToString(fov));
            if (size != null && size.HasValue)
                request.AddQueryParameter("size", $"{size.Value.Width}x{size.Value.Height}");
            else
                request.AddQueryParameter("size", $"128x128");

            var response = await restClient.ExecuteGetTaskAsync(request).ConfigureAwait(false);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Convert.ToBase64String(response.RawBytes);
            }
            return null;
        }
    }
}
