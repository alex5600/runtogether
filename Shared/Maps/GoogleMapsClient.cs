﻿
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Shared.Maps
{
   
   

    public class GoogleMapsClient
    {
        protected readonly RestClient restClient;
        protected readonly string mToken;
        string baseUri;
      
        // https://maps.googleapis.com/maps/api/directions/json?origin=Brooklyn&destination=Queens&departure_time=1464274007&key=AIzaSyDLpKNHlsGlAqGOGO7bRIVg3OKOMcnLogQ
        // https://maps.googleapis.com/maps/api/distancematrix/json?origins=Brooklyn&destinations=Queens&departure_time=now&key=AIzaSyDLpKNHlsGlAqGOGO7bRIVg3OKOMcnLogQ
        public GoogleMapsClient(string token, string baseUri = "https://maps.googleapis.com/maps/api/distancematrix/json")
        {
            this.baseUri = baseUri;
            restClient = new RestClient(baseUri);
           
            mToken = token;
            mToken = "AIzaSyDLpKNHlsGlAqGOGO7bRIVg3OKOMcnLogQ";
         
            
            // Set accept headers to JSON only
          //  _httpClient.DefaultRequestHeaders.Accept.Clear();
           // _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        //public async Task<DistanceResponse> GetDistance(Location origin, Location destination, DateTime? departureTime = null)
        //{
        //    var request = CreateRequest();
        //    request.AddQueryParameter("origins", origin.GetLocation());
        //    request.AddQueryParameter("destinations", destination.GetLocation());
           
        //    if (departureTime != null && departureTime.HasValue)
        //    {
        //        int depart = departureTime.Value.ToUnixUtc();
        //        request.AddQueryParameter("departure_time", depart.ToString());
        //    }
            
           
        //    var response = await restClient.ExecuteGetTaskAsync<DistanceResponse>(request)
        //        .ConfigureAwait(false);

        //    return response.Data;
          
        //}
        

        //public async Task<DistanceResponse> EstimateTravel(IEnumerable<ResRequest> requests)
        //{
         
        //    var query = CreateQuery();
        //    string origin = string.Join("|", requests.Select(i => i.Origin));
        //    query["origins"] = origin;
        //    string destination = string.Join("|", requests.Select(i => i.Destination));
        //    query["destinations"] = destination.ToString();
        //    if (departureTime != null && departureTime.HasValue)
        //    {
        //        int depart = departureTime.Value.ToUnixUtc();
        //        query["departure_time"] = depart.ToString();
        //    }


        //    UriBuilder b = new UriBuilder(baseUri);
        //    b.Query = query.ToString();
        //    string url = b.ToString();
        //    var response = await _httpClient
        //        .GetAsync(url)
        //        .ConfigureAwait(false);

        //    var responseContent = await response.Content.ReadAsStringAsync();
        //    var res = JsonConvert.DeserializeObject<DistanceResponse>(responseContent);
        //    return res;

        //}

        //public async Task<DistanceResponse> GetDistance1(Location origin, Location destination, int? departureTime = null)
        //{
        //    var query = CreateQuery();
        //    query["origins"] = origin.ToString();
        //    query["destinations"] = destination.ToString();

        //    ;
        //    if (departureTime != null && departureTime.HasValue)
        //        query["departure_time"] = departureTime.Value.ToString();

        //    UriBuilder b = new UriBuilder(baseUri);
        //    b.Query = query.ToString();
        //    string url = b.ToString();
        //    var response = await _httpClient
        //        .GetAsync(url)
        //        .ConfigureAwait(false);

        //    var responseContent = await response.Content.ReadAsStringAsync();
        //    var res = JsonConvert.DeserializeObject<DistanceResponse>(responseContent);
        //    return res;
        //}
        protected RestRequest CreateRequest(string url = null)
        {
            RestRequest r = new RestRequest(url);
            r.RequestFormat = DataFormat.Json;
            r.AddQueryParameter("key", mToken);
            return r;
           
        }
       
    }
}
