﻿using Shared.Factories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace Shared.Maps
{
    public class FileRouteManager : IRouteManager
    {
        const string FileName = "Routes.json";

        List<RouteDefinition> mRoutes = null;
        string GetFileName()
        {
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return Path.Combine(documents, FileName);

        }
        public async Task<IEnumerable<RouteDefinition>> GetRoutes()
        {
            return await Task.Run(() =>
            {
                if (mRoutes == null)
                {
                    string filename = GetFileName();
                    if (File.Exists(filename))
                    {
                        string data = File.ReadAllText(filename);
                        var ser = SharedFactory.CreateSerializer();
                        mRoutes = ser.Deserialize<List<RouteDefinition>>(data);
                    }
                    else
                        mRoutes = new List<RouteDefinition>();
                }
                return mRoutes;
            });
            
        }

        public async Task<RouteDefinition> GetRoute(RouteDefinition route)
        {
            var routes = await GetRoutes();
            if (route == null)
                return mRoutes.FirstOrDefault();
            return routes.Where(r => r.Id == route.Id).SingleOrDefault();
        }
        public void Update(RouteDefinition route)
        {

        }
        public async Task<bool> Add(RouteDefinition route)
        {
            if (mRoutes == null)
            {
                await GetRoutes();
            }
            var item = mRoutes.Where(r => r.Id == route.Id).SingleOrDefault();
            if (item != null)
                mRoutes.Remove(item);
            mRoutes.Add(route);
            Save();
            return true;

        }
        public void Delete(RouteDefinition route)
        {
           
        }

        public void Clear()
        {
            string filename = GetFileName();
            if (File.Exists(filename))
                File.Delete(filename);
        }
        void Save()
        {
            string filename = GetFileName();
            //string data = File.ReadAllText(filename);
            var ser = SharedFactory.CreateSerializer();
            string data = ser.Serialize(mRoutes);
            File.WriteAllText(filename, data);
        }

        public async Task PopulateRoutes()
        {
            Clear();
            var route = new RouteDefinition();
            //route.Category = "Grand Canyoun";
            //route.Name = "Brigh Angel Trail";
            //route.Id = Guid.Parse("EF3E239E-873A-4C97-804F-AB5229021688");
            //var mn = SharedFactory.CreateRouteManager();
            //route.Origin = new MapCoordinate(36.100267, -112.093434);
            //route.Destination = new MapCoordinate(36.077836, -112.127945);
            //route.MapOnly = true;

            route.Category = "Roads";
            route.Name = "169";
            route.Id = Guid.Parse("EF3E239E-873A-4C97-804F-AB5229021688");
            var mn = SharedFactory.CreateRouteManager();
            route.Origin = new MapCoordinate(44.939347, -93.421248);
            route.Destination = new MapCoordinate(44.960497, -93.420703);

            var cl = new DirectionsClient();
            await cl.PopulateDirectionsAsync(route);
#if __IOS__
            if (!route.MapOnly)
            {
                StreetViewApp.Code.PanoramaRouteFactory f = new StreetViewApp.Code.PanoramaRouteFactory();
                await f.CreatePanoramaSteps(route);
                //route.Steps = steps;
            }
#endif
            var cl1 = new StreetImageClient();
            route.ImageContent = cl1.GetImageAsync(route.Origin, route.Origin.Heading).Result;
            await Add(route);

            //45.026769, -93.400729
            //44.977705, -93.400592
        }
    }
}
