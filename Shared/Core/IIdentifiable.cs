﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Core
{
    public interface IIdentifiable
    {
        string Id { get; set; }
    }
}
