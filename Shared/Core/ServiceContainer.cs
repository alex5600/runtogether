﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Core
{
    public static class ServiceContainer
    {
        static Dictionary<Type, object> services = new Dictionary<Type, object>();
        static object gate = new object();
        public static void AddService<T>(T service)
        {
            lock(gate)
            {
                services[typeof(T)] = service;
            }
        }

        public static T GetService<T>()
        {
            lock (gate)
            {
                object service;
                if (services.TryGetValue(typeof(T), out service))
                    return (T)service;
                return default(T);
            }
        }
    }

}
