﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Shared.Core
{
    public class DbLogger
    {
        public const string DbLayer = "Log";
        public const string Locking = "Log";
    }
    public static class LoggingService
    {
        public static void LogDebug(string format, params object[] args)
        {

            System.Diagnostics.Debug.WriteLine(string.Format("Time {0}: {1}", DateTime.Now.TimeOfDay, string.Format(format, args)));
        }

        public static void LogDebug(string message)
        {

            System.Diagnostics.Debug.WriteLine(string.Format("Time {0}: {1}", DateTime.Now.TimeOfDay, message));
        }

        public static void LogDebug(string logger , string message)
        {

            System.Diagnostics.Debug.WriteLine(string.Format("Time {0}: {1}", DateTime.Now.TimeOfDay, message));
        }

        public static void Info(string logger, string message)
        {

            System.Diagnostics.Debug.WriteLine(string.Format("Time {0}: {1}", DateTime.Now.TimeOfDay, message));
        }
        public static void Debug(string logger, string message)
        {

            System.Diagnostics.Debug.WriteLine(string.Format("Time {0}: {1}", DateTime.Now.TimeOfDay, message));
        }

        public static void Error(string logger, string message)
        {

            System.Diagnostics.Debug.WriteLine(string.Format("Time {0}: {1}", DateTime.Now.TimeOfDay, message));
        }
    }
}
