﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Impl
{
    class RestSharpSerializer : ISerializer
    {
        public T Deserialize<T>(string data)
        {
            var s = new RestSharp.Deserializers.JsonDeserializer();
            RestSharp.RestResponse r = new RestSharp.RestResponse();
            r.Content = data;
            return s.Deserialize<T>(r);
        }

        public object Deserialize(string data)
        {
            throw new NotImplementedException();
        }

        public string Serialize(object item)
        {
            var s = new RestSharp.Serializers.JsonSerializer();
            return s.Serialize(item);
        }
    }
}
