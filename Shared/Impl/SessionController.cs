﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Shared.Definitions;
using Shared.Model;
using System.Linq;
using System.Threading;

namespace Shared.Impl
{
    public class SessionController : ISessionController
    {
        Timer updateTimer;
        ConcurrentDictionary<Guid, Runner> mRunners;
        public SessionController():this(null)
        {
           
        }
        public SessionController(RunSpace space)
        {
            mRunners = new ConcurrentDictionary<Guid, Runner>();
            if (space != null)
            {
                foreach (var r in space.Runners)
                    mRunners[r.UserId] = r;
            }
            updateTimer = new Timer(UpdateSession, null, 2000, 2000);
        }
        void UpdateSession(object state)
        {
            RunSpace sp = new RunSpace();
            sp.Runners = mRunners.Values.ToList();
            OnSessionSpaceChanged(sp);
        }
        //public event Action<RunSpace> GlobalSpaceChanged;
        public event Action<RunSpace> SessionSpaceChanged;

        protected virtual void OnSessionSpaceChanged(RunSpace space)
        {
            SessionSpaceChanged?.Invoke(space);
        }
        //public RunSpace UpdateRunner(Runner runner)
        //{
           
        //    mRunners[runner.UserId] = runner;
        //    if (runner.LinkedRunners != null)
        //    {
        //        RunSpace sp = new RunSpace();

        //        sp.Runners = mRunners.Values.Where(r => runner.LinkedRunners.Contains(r.UserId)).ToList();
        //        return sp;
        //    }
        //    else
        //        return null;
        //}

        public void UpdateRunner(Runner runner)
        {

            mRunners[runner.UserId] = runner;
            if (runner.LinkedRunners != null)
            {
                RunSpace sp = new RunSpace();

                sp.Runners = mRunners.Values.Where(r => runner.LinkedRunners.Contains(r.UserId)).ToList();
                OnSessionSpaceChanged(sp);
                
            }
           
        }


    }
}
