﻿using System;
using System.Collections.Generic;
using System.Text;
using Shared.Model;

namespace Shared.Definitions
{
    interface IRunSpaceManager
    {
        void AddRunner(Runner runner);
        void RemoveRunner(Runner runner);
        List<Runner> Update(Runner runner);
        RunSpace GetSpace();
    }
}
