﻿using System;
using System.Collections.Generic;
using System.Text;
using Shared.Model;

namespace Shared.Definitions
{
    public interface ISessionController
    {
        event Action<RunSpace> SessionSpaceChanged;
        //event Action<RunSpace> GlobalSpaceChanged;
       // RunSpace UpdateRunner(Runner runner);
        void UpdateRunner(Runner runner);
    }
}
