﻿using Shared.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Shared.Routes
{
#if __IOS__
    using SCNVector3 = SceneKit.SCNVector3;
#else
    using SCNVector3 = Shared.Geometry.SCNVector3;
#endif

    public class StraightRoute : RouteDefinition
    {
        public static string RouteId = "E84557AC-26C5-4EF0-B70C-B42A5AFC7EA1";
        public StraightRoute(double distance, int steps)
        {
            this.Id = RouteId;
            this.Distance = distance;
            Steps = new List<RouteStep>();
            float dz = (float)(distance / (steps - 1));
            for (int i = 0; i < steps; i++)
            {
                var s = new SCNVector3() { X = 0, Y = 0, Z = dz * i };
                var step = new RouteStep() { Position = s };
                //step.Heading = 0;
                step.Distance = s.Z;
                //step.Heading = new SCNVector3(0, 0, 1);
                Steps.Add(step);
            }
            VideoInfo = new VideoInfo() { File = "Images/short.mp4", Speed = 5 };
        }
    }

    public class CircleRoute : RouteDefinition
    {
        public static string RouteId = "E84557AC-26C5-4EF0-B70C-B42A5AFC7EA1";
        public CircleRoute(double radius, int steps)
        {
            this.Id = RouteId;
            this.Distance = Math.PI * 2 * radius;
            Steps = new List<RouteStep>();
            double w = Math.PI * 2 / (steps - 1);
            RouteStep lastStep = null;
            double distance = 0;
            for (int i = 0; i < steps; i++)
            {
                var z = radius * Math.Sin(w * i);
                var x = radius * Math.Cos(w * i);
                var s = new SCNVector3((float)x, 0, (float)z);
                var step = new RouteStep() { Position = s };

                Steps.Add(step);
                if (lastStep != null)
                {
                   // var delta = new double[3] { s.X - lastStep.Position.X, s.Y - lastStep.Position.Y, s.Z - lastStep.Position.Z };
                    //lastStep.Heading = s;
                    distance += Geometry.Utils.GeometryUtils.Distance(s, lastStep.Position);
                    lastStep.Distance = distance;
                }
                lastStep = step;
            }
        }
    }
}
