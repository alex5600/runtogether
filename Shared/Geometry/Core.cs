﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Geometry
{

#if !__IOS__
    public struct SCNVector3
    {
        public float X;
        public float Y;
        public float Z;
        public SCNVector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public override string ToString()
        {
            return string.Join(", ",X,Y,Z);
        }
    }
#endif

   
}
