﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Geometry.Utils
{
//#if __IOS__
//    using SCNVector3 = SceneKit.SCNVector3;
//#else
//    using SCNVector3 = Shared.Geometry.SCNVector3;
//#endif
    public static class GeometryUtils
    {
        //public static double Distance(SCNVector3 v1, SCNVector3 v2)
        //{
        //    return Math.Sqrt((v1.X - v2.X) * (v1.X - v2.X) + (v1.Y - v2.Y) * (v1.Y - v2.Y) + (v1.Z - v2.Z) * (v1.Z - v2.Z));
        //}


        public static double Distance(Location3 v1, Location3 v2)
        {
            return Math.Sqrt((v1.X - v2.X) * (v1.X - v2.X) + (v1.Y - v2.Y) * (v1.Y - v2.Y) + (v1.Z - v2.Z) * (v1.Z - v2.Z));
        }
    }
}
