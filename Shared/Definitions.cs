﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{

    public interface ISerializer
    {
        string Serialize(object item);
        object Deserialize(string data);
        T Deserialize<T>(string data);
    }

    public interface IFactory
    {
        ISerializer CreateSerializer();
    }
}
