﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
#if __IOS__
    using SCNVector3 = SceneKit.SCNVector3;
#else
    using SCNVector3 = Shared.Geometry.SCNVector3;
#endif
    public enum SessionState
    {
        Started,
        Running,
        Paused,
        Aborted,
        Completed
    }
    public class Workout
    {
        public string SessionId { get; set; }
        public string RouteId { get; set; }
        public DateTimeOffset Started { get; set; }
        public DateTimeOffset LastUpdate { get; set; }
        public SessionState State { get; set; }
        public double Velocity { get; set; }
        public double Distance { get; set; }
        public int StepIndex { get; set; }
        public SCNVector3 Position { get; set; }
        public RouteStep  CurrentStep { get; set; }
        public RouteStep NextStep { get; set; }
        public static Workout Create()
        {
            return new Workout() { SessionId = Guid.NewGuid().ToString(), Started = DateTimeOffset.Now };
        }
       
                                         
    }
}
