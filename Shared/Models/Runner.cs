﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
    //public enum SessionState
    //{
    //    Started,
    //    Running,
    //    Paused,
    //    Aborted,
    //    Completed
    //}
    public class Runner: Workout
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public List<string> LinkedRunners{ get; set; }
       
    }
}
