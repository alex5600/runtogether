﻿using Shared.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
    class User: IIdentifiable
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Avatar { get; set; }
        //Other Data
    }
}
