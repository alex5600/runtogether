﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
#if __IOS__
    using SCNVector3 = SceneKit.SCNVector3;
#else
    using SCNVector3 = Shared.Geometry.SCNVector3;
#endif
    public enum RouteType
    {
        walking,
        biking
    }

    public class RouteStep
    {
        public SCNVector3 Position { get; set; }
        public double Distance { get; set; }
        //public SCNVector3 Heading { get; set; }
    }

    public class RouteDefinition
    {
        public RouteType RouteType { get; set; }
        public string Id { get; set; }
        public double Distance { get; set; }
        public double Duration { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
        public string Category { get; set; }
      
        public List<RouteStep> Steps { get; set; }

        public VideoInfo VideoInfo { get; set; }
        protected virtual void Initialize()
        {

        }

    }
    public class VideoInfo
    {
        public string File { get; set; }
        public double Speed { get; set; }
    }
}
