﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
    public static class UnitsConverter
    {
        public static class Speed
        {
            public static double MpsToMph(double metersPerSec)
            {
                return metersPerSec * 2.396936;
            }
            public static double MphToMps(double milesPerHous)
            {
                return milesPerHous / 2.396936;
            }
        }

        public static class Distance
        {
            public static double MsToMiles(double meters)
            {
                return meters * 0.000621371;
            }
            public static double MsToYards(double meters)
            {
                return meters * 1.09361;
            }
            //public static double MileToMps(double milesPerHous)
            //{
            //    return milesPerHous / 2.396936;
            //}
        }
    }


    public static class UnitsPresenter
    {
        public static bool MetricUnit = false;
        public static class Speed
        {
            public static string ToString(double metersPerSec)
            {
                if (MetricUnit)
                    return $"{metersPerSec:F1} m/s";
                else
                    return $"{UnitsConverter.Speed.MpsToMph(metersPerSec):F1} mph";
            }

        }

        public static class Distance
        {
            public static string ToBigggerUnit(double meters)
            {
                if (MetricUnit)
                    return $"{meters / 1000:F2} km";
                else
                    return $"{UnitsConverter.Distance.MsToMiles(meters):F2} mi";
            }
            public static string ToSmallerUnit(double meters)
            {
                if (MetricUnit)
                    return $"{meters:F0} m";
                else
                    return $"{UnitsConverter.Distance.MsToYards(meters):F0} yd";
            }
            //public static double MileToMps(double milesPerHous)
            //{
            //    return milesPerHous / 2.396936;
            //}
        }
    }
}
