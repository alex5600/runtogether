﻿using AForge.Math;
using Shared.Core;
using Shared.Geometry;
using Shared.Impl;
using Shared.Maps;
using Shared.DataObjects;
using Shared.Routes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleTests
{
    using Shared.Geometry.Utils;
#if __IOS__
    using SCNVector3 = SceneKit.SCNVector3;
#else
    using SCNVector3 = Shared.Geometry.SCNVector3;
#endif
    class Program
    {
        static RouteDefinition route;
        static Runner actor;
        static void Main(string[] args)
        {
            var a = 12.8;
            string s = $"{a:F2}";
            var mn = new UserManager();
            actor = mn.CreateCurrentUserRunner();
            // var user = mn.CreateSecondUserRunner();
            //var user1 = mn.CreateThirdUserRunner();
            LocalSessionManager lm = new LocalSessionManager(0.2);
            lm.AddRunners(new List<Runner>() { actor }, false);
            lm.Actor = actor;
            ServiceContainer.AddService<LocalSessionManager>(lm);
            route = new RouteFactory().GetRoute(Guid.Empty);
            lm.Routes = new List<RouteDefinition>() { route };
            actor.Distance = Math.PI * 50;
            lm.PositionUpdate += Lm_PositionUpdate;
            lm.Start();
            Console.ReadLine();
        }

        private static void Lm_PositionUpdate()
        {
            var d = GeometryUtils.Distance(actor.Position, actor.CurrentStep.Position);
            var d2 = GeometryUtils.Distance(actor.Position, actor.NextStep.Position);
            Console.WriteLine($"Actor Position {actor.Position},StepIndex {actor.StepIndex},  Pos1 {d}, Pos2 {d2}");
        }
    }

    class UserManager
    {
        static User mUser;
        static User mUser2;
        static User mUser3;
        public User GetCurrentUser()
        {
            if (mUser == null)
            {
                mUser = new User()
                {
                    Name = "Alex",
                    Id = Guid.Parse("CAA9F1C6-3D35-4570-9F50-B6581DCD7C1F")
                };
            }
            return mUser;

        }
        public User GetSecondUser()
        {
            if (mUser2 == null)
            {
                mUser2 = new User()
                {
                    Name = "Vadim",
                    Id = Guid.Parse("61B815F6-EA74-4CB4-801D-D80BFB90A4BE")
                };
            }
            return mUser2;

        }

        public User GetThirdUser()
        {
            if (mUser3 == null)
            {
                mUser3 = new User()
                {
                    Name = "Vadim",
                    Id = Guid.Parse("A570146D-306D-4B57-8FFB-7AEF91DAE019")
                };
            }
            return mUser3;

        }
        public Runner CreateCurrentUserRunner()
        {
            Runner r = new Runner();
            r.SessionId = Guid.NewGuid();
            r.Velocity = 2;
            r.UserId = GetCurrentUser().Id;
            r.LinkedRunners = new List<Guid>()
            {
                //  GetSecondUser().Id
                // , GetThirdUser().Id
            };
            r.State = SessionState.Running;
            r.RouteId = CircleRoute.RouteId;
            return r;
        }

        public Runner CreateSecondUserRunner()
        {
            Runner r = new Runner()
            {
                SessionId = Guid.NewGuid(),
                Velocity = 2,
                //// Origin = 25,
                UserId = GetSecondUser().Id,
                LinkedRunners = new List<Guid>() { GetCurrentUser().Id },
                State = SessionState.Running,
                RouteId = CircleRoute.RouteId,
                Distance = 25,
            };
            return r;
        }
        public Runner CreateThirdUserRunner()
        {
            Runner r = new Runner()
            {
                SessionId = Guid.NewGuid(),
                Velocity = 2,
                // Origin = -25,
                UserId = GetThirdUser().Id,
                LinkedRunners = new List<Guid>() { GetCurrentUser().Id },
                RouteId = CircleRoute.RouteId,
                Distance = -25,
                State = SessionState.Running,
            };
            return r;
        }

    }
}
