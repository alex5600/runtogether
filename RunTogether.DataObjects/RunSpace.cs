﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RunTogether.DataObjects
{
    public class RunSpace
    {
        public List<Runner> Runners { get; set; }
    }
}
