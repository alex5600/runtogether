﻿using System;
using System.Collections.Generic;
using System.Text;
using Shared.Geometry;

namespace RunTogether.DataObjects
{

    public enum RouteType
    {
        Walking,
        Biking
    }

    //public class RouteStep
    //{
    //    public Loc Position { get; set; }
    //    public double Distance { get; set; }
    //    //public SCNVector3 Heading { get; set; }
    //}

    public class RouteInfo: BaseDataObject
    {
        public RouteType RouteType { get; set; }
       
        public double Distance { get; set; }
        public double Duration { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
        public string Category { get; set; }
      
        public byte[] Picture { get; set; }
        public VideoInfo VideoInfo { get; set; }

        public virtual List<Position> Steps { get; set; }
        protected virtual void Initialize()
        {

        }

    }
    public class VideoInfo
    {
        public string File { get; set; }
        public double Speed { get; set; }
    }
}
