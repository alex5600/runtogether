﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RunTogether.DataObjects
{
    //public enum SessionState
    //{
    //    Started,
    //    Running,
    //    Paused,
    //    Aborted,
    //    Completed
    //}
    public class Runner: Trip
    {
        public User User { get; set; }
        public List<string> LinkedRunners{ get; set; }
       
    }
}
