﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace RunTogether.DataObjects
{
    public class User: BaseDataObject
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string City { get; set; }
        //Other Data
    }
}
