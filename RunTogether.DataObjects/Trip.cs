﻿using System;
using System.Collections.Generic;
using System.Text;
using Shared.Geometry;

namespace RunTogether.DataObjects
{

    public enum TripState
    {
        Started,
        Running,
        Paused,
        Aborted,
        Completed
    }
    public class Trip: BaseDataObject
    {  
        
        public string RouteId { get; set; }
        public DateTimeOffset Started { get; set; }
        public DateTimeOffset LastUpdate { get; set; }
        public TripState State { get; set; }
        public double Velocity { get; set; }
       
        public int StepIndex { get; set; }
        public double Distance { get; set; }
        public Position CurrentStep { get; set; }
        public Position NextStep { get; set; }

        public Location3 Location { get; set; }


    }
}
